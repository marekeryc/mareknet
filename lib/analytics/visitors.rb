class Visitors
  extend Garb::Model
  
  metrics :pageviews, :avg_time_on_page, :percent_new_visits
  dimensions :page_path
end