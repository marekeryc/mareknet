class Keywords
  extend Garb::Model
  
  metrics :visits
  dimensions :keyword
end