class Sources
  extend Garb::Model
  
  metrics :visits
  dimensions :source
end