task :generate_new_images => :environment do
  # Recreate new image sizes
  GalleryImage.where('filename IS NOT NULL').each do |gi|
    begin
      gi.filename.recreate_versions!
    rescue
      puts 'failed: ' + gi.filename.url
    end
  end
end