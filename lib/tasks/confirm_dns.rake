task :confirm_dns => :environment do
  Site.where(:domain_type => "existing_custom_domain").where(:dns_confirmed => false).each do |s|
    begin
      ip = IPSocket::getaddress(s.domain_name)
      if ip == "66.228.41.175"
        s.update_column(:dns_confirmed, true)
      end
    rescue
      next
    end
  end
end