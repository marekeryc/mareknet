task :cache_analytics, [:site_id]  => :environment  do |t, args|
  args.with_defaults(:site_id => 0) #rake cache_analytics[123]

  require 'google/api_client'

  client = Google::APIClient.new(:application_name => "Onepager", :application_version => "1")
  key = Google::APIClient::PKCS12.load_key("0ce2774cfe5337b407408d659f572260e97fc1be-privatekey.p12", "notasecret")
  service_account = Google::APIClient::JWTAsserter.new("340890208180-cire8348lr1ji49410cve9k3n3h2jjna@developer.gserviceaccount.com", "https://www.googleapis.com/auth/analytics.readonly", key)
  client.authorization = service_account.authorize
  oauth_client = OAuth2::Client.new("", "", {
    :authorize_url => 'https://accounts.google.com/o/oauth2/auth',
    :token_url => 'https://accounts.google.com/o/oauth2/token'
  })
  token = OAuth2::AccessToken.new(oauth_client, client.authorization.access_token)
  ga_profile = Legato::User.new(token).accounts.first.profiles[1]

  if args.site_id == 0
    as = Analytic.joins(:site => :user).where("users.payment_frequency != 'Trial'").where("analytics.updated_at > ?", 3.months.ago).order("analytics.updated_at ASC").readonly(false)
  else
    as = Analytic.joins(:site => :user).where("sites.id = ?", args.site_id).readonly(false)
  end

  as.each do |a|
    begin
      if a.site
        if a.site.custom_domain?
          page_path = a.site.domain_name.downcase + "/"
        else
          page_path = "onepagerapp.com/" + a.site.onepager_address
        end
        if a.time_travel == "week"
          time = {:start_date => (1.week.ago - 1.day), :end_date => 1.day.ago}
        elsif a.time_travel == "month"
          time = {:start_date => (1.month.ago - 1.day), :end_date => 1.day.ago}
        end

        # The meat and potatoes
        results = Analytic.for_page_path(page_path, ga_profile).results(time)
        results.metrics << [:page_views, :avg_session_duration, :percent_new_sessions]

        # If site uses Onepager address and onepagerapp.com/xyz returns no results try easysiteadmin.com/xyz
        if results.total_results == 0 && !a.site.custom_domain?
          page_path = page_path.gsub("onepagerapp.com", "easysiteadmin.com")
          results = Analytic.for_page_path(page_path, ga_profile).results(time)
          results.metrics << [:page_views, :avg_session_duration, :percent_new_sessions]
        end

        if results.total_results > 0
          # Total Views, Avg Time on Site and % New Visitors
          a.total_views = results.first.pageViews
          a.avg_time_on_site = results.first.avgSessionDuration
          a.new_visitors = results.first.percentNewSessions

          # Visits per day
          results = Analytic.for_page_path(page_path, ga_profile).results(time)
          results.dimensions << [:date, :month, :day, :year, :day_of_week]
          results.metrics << :pageviews
          deep_dive = Array.new
          results.each do |r|
            deep_dive << { :day => r.day, :month => r.month, :day_of_week => r.dayOfWeek, :visits => r.pageviews }
          end
          a.deep_dive = deep_dive
          # End Visits per Day

          # Top Keywords
          results = Analytic.remove_keyword("(not provided)").remove_keyword("(not set)").for_page_path(page_path, ga_profile).results(time.merge({:sort => "-pageviews", :limit => 10}))
          results.dimensions << :keyword
          results.metrics << [:sessions, :pageviews]
          keywords = Array.new
          results.each do |r|
            keywords << { :keyword => r.keyword, :visits => r.pageviews }
          end
          a.keywords = keywords
          # End Top Keywords

          # Sources
          results = Analytic.remove_source("onepagerapp.com").remove_source("localhost:3000").for_page_path(page_path, ga_profile).results(time.merge({:sort => "-pageviews", :limit => 10}))
          results.dimensions << :source
          results.metrics << [:sessions, :pageviews]
          sources = Array.new
          results.each do |r|
            sources << { :source => r.source, :visits => r.pageviews }
          end
          a.sources = sources
          # End Sources

          a.save
        end
        
        sleep 1
      end
    rescue Exception => e
      puts e
      if e["errors"][0]["reason"] == "authError"
        client = Google::APIClient.new(:application_name => "Onepager", :application_version => "1")
        key = Google::APIClient::PKCS12.load_key("0ce2774cfe5337b407408d659f572260e97fc1be-privatekey.p12", "notasecret")
        service_account = Google::APIClient::JWTAsserter.new("340890208180-cire8348lr1ji49410cve9k3n3h2jjna@developer.gserviceaccount.com", "https://www.googleapis.com/auth/analytics.readonly", key)
        client.authorization = service_account.authorize
        oauth_client = OAuth2::Client.new("", "", {
          :authorize_url => 'https://accounts.google.com/o/oauth2/auth',
          :token_url => 'https://accounts.google.com/o/oauth2/token'
        })
        token = OAuth2::AccessToken.new(oauth_client, client.authorization.access_token)
        ga_profile = Legato::User.new(token).accounts.first.profiles[1]
      end
      next
    end
  end
end

# namespace :analytics do
#   require 'analytics/analytics_service'
  
#   desc 'cache all analytics information'
#   task :cache_all => :environment do
#       start_time = Time.now
#       count = 0
      
#       # only for Paying users only
#       Analytic.joins(:site => :user).where("users.payment_frequency != 'Trial'").order("analytics.updated_at ASC").readonly(false).each do |a|

#         begin
#           # analytics throttling only every 10 calls.
#           # sleep(2) if count % 10 == 0
#           # get the google information for this theme and this time period (time_travel)
#           googs_output = AnalyticsService::gather(a.site, { :time_travel => a.time_travel })
#           count += 1
#         rescue Exception => e
#           # PostOffice.send_simple_message('Analytics Cache Error', e.message + "\n" + e.backtrace.join("\n"))
#           next
#         end

#         a.attributes = googs_output

#         if a.save
#           puts "Cached analytics for site -> #{a.site.id} : #{a.site.user.email}, threshold = #{a.time_travel} - #{Time.now}"
#         else
#           puts "Could not cache analytics for site -> #{a.site.id} : #{a.site.user.email}"
#         end

#       end
      
#       # PostOffice.send_simple_message("Task cache_all completed", "Total runtime -> #{Time.now - start_time} seconds")
#       puts "Total runtime -> #{Time.now - start_time} seconds"
#   end
# end
