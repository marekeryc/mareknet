task :delete_no_user_sites => :environment do
  s = Site.where("created_at < ?", 2.months.ago).where("user_id IS NULL")
  s.each do |s|
    p = s.pages.first
    puts "site: #{s.id}"
    if p
      puts p.id

      p.buttons.each do |b|
        b.destroy
      end

      p.contact_sections.each do |c|
        c.destroy
      end

      p.containers.each do |c|
        c.downloads.each do |d|
          d.destroy
        end

        c.gallery_images.each do |g|
          g.destroy
        end

        c.hours.each do |h|
          h.destroy
        end

        c.services.each do |s|
          s.destroy
        end

        if c.social_button
          c.social_button.social_links.each do |s|
            s.destroy
          end
        end
      end

      p.embeds.each do |e|
        e.destroy
      end

      p.iframes.each do |i|
        i.destroy
      end

      p.social_feeds.each do |s|
        s.destroy
      end

      p.text_areas.each do |t|
        t.destroy
      end

      p.destroy
      puts p.errors.full_messages
    end

    s.destroy
    puts s.errors.full_messages
  end
end

task :import_arb => :environment do
  require 'nokogiri'

  f = File.open("#{Rails.root}/arb.html")
  doc = Nokogiri::XML(f)
  f.close

  puts doc.css("td").length

  subid = ""
  status = ""
  lastfour = ""
  amount = 0.0

  doc.css("td").each_with_index do |x, i|
    if i%8 == 0
      puts "Subscription ID - " + x.text.gsub(/\s+/, "")
      subid = x.text.gsub(/\s+/, "")
    end
    if i%8 == 2
      puts "Status - " + x.text.gsub(/\s+/, "")
      status = x.text.gsub(/\s+/, "")
    end
    if i%8 == 5
      puts "Last Four - " + x.text.gsub(/\s+/, "")
      lastfour = x.text.gsub(/\s+/, "")[-4..-1]
    end
    if i%8 == 6
      puts "Amount $" + x.text.gsub(/\s+/, "")
      amount = x.text.gsub(/\s+/, "").to_f

      p = Payment.find_by_subscriber_id_monthly(subid)
      if p
        lastfour = ActiveRecord::Base.connection.quote(lastfour)
        if !p.amount.blank?
          amount = amount + p.amount
        end
        puts amount
        # amount = amount if p.amount.blank?
        # amount = amount + p.amount if !p.amount.blank?
        ActiveRecord::Base.connection.execute("UPDATE payments SET last_four = #{lastfour}, amount = #{amount} WHERE id = #{p.id}") if p.credit_card_type.blank?
      end

      p = Payment.find_by_subscriber_id_annual(subid)
      if p
        lastfour = ActiveRecord::Base.connection.quote(lastfour)
        amount = amount if p.amount.blank?
        amount = amount + p.amount if !p.amount.blank?
        ActiveRecord::Base.connection.execute("UPDATE payments SET last_four = #{lastfour}, amount = #{amount} WHERE id = #{p.id}") if p.credit_card_type.blank?
      end
    end
  end
end

task :import_transactions => :environment do
  require 'csv'

  ####################### Updates Authorize.net ARB with User's ID
  # gateway = ActiveMerchant::Billing::Base.gateway('authorize_net')...

  # Payment.where("(subscriber_id_monthly IS NOT NULL OR subscriber_id_annual IS NOT NULL) AND id > 325").each do |p|
  #   puts p.user_id
  #   if !p.subscriber_id_monthly.blank?
  #     r = gateway.update_recurring({:subscription_id => p.subscriber_id_monthly, :customer => {:id => p.user_id}})
  #     if !r.success?
  #       puts p.user.email
  #       puts r.message
  #     end
  #   end

  #   if !p.subscriber_id_annual.blank?
  #     r = gateway.update_recurring({:subscription_id => p.subscriber_id_annual, :customer => {:id => p.user_id}})
  #     if !r.success?
  #       puts p.user.email
  #       puts r.message
  #     end
  #   end
  # end

  ####################### Takes imported transactions and tries to find associated User

  # User.all.each do |u|
  #   transactions = Transaction.where(:first_name => u.first_name).where(:last_name => u.last_name).where(:postal_code => u.zip)
  #   transactions.each do |t|
  #     t.update_column(:user_id, u.id)
  #   end
  # end

  # # Matching transactions is probably more accurate by email, so this overrides what happens above
  # User.all.each do |u|
  #   transactions2 = Transaction.where(:email => u.email)
  #   transactions2.each do |t|
  #     t.update_column(:user_id, u.id)
  #   end
  # end

  ####################### Checks if there are any active users without any tranactions
  # User.where("payment_frequency != 'Trial'").where("cancellation_date IS NULL").each do |u|
  #   if Transaction.where(:user_id => u.id).count == 0
  #     puts u.email
  #   end
  # end

  ####################### Checks to see if transaction matches csv
  # CSV.foreach("#{Rails.root}/transactions.csv", :headers => true) do |row|
  #   # t = Transaction.find_by_transaction_id(row[0])
  #   # puts "a1" if t.transaction_status != row[1].to_s.strip
  #   # puts "a2" if t.amount.to_f != row[2].to_f
  #   # puts "a3" if t.created_at != DateTime.parse(row[4].to_s)
  #   # puts "a5" if t.last_four != row[16].to_s.strip
  #   # puts "a6" if t.first_name != row[24].to_s.strip
  #   # puts "a7" if t.last_name != row[25].to_s.strip
  #   # puts "a8" if t.address != row[27].to_s.strip
  #   # puts "a9" if t.city != row[28].to_s.strip
  #   # puts "a10" if t.state != row[29].to_s.strip
  #   # puts "a11" if t.postal_code != row[30].to_s.strip
  #   # puts "a12" if t.email != row[34].to_s.strip
  #   # puts "a13" if t.customer_id != row[35].to_s.strip
  #   # puts "a14" if t.invoice_number != row[22].to_s.strip
  #   # t = Transaction.where(:transaction_status => row[1].to_s.strip).where(:amount => row[2]).where(:created_at => DateTime.parse(row[4].to_s)).where(:last_four => row[16].to_s.strip).where(:first_name => row[24].to_s.strip).where(:last_name => row[25].to_s.strip).where(:address => row[27].to_s.strip).where(:city => row[28].to_s.strip).where(:state => row[29].to_s.strip).where(:postal_code => row[30].to_s.strip).where(:email => row[34].to_s.strip).where(:customer_id => row[35].to_s.strip).where(:invoice_number => row[22].to_s.strip).first
  #   # if t1.id != t.id
  #   #   puts row[25]
  #   # end
  #   # if t.count == 0
  #   #   trans = t.first
  #   #   trans.transaction_id = row[0]
  #   #   trans.save
  #   # else
  #   #   trans = t.first
  #   #   trans.transaction_id = row[0]
  #   #   trans.save
  #   #   puts t.count
  #   #   puts row[25]
  #   # end
  # end

  ####################### Imports transactions from CSV file
  CSV.foreach("#{Rails.root}/transactions7.csv", :headers => true) do |row|
    # if row.to_s.count(",") == 69
      address = row[27].to_s.strip
      city = row[28].to_s.strip
      state = row[29].to_s.strip
      postal_code = row[30].to_s.strip
      email = row[34].to_s.strip
      customer_id = row[35].to_s.strip
    # elsif row.to_s.count(",") == 70
    #   address = row[27].to_s.strip + " " + row[28].to_s.strip
    #   city = row[29].to_s.strip
    #   state = row[30].to_s.strip
    #   postal_code = row[31].to_s.strip
    #   email = row[35].to_s.strip
    #   customer_id = row[36].to_s.strip
    # end
    t = Transaction.new
    t.transaction_id = row[0]
    t.transaction_status = row[1].to_s.strip
    t.amount = row[2]
    t.created_at = DateTime.parse(row[4].to_s)
    t.updated_at = DateTime.parse(row[4].to_s)
    t.last_four = row[16].to_s.strip
    t.first_name = row[24].to_s.strip
    t.last_name = row[25].to_s.strip
    t.address = address
    t.city = city
    t.state = state
    t.postal_code = postal_code
    t.email = email
    t.customer_id = customer_id
    t.invoice_number = row[22].to_s.strip
    if !customer_id.blank?
      if customer_id.length == 20
        u = User.where("recurly_account_uuid LIKE '#{customer_id}%'")
        if u.count == 1
          t.user_id = u.first.id
        else
          u2 = User.find_by_email(email)
          if u2
            t.user_id = u2.id
          end
        end
      else
        t.user_id = User.find(customer_id).id
      end
    end
    t.save
  end

  ####################### Takes transactions with a recurly ID and sets User ID
  # Transaction.where("customer_id != ''").each do |t|
  #   u = User.where("recurly_account_uuid LIKE '#{t.customer_id}%'").first
  #   if u != nil
  #     t.update_column(:user_id, u.id)
  #   end
  # end
end