task :move_uploaded_files => :environment do
  require 'mysql2'
  require 'fileutils'
  
  db = Mysql2::Client.new(:host => "localhost", :username => "onepagr", :password => "0nir", :database => "onepagr_prod")
  users = db.query("SELECT * FROM users WHERE created_at IS NOT NULL ORDER BY id ASC LIMIT 150")
  
  FileUtils.mkdir_p Rails.root.join('public', 'uploads') # make sure uploads folder exists
  from_path = Rails.root.join('public','system')
  to_path = Rails.root.join('public','uploads','site')
  theme_bg_path = Rails.root.join('public','system', 'theme_backgrounds')
  
  users.each_with_index(:symbolize_keys => true) { |user, i|
    theme = db.query("SELECT * FROM themes WHERE user_id = #{user[:id]} AND created_at IS NOT NULL", :symbolize_keys => true).first
    preset_backgrounds = ["springtime-stroll.jpg", "la-machina.jpg", "rocky.jpg", "charted-waters.png", "slate.jpg", "splinter.jpg", "weathered.jpg", "plaid.png", "old-school.jpg"]
    site = Site.find_by_user_id(user[:id])
    page = Page.find_by_site_id(site[:id])
    
    # MOVE BACKGROUND IMAGES
    if !theme[:background_file_name].blank? && !preset_backgrounds.include?(theme[:background_file_name])
      new_bg_path = Rails.root.join('public', 'uploads', 'site', 'background_image', site[:id].to_s) 
      from_bg = from_path + 'backgrounds' + (theme[:id].to_s + '-' + theme[:background_file_name])
      to_bg = to_path + 'background_image' + site[:id].to_s + theme[:background_file_name]
      
      FileUtils.mkdir_p(new_bg_path)
      begin
        FileUtils.mv(from_bg,to_bg)
      rescue
        FileUtils.rmdir(new_bg_path)
        puts 'failed: ' + from_bg.to_s
      end
    end
    
    # UPLOAD PRESET IMAGE IF OLD DB WAS USING PRESET BACKGROUND IMAGE
    # make copy and move to background_image/id
    if preset_backgrounds.include?(theme[:background_file_name])
      new_bg_path = Rails.root.join('public', 'uploads', 'site', 'background_image', site[:id].to_s) 
      from_bg = theme_bg_path + theme[:background_file_name]
      to_bg = to_path + 'background_image' + site[:id].to_s + theme[:background_file_name]
      
      FileUtils.mkdir_p(new_bg_path)
      begin
        FileUtils.cp(from_bg,to_bg)
      rescue
        FileUtils.rmdir(new_bg_path)
        puts 'failed: ' + from_bg.to_s
      end
    end
    
    # MOVE LOGO IMAGE
    if !theme[:logo_file_name].blank?
      new_logo_path = Rails.root.join('public', 'uploads', 'site', 'logo_image', site[:id].to_s)
      from_logo = from_path + 'logos' + (theme[:id].to_s + '-' + theme[:logo_file_name])
      to_logo = to_path + 'logo_image' + site[:id].to_s + theme[:logo_file_name]
      
      FileUtils.mkdir_p(new_logo_path)
      begin
        FileUtils.mv(from_logo,to_logo)
      rescue
        FileUtils.rmdir(new_logo_path)
        puts 'failed: ' + from_logo.to_s
      end
    end
    
    # MOVE FAVICON IMAGE
    settings = db.query("SELECT * FROM settings WHERE theme_id = #{theme[:id]} AND favicon_file_name IS NOT NULL", :symbolize_keys => true).first
    if !settings.blank? && !settings[:favicon_file_name].blank?
      new_favicon_path = Rails.root.join('public', 'uploads', 'site', 'favicon_image', site[:id].to_s)
      from_favicon = from_path + 'favicons' + (settings[:id].to_s + '-' + settings[:favicon_file_name])
      to_favicon = to_path + 'favicon_image' + site[:id].to_s + settings[:favicon_file_name]
      
      FileUtils.mkdir_p(new_favicon_path)
      begin
        FileUtils.mv(from_favicon,to_favicon)
      rescue
        FileUtils.rmdir(new_favicon_path)
        puts 'failed: ' + from_favicon.to_s
      end
    end
    
    download_container = Container.where("page_id = #{page[:id]} AND child_object = 'downloads'").first
    
    if !download_container.blank?
      old_downloads = db.query("SELECT * FROM downloads WHERE theme_id = #{theme[:id]} AND file_file_name IS NOT NULL", :symbolize_keys => true)
      new_downloads = Download.where("container_id = #{download_container[:id]}")
      
      if new_downloads.size == old_downloads.size
        old_ids = old_downloads.collect { |o| { 'id' => o[:id], 'file_file_name' => o[:file_file_name] } if !o[:id].blank? && !o[:file_file_name].blank? }
        new_ids = new_downloads.collect { |n| { 'id' => n[:id], 'filename' => n[:filename] } if !n[:id].blank? && !n[:filename].blank? }
        
        new_ids.each_with_index do |new_id, i|
          new_download_path = Rails.root.join('public', 'uploads', 'download', new_id['id'].to_s)
          from_download = from_path + 'downloads' + (old_ids.at(i)['id'].to_s + '-' + old_ids.at(i)['file_file_name'])
          to_download = new_download_path + new_id['filename']
          
          FileUtils.mkdir_p(new_download_path)
          begin
            FileUtils.mv(from_download,to_download)
          rescue
            FileUtils.rmdir(new_download_path)
            puts 'failed: ' + from_download.to_s
          end
        end
      else
        puts 'Error: mis-matching number of downloads between old theme and new downloads container for user ' + user[:id]
      end
    end
    
  } # END USERS LOOP
  db.close
  
  # MOVE GALLERY IMAGES - TODO run originals through carrierwave GalleryImageUploader
  GalleryImage.where('filename IS NOT NULL').each do |gi|
    old_path = Rails.root.join('public', 'system', 'images', gi.filename.file.filename)
    begin
      gi.filename = File.open(old_path)
      gi.save!
    rescue
      puts 'failed: ' + old_path.to_s
    end
  end
end