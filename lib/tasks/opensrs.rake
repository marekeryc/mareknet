task :opensrs_batch => :environment do

require 'opensrs'
include OpenSRS

# r = OpenSRS::Server.new(
#   :server => "https://rr-n1-tor.opensrs.net:55443/", 
#   :username => "simande", 
#   :key => "78428c03d56275fc4bbb58e072d8114361d72c691da24e45e446415699a334d1c2bce1fc64834e8b13178ef08ca6e5a9e2f5d95fa82eb459"
# )
s = OpenSRS::Server.new(
  :server => "https://batch.opensrs.net:55443/",
  :username => "simande",
  :key => "78428c03d56275fc4bbb58e072d8114361d72c691da24e45e446415699a334d1c2bce1fc64834e8b13178ef08ca6e5a9e2f5d95fa82eb459"
)

domain_list = ["ctmgranitedesign.com", "americanhomeinspectionsnj.com", "hartroofingdenver.com", "lexingtonconstructionky.com", "peirgreensprinklersandsod.com", "primopaintingnc.com", "TrashHaulingEstimate.com", "planotxappliancerepair.com", "AustinRaccoonRemovals.com", "TreeServiceEstimate.com", "strongenterprisesllc.com", "hillshomeimprovementsoh.com", "banyanenvironmentalco.com", "anteloelectricalrepair.com", "jrvcontractingct.com", "peirgreensprinklers.com", "carpetcleaningindfw.com", "setxkitchenandbathrooms.com", "topnotchroofingandsiding.com", "whitesacheating.com", "3brotherspaintingco.com", "appliancerepairinhoustontx.com", "pestcontrolingreenville.com", "skipspavingandsealcoating.com", "austinadtdealer.com", "bluemountainhomeinspection.com", "chesapeakehomeinspectionandimprovement.com", "southbaysepticservice.com", "cleaningserviceintulsa.com", "rentaboatonlaketravis.com"]

response = s.call(
  :action => "submit",
  :object => "bulk_change",
  :attributes => { 
    :apply_to_all_reseller_items => 0,
    :apply_to_locked_domains => 1,
    :change_items => domain_list,
    :change_type => "domain_renew",
    :auto_renew => 0,
    :let_expire => 0
  }
)

# response = s.call(
#   :action => "submit",
#   :object => "bulk_change",
#   :attributes => { 
#     :apply_to_all_reseller_items => 1,
#     :apply_to_locked_domains => 1,
#     :change_type => "domain_contacts",
#     :contact_email => "hi@onepagerapp.com",
#     :contacts => [ 
#       :type => "tech", 
#       :set => { 
#         :first_name => "Onepager", 
#         :last_name => "Inc.", 
#         :org_name => "Onepager, Inc.", 
#         :phone => "+1.9175428172", 
#         :address1 => "2028 E Ben White Blvd", 
#         :address2 => "240-1788", 
#         :city => "Austin", 
#         :state => "TX", 
#         :postal_code => "78741", 
#         :email => "hi+78741@onepagerapp.com" 
#       } 
#     ]
#   }
# )

# response = s.call( 
#   :action => "submit", 
#   :object => "bulk_change", 
#   :attributes => { 
#     :apply_to_all_reseller_items => 1, 
#     :apply_to_locked_domains => 1, 
#     :change_type => "domain_contacts", 
#     :contact_email => "hi@onepagerapp.com", 
#     :contacts => [ 
#       :type => "owner", 
#       :set => { 
#         :phone => "+1.9175428172", 
#         :address1 => "2028 E Ben White Blvd", 
#         :address2 => "240-1788", 
#         :city => "Austin", 
#         :state => "TX", 
#         :postal_code => "78741", 
#         :email => "hi+78@onepagerapp.com" 
#       } 
#     ] 
#   } 
# )

# http://domains.opensrs.guide/docs/submit-bulk_change

puts response

end