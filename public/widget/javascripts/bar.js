$( document ).ready( function() {
	$('#onepager_bar .onepager_start').click( function( e ) {
		e.preventDefault();
		$( '#onepager_bar form' ).submit();
	} );
	$( '.onepager_bar_remove' ).click( function( e ) {
		e.preventDefault();
		$( '#onepager_bar' ).remove();
	} );
} );