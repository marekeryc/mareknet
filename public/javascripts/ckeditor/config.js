﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config ) {
	config.toolbar_Onepager = [
		{ name: 'basicstyles', items : [ 'Bold','Italic'] },
		{ name: 'links', items : [ 'Link','Unlink'] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList' ] }
	];
	
	config.toolbar_Basic =  [
		['Bold', 'Italic'],
		['Link', 'Unlink']
	];
	
	config.toolbar = 'Onepager';
	config.forcePasteAsPlainText = true;
};

CKEDITOR.on( 'dialogDefinition', function( ev ) {
	// Take the dialog name and its definition from the event data
	var dialogName = ev.data.name;
	var dialogDefinition = ev.data.definition;
	
	if ( dialogName == 'link' ) {
		
		dialogDefinition.buttons = [ CKEDITOR.dialog.okButton, CKEDITOR.dialog.cancelButton ];
		
		dialogDefinition.title = 'Link URL';
		
		// Get a reference to the "Link Info" tab.
		var url_info = dialogDefinition.getContents( 'info' ),
			url_target = dialogDefinition.getContents( 'target' );

		// Remove the "Link Type" combo and the "Browser
		// Server" button from the "info" tab.
		// url_info.remove( 'linkType' ); We hide this instead below, removing it breaks in latest ckeditor
		url_info.remove( 'browse' );

		var linktype = url_info.get('linkType');
		linktype.style = "display: none;";
		
		// Set the default value for the URL field.
		var url = url_info.get( 'url' ),
			protocol = url_info.get( 'protocol' ),
			target = url_target.get( "linkTargetType" );
		
		url.label = '';
		protocol.label = '';
		target.label = 'Open in &nbsp; ';
		
		protocol.items = [
			["http://‎", "http://"],
			["https://‎", "https://"]
		];
		
		target.items = [
			['Default Window', 'notSet'],
			['New Window', '_blank']
		]
	}
	
	// Remove advanced and local
	dialogDefinition.removeContents('advanced');
	dialogDefinition.removeContents('upload');
	// dialogDefinition.removeContents('target');
});
