class PostOffice < ActionMailer::Base
  default from: '"Onepager" <hi@onepagerapp.com>', :reply_to => "hi@onepagerapp.com", :content_type => "text/html"
  default_url_options[:host] = "onepagerapp.com"
  
  def confirm_subscription(subscriber)
    @subscriber = subscriber
    @site = @subscriber.site
    if @site.user.white_label_active
      mail("Return-Path" => "do-not-reply@easysiteadmin.com", :from => '"' + @site.company_name + '" <do-not-reply@easysiteadmin.com>', :reply_to => @site.user.email, :to => @subscriber.email, :subject => "Confirm Newsletter Subscription", :date => Time.now)
    else
      mail("Return-Path" => "do-not-reply@onepagerapp.com", :from => '"' + @site.company_name + '" <do-not-reply@onepagerapp.com>', :reply_to => @site.user.email, :to => @subscriber.email, :subject => "Confirm Newsletter Subscription", :date => Time.now)
    end
  end
  
  def newsletter(newsletter, subscriber)
    @newsletter = newsletter
    @subscriber = subscriber
    @site = newsletter.site
    mail("Return-Path" => "hi@onepagerapp.com", :from => '"' + @site.company_name + '" <hi@onepagerapp.com>', :reply_to => @newsletter.from, :to => @subscriber.email, :subject => @newsletter.subject, :date => Time.now)
  end
  
  def reset_password(user)
    @user = user
    mail(:to => user.email, :subject => "Your Onepager Password", :date => Time.now)
  end

  def whitelabel_reset_password(user)
    @user = user
    mail("Return-Path" => "do-not-reply@easysiteadmin.com", :from => '"Do Not Reply" <do-not-reply@easysiteadmin.com>', :reply_to => "do-not-reply@easysiteadmin.com", :to => user.email, :subject => "Your Site Admin Password", :date => Time.now)
  end
  
  def new_paid_user(user)
    @user = user
    @site = @user.sites.last
    mail(:to => [user.email, "hi@onepagerapp.com"], :subject => "Welcome to Onepager!", :date => Time.now)
  end
  
  def new_trial_user(user)
    @user = user
    @site = @user.sites.last
    mail(:to => [user.email, "hi@onepagerapp.com"], :subject => "Welcome to Onepager!", :date => Time.now)
  end
  
  def new_user_notification(user)
    @user = user
    @site = @user.sites.last
    mail(:to => "everybody@onepagerapp.com", :subject => "[Onepager] New #{@user.payment_plan} #{@user.payment_frequency} User - #{@site.domain_type} - #{@user.email}", :date => Time.now)
  end
  
  def contact_form(contact_form_responses)
    @contact_form_fields_and_responses = Hash.new
    @company_name = contact_form_responses[0].contact_form_field.container.page.site.company_name
    
    if contact_form_responses[0].contact_form_field.container.page.site.user.white_label_active
      @reply_to = "do-not-reply@easysiteadmin.com"
    else
      @reply_to = "do-not-reply@onepagerapp.com"
    end

    # If contact form includes an email field that's filled out with an email, use that as reply to address
    contact_form_responses.each do |cfr|
      if cfr.contact_form_field.field_name.downcase.include? "mail"
        if cfr.value =~ /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i
          @reply_to = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i.match(cfr.value)[0]
        end
      end
    end

    # Use the email specified in "Email form submissions to" if it exists
    if contact_form_responses[0].contact_form_field.container.format.blank?
      @recipient_email = contact_form_responses[0].contact_form_field.container.page.site.user.email
    else
      @recipient_email = contact_form_responses[0].contact_form_field.container.format
    end

    contact_form_responses.each do |cfr|
      @contact_form_fields_and_responses[cfr.contact_form_field.field_name] = cfr.value
    end

    if contact_form_responses[0].contact_form_field.container.page.site.user.white_label_active
      @return_path_email = "do-not-reply@easysiteadmin.com"
    else
      @return_path_email = "do-not-reply@onepagerapp.com"
    end

    mail("Return-Path" => @return_path_email, :from => @return_path_email, :reply_to => @reply_to, :to => @recipient_email, :subject => "Web Site Form Submission for #{@company_name}")
  end
  
  def send_simple_message(subject, email_body, send_to = "everybody@onepagerapp.com", reply_to = "hi@onepagerapp.com")
    mail("Return-Path" => "hi@onepagerapp.com", :from => '"Onepager" <hi@onepagerapp.com>', :reply_to => reply_to, :to => send_to, :subject => subject, :date => Time.now) do |format|
    #mail(:to => send_to, :subject => subject, :date => Time.now) do |format|
      format.text { render :text => email_body }
    end.deliver
  end

  def new_promotion_response(promotion_response)
    @promotion_response = promotion_response
    @user = @promotion_response.page.site.user
    @company_name = @promotion_response.page.site.company_name
    mail("Return-Path" => "do-not-reply@onepagerapp.com", :from => '"Onepager" <do-not-reply@onepagerapp.com>', :reply_to => 'do-not-reply@onepagerapp.com', :to => @promotion_response.promotion.email, :subject => "Onepager Promotion Form Submission")
  end

  def new_paypal_callback_user(user, site, password)
    @user = user
    @site = site
    @password = password
    mail("Return-Path" => "do-not-reply@easysiteadmin.com", :from => '"Do Not Reply" <do-not-reply@easysiteadmin.com>', :reply_to => "do-not-reply@easysiteadmin.com", :to => user.email, :subject => "Your New Account Information", :date => Time.now)
  end
end
