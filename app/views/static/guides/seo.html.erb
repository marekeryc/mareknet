<% content_for :title do %>Search Engine Optimization for Small Businesses - Guides<% end %>
<% content_for :content_class do %>main_content detail_content<% end %>
<%= render :partial => 'header', :locals => { :section => 'guides' } %>

<section class="guide_introduction">
	<h2>Search Engine Optimization for Small Businesses</h2>
	<img src="/assets/application/resources/search-engine-optimization-banner.png" alt="" />
</section>

<article class="detailed_article guide">
	<h3>Search Engine Optimization (SEO) is one of the most mysterious parts of building an online presence, especially for small businesses.</h3>
	<p>We've built this guide to help answer many of the questions about SEO and to give tips about what you should and should not do in your quest to earn the best possible ranking for your site.</p>
	<h4>What is SEO?</h4>
	<p>In the broadest sense, Search Engine Optimization is a series of steps that a website owner uses to ensure their site appears in search results for specific terms. For example, if you are a California real estate agent and you manage your own site, you might choose to optimize for terms like "San Francisco Real Estate" or "Marin County New Homes." Choosing what terms to optimize for is an important step in the process, which we'll get to later.</p>
	<p>This guide discusses SEO techniques in two ways, actions you can take On-Site or Off-Site. On-Site actions are steps you can take to improve the optimization of your own pages and the content they contain. Off-Site actions are done elsewhere on the internet with the goal of driving additional traffic to your site. Both of these are very important to consider when implementing your SEO strategy.</p>
	<h4>What isn't SEO?</h4>
	<p>SEO isn't about searching for your company name and having your website appear. If your website doesn't appear on a direct search for your business's name, you likely have much larger optimization problems. SEO is about attracting unsolicited traffic to your site. These should be people who are interested in doing business with someone in your field but not necessarily you. Of course, when your site appears in their search results, the goal is for them to want to work with you after being successfully sold through the content in your website.</p>
	<h4>What is SEM?</h4>
	<p>SEO is not Search Engine Marketing (SEM), or ad-buying. All SEO techniques can be done for free or extremely low cost. SEM can become very expensive depending on your implementation. Never should SEM or SEO be used as a substitute for each other. Using them in conjunction is the best approach and many people do fine without using SEM at all.</p>
	<p>With that said, SEM is useful in many situations. Since SEO techniques can take as much as a few months to fully grab hold with search engines, SEM can be used as a bridge to the higher levels of traffic that successful SEO tactics provide. SEM is also great if you'd like additional traffic beyond what your SEO strategy has earned you.</p>
	<h4>Why should I worry about SEO?</h4>
	<p>SEO is an effective and inexpensive way to attract new customers. Before the internet, attracting customers was much more difficult. Sending postcards, purchasing newspaper ads, and running radio ads can be useful, but the problem is they are blanket approaches. There is no way to know the person you're advertising towards has any interest in the service you're offering.</p>
	<p>SEO allows you to be in a great position to be noticed by a person that <em>wants to business with you</em> because they are searching for the terms you've optimized for. For as low as the cost of maintaining a website, your business can benefit from SEO and get those motivated customers.</p>
	<h4>How does SEO work?</h4>
	<p>Since their algorithms are trade secrets, no one knows exactly how Google, Yahoo and Bing score websites in terms of SEO. That means there is no way to know exactly how your SEO strategy will impact your ranking across their results. But, there are several general practices that will lead to a higher rank, even though you can't know exactly how those changes will impact your rank.</p>
	<ul>
		<li>
			<h5>Relevant Keyword Usage (On-Page)</h5>
			<p>Selecting and using keywords that support your SEO strategy is the technique used within your actual site pages.</p>
		</li>
		<li>
			<h5>Site Trustworthiness (Off-Page)</h5>
			<p>The credibility of your site is a big determining factor in how it ranks in results.</p>
		</li>
		<li>
			<h5>Site Popularity/Backlinks (Off-Page)</h5>
			<p>This includes links back to your site from other sites. The more prominent the sites that link to you, the better.</p>
		</li>
		<li>
			<h5>Traffic and Social Graph (Off-Page)</h5>
			<p>Your site's traffic and connectedness within people's social graph are less significant, but are becoming more relevant and are worth considering.</p>
		</li>
	</ul>
	<h3>On-Page Factors</h3>
	<p>There are several things you can do today to start to improve your search engine rankings. Your website is the foundation of your SEO strategy, but it's been estimated that your actual site only accounts for less than 20% of your search engine rank.</p>
	<p>Why is that? It turns out that the fact that there has been so much "gaming" of the search engine results in the past, Google/Yahoo/Bing have been forced to rely more and more on off-page factors to determine the usefulness of a given site.</p>
	<p>With that said, neglecting your on-page factors isn't wise either. A good analogy is your on-page factors are sort of like the foundation of your house. You don't show guests the concrete sitting under your house, but it's important, and your house wouldn't stand too well without it.</p>
	<p>The good news is that building a strong foundation isn't hard, you just need to know where to start.</p>
	<h4>Understandable Content</h4>
	<p>For the content on your site to be understandable by a search engine robot, it needs to be written in code. This means keeping content in images or Flash to an absolute minimum. Expect content in images/Flash to not be readable by the robots.</p>
	<h4>Create the Site for Users, not Robots</h4>
	<p>Legitimate SEO experts have long argued that if you create your site in an effort to maximize its impact with search engine robots and not actual users (humans!), you run a serious risk of losing site trustworthiness. In the past, site creators have created extremely keyword dense content that is hidden from users but presented to robots. This and other "blackhat" SEO tactics are a good way to get your business website excluded from search results if you're caught. Don't try to game the system!</p>
	<h4>Standards Compliance</h4>
	<p>Building your site in compliance with W3C standards (they are the people who make sure the internet has guidelines everyone can follow) is a great idea for many reasons. It ensures people with disabilities can view your site as well as people using a variety of browsers on different devices. But, it's also important from an SEO perspective.</p>
	<p>By complying with standards, you are reducing the quantity of the code that makes up your webpages, which in turn puts more weight on the keywords you want to be crawled and indexed by robots. Compliance also helps robots be able to more quickly index your site and minimizes any confusion with how your site is structured.</p>
	<h4>HTML5 vs. Flash</h4>
	<p>Ever since Steve Jobs and Apple announced the iPhone and iPad would not be supporting Flash, the online world has been touting the glory of HTML5 and the impending doom of Flash. Flash will likely be around for some time, but our philosophy is to avoid it whenever possible due to its standards compliance and SEO deficiencies.</p>
	<p>Despite some advancements from Google over the years to attempt to index Flash content, it is still challenging and Flash-based sites consistently rank lower than HTML-based ones. With the advent of HTML5, many of the dynamic animation, video, and audio capabilities that Flash made accessible are now doable in HTML. The takeaway here is that HTML5 can do much of what Flash does, but in a far more SEO friendly way. Use HTML5, not Flash.</p>
	<h4>Choosing Keywords</h4>
	<p>While some aspects of SEO are more scientific, choosing which keywords to optimize for could be considered more of an art. If you are a lawyer in New York, you would have a hard time optimizing for a search term like "lawyer" or "legal advice". Sure, you could do everything right, both on-page and off-page, but the competition for terms like that is so high, it'll be hard to see results. SEO is about appearing on the first page of results and extremely general terms aren't usually worth chasing after.</p>
	<p>Instead, that lawyer could choose to optimize for something like "New York divorce lawyer," "Upper West Side lawyer" or something else more specific. Optimizing for those sorts of terms will be less competitive and you'd like be getting traffic more interested in your business than if you were getting lots of random traffic for a general set of keywords.</p>
	<p>But how do you decide which keywords to shoot for? Thankfully the <a href="https://adwords.google.com/select/KeywordToolExternal">Google Adwords Keyword Tool</a> makes the art of choosing keywords a little more informed. SEOMoz has assembled a great <a href="http://www.seomoz.org/beginners-guide-to-seo/keyword-research">guide</a> for how to do effective keyword research and selection.</p>
	<h4>Keyword Frequency</h4>
	<p>Now that you've chosen your keywords you'd like to optimize for, the next step is to enter them into your site. There are many places you should take a look at when deciding where to plug them in.</p>
	<h5>Page Title</h5>
	<p>The title of each of your pages is important for search engine robots to get a general sense of what your page is about. Your keywords should definitely have some sort of presence in your page titles. </p>
	<p>Aside from the content of your page titles, it's important to think about how you structure your page titles. Since SEO isn't about a user doing a direct search for your company name, it makes sense to put your company in a secondary position to what the content of the actual page is. So, if you're building a site for a new restaurant called Taco Tree, it might be a good idea to format the homepage title tag like this:</p>
	<p><em>Springfield NJ's Favorite Mexican Restaurant: Taco Tree</em></p>
	<p>Or if your keywords are more focused, this could work well:</p>
	<p><em>Springfield's Best Tacos at The Taco Tree 07081</em></p>
	<p>Aside from getting keywords into your title tags, it's important to know another role of title tags: they are your site's salesperson! When a user does a search, the two pieces of your site that are returned to them are the page title and meta description. If your page ranks highly, those two elements are what will convince the user to click or to move on to a next result. Be sure to consider what a user might think when only seeing those small pieces of your site.</p>
	<h5>Headers</h5>
	<p>Your page header is typically contained in the &lt;h1&gt; tags in your HTML. This should contain the content you most want the search engine robots to index. It can be your company name, but consider having it be more keyword focused as &lt;h1&gt; is important in SEO.</p>
	<p>There are other header tags used in HTML (&lt;h2&gt;, &lt;h3&gt;, &lt;h4&gt;, etc.) and while they are important, they become less important in determining your rank as their number goes up. Be sure to place your keywords in the higher ones for more effectiveness.</p>
	<h5>Body</h5>
	<p>The body area of your web pages is the area where most of your content is entered. It's important that the keywords you're optimizing for appear here multiple times and with multiple variations. Of course, you don't want to repeat the keywords excessively, but they should be used and should appear natural to the reader. </p>
	<h5>URL</h5>
	<p>Using keywords in the URL (www.mywebsite.com) of your site is a powerful tactic for SEO. If it doesn't make sense (or it's too late) to use a keyword in the main part of the URL (the domain), then consider using them in any directories you can (www.mywebsite.com/directory-name/) and in page file names (www.mywebsite.com/page-name.html).</p>
	<h5>Alt Tags</h5>
	<p>Since the contents of images can't be crawled, alt tags are used to describe them to the robots. Make sure you use alt tags and include any relevant keywords.</p>
	<p>It's also important that image file names should describe the image and use keywords whenever appropriate.</p>
	<h5>Meta Description</h5>
	<p>Every page should have a meta description, which is a short entry (about 150 characters) that describes the content of the page. Meta descriptions are not viewable within the site to the user, but they are indexed by robots and appear in search results, directly under the page title. So, like page titles, it's a good way to sell the content of your web page and important to get right.</p>
	<p>Using keywords here is a good idea, but since meta descriptions aren't weighted heavily in determining your page ranking, there's no need to be excessive.</p>
	<h5>Meta Keywords</h5>
	<p>Meta keywords are similar to meta descriptions since they aren't viewable by the user within the page. Instead of a sentence or two, meta keywords are actually keywords or phrases, separated by commas (metaword 1, metaword 2, etc.). In the late 90's, meta keywords were important in determining page ranking, but thanks to keyword spamming, they are no longer significantly weighted. Still, experts say they do carry some significance, so it's worthwhile to include some meta keywords in every page within your site.</p>
	<h4>Optimized Markup</h4>
	<p>Whenever building a new webpage, using optimized markup, or the least code possible, is encouraged for SEO purposes. SEO professionals use a term called content to code ratio, which is useful when thinking about your markup. For SEO purposes, you always want more emphasis on the content since that is what will earn you a higher ranking. Extraneous code just waters down the makeup of your pages.</p>
	<p>Throughout the internet there is still a significant number of sites that use old, out-dated markup that hurts their page rankings. Be sure that you or your developer are using the latest coding techniques (HTML5, non-inline CSS, etc.) and are mindful of the concept of content to code ratio.</p>
	<h4>Sitemap.xml</h4>
	<p>All of the major search engines support the use of a sitemap.xml file for websites. This file is just a list containing the URLs of every page you'd like indexed within your site. Using a sitemap.xml file doesn't ensure a page will be crawled and indexed, but it is helpful for robots when they arrive on your site. Why doesn't a sitemap.xml ensure indexing? Search engines put more weight on other sites linking to your pages than what you have to say about your own. Still, sitemap.xml is an important item to take care of with any SEO strategy.</p>
	<h4>Schema.org</h4>
	<p>Recently, the major search engines have thrown their weight behind a new standard that is designed to further improve search results. From Schema.org's homepage: "Many sites are generated from structured data, which is often stored in databases. When this data is formatted into HTML, it becomes very difficult to recover the original structured data."</p>
	<p>An example is an easier way to understand what schema.org does. When you see the word "bass", you might think the musical instrument or the fish. Up until the introduction of schema.org, there was no consistent way to specify if you meant the guitar or the aquatic animal.</p>
	<p>Although it's early on in schema.org's implementation, it's important to consider where to use it in your SEO strategy.</p>
	<h4>Things to Avoid</h4>
	<p>Any well-meaning website manager can inadvertently do things that wind up hurting their page rankings more than helping them. Here are some common issues that you should avoid if possible.</p>
	<h5>Duplicate Content</h5>
	<p>Using the same content on one site that's used on another is a big no-no. Plagiarism can get students kicked out of college and search engines feel the same way about using another site's content on your own. Not taking another site's content is probably common sense. The thing that can be more difficult is if you re-purpose content you've written for one site for another. Search engines don't know that you are technically the same author, but since it's duplicate content, they may hold that against you. Be careful whenever you're in this sort of situation.</p>
	<h5>Overuse of Keywords</h5>
	<p>Commonly called "keyword stuffing", using specific keywords too heavily can get your site penalized by search engines. Experts say that your targeted keywords should be between 1% and 3% of your site's content. You should never create content that is purposely hidden from the user (extremely small type, white text on a white background, etc.). Those are good ways to get penalized for keyword stuffing and shouldn't ever be part of your SEO strategy.</p>
	<p>The thing to remember here is to write your content for the user, not the search engines. If you read your content and it sounds extremely keyword heavy, then you might be getting over that 3% threshold.</p>
	<h5>Misuse of Keywords</h5>
	<p>Using inaccurate keywords to describe images or other content in your site will just dilute the effectiveness of the keywords you are targeting. It will also be frustrating for users that come to your site looking for one thing, then see a totally different thing upon arrival. Be honest and accurate. Your visitors will appreciate it.</p>
	<h5>Excessive Site Downtime</h5>
	<p>Search engine robots are constantly crawling both new sites and ones that have been up for years. If your site is down for any period of time longer than a few hours, it might be crawled and identified as inactive. Search engines aren't interested in sending their users to sites that are sporadically inactive, or even worse, permanently down. Make sure you have a hosting provider with a good track record of consistent uptime.</p>
	<h4>How Onepager Can Help</h4>
	<p>Wow! That was a lot of information to digest. SEO isn't rocket science, and can be accomplished by anyone with patience and persistence, regardless of where you build and host your site.</p>
	<p><em>At Onepager, we like to make things as easy as possible for small business owners that want to use the internet to build their businesses. We do a variety of these best practices to put your site on a good foundation for search engine success. Be sure to check out Onepager if you need a simple site and would like to take advantage of our SEO capabilities.</em></p>
	<h3>Off-Page Factors</h3>
	<p>It seems counter intuitive that your SEO success is largely based on what happens in the world outside of your site but it's true. Search engines have been increasing the importance of off page factors more and more over time because it's an easier way to determine the true value of a page.</p>
	<p>When you think about it, it makes a good deal of sense for the search engines to take this approach. If several trustworthy sites say your page features a specific type of content, then that is much more credible than your own site saying the same thing. </p>
	<p>The heavy usage of off page factors in determining page rankings is great for search engine users trying to find relevant content. But for a young website, it can be a serious snag on the path to SEO success. On page factors can be set up within a single day. Getting your off page factors to the level you'd like will take far longer. But don't worry, with some perseverance, you can achieve a well-earned page ranking by addressing the following factors.</p>
	<h4>Backlinks and Inbound Traffic</h4>
	<p>Backlinks are heavily weighted in search engine algorithms because they indicate credibility for your site. Any opportunity you have to be linked from a credible site should be taken.</p>
	<p>When your site is linked to, there are a couple of factors search engines take into consideration:</p>
	<ul>
		<li>
			<h5>Relevance of Linked Text</h5>
			<p>The text that the link is attached to is important. Some content creators link text such as "click here" or "learn more here". Using this sort of vague text as links doesn't help your SEO as much as having the link on a word or phrase that actually describes what is being linked to.</p>
			<p>Say you have a page about how to care for large dogs. Here are two ways of linking to that page:</p>
			<ol>
				<li><u>Click here</u> to learn more about feeding and taking care of large dogs.</li>
				<li><u>Taking care of large dogs</u> can be a challenge.</li>
			</ol>
			<p>Whenever possible, make an effort to ensure people are linking to you in the most effective way possible. Your page ranking will thank you.</p>
		</li>
		<li>
			<h5>Age of Link</h5>
			<p>Links that exist for a long period of time are weighted more heavily than new links. Search engines consider links older than 3 months to be more credible than new ones.</p>
		</li>
	</ul>
	<h4>Things you can do</h4>
	<p>There are lots of ways to increase the number of inbound links to your site. Some are very easy, others take a bit more effort.</p>
	<h5>Build Your Profiles</h5>
	<p>Links from profile pages you've set up on social networks and directories are a great place to start when building backlinks. Facebook, Twitter, YouTube, LinkedIn, and Google Places are all must-haves.</p>
	<p>There is a handy tool called <a href="http://namechk.com">namechk.com</a> that allows you to search for your preferred username across a variety of social networks and directories. Use it and register as many as you feel are relevant.</p>
	<p>Depending on your industry, there are likely several directories that may be worth your attention. Some are paid like Angie's List. Others are free to get a listing, but might require an email to the moderator of the list.</p>
	<h5>Blogging</h5>
	<p>Running a blog effectively is a significant amount of effort. Aside from writing unique and interesting blog posts, actually getting people to read them and link to you is a challenge! Still, an effective blogging strategy is a great way to increase inbound links and continuously improve your SEO "juice".</p>
	<p>Another useful blogging tactic is to write guest posts for other blogs in your industry. Many blogs are always looking for new people to contribute quality content and plugging your business is generally accepted when writing a guest post. Just be sure to link back to your site in a way that supports your SEO strategy.</p>
	<h5>Press</h5>
	<p>Getting press is another effective way to get quality backlinks. Connect with people at your local newspaper. Connect with people at any relevant industry publications. People in the press are often very interested in new, meaningful concepts for stories. The art of getting press is to present them with ideas that they believe their readers will want to learn about, all while tying in your business.</p>
	<p>Assume you own a pet store. You could pitch a story to the local paper about any noteworthy changes in the number of animals up for adoption and how that relates to current economic situations. Read your publications of interest and see what sort of stories qualify as news for them and think about how to position your story at a similar level of importance.</p>
	<h5>Things to Avoid</h5>
	<p>When working hard to build a series of backlinks, an easy solution might appear to be to buy them. This is something called linkfarming, which is considered to be a way to game the search engine algorithms. By doing this, you risk getting penalized by the search engines. Keep in mind that linkfarming is different from doing search engine marketing, which is a legitimate way to purchase links to your site.</p>
	<h3>Measuring Your Progress</h3>
	<p>As with anything you'd like to improve, it's critical to measure your progress. Thankfully, that's easy to do with a powerful analytics solution like Google Analytics. Google Analytics is free, so it's available to anyone on any budget. Other platforms include their own analytics package, which work great as well. How you collect the data isn't as important as collecting it and monitoring it.</p>
	<h4>What You Should See</h4>
	<p>If you've successfully implemented several of the areas of focus earlier in this guide, there are several things you should start to see in your analytics reports.</p>
	<h5>Increased Keyword Inbound Traffic</h5>
	<p>Your analytics report should start to show more traffic coming to your site through the terms you optimized for. If several weeks have passed and you aren't seeing a noticeable improvement, you may need to reassess your SEO strategy. Maybe you haven't used the keywords effectively in your site, maybe you've chosen keywords that are too highly competitive, or maybe you need to work to increase your backlinks.</p>
	<h5>Increased Traffic</h5>
	<p>Your total site visits should increase both from inbound traffic from keyword searches and also from backlink traffic.</p>
	<h5>Lower Bounce Rate</h5>
	<p>The number of users that come to your site and leave it immediately helps determine the bounce rate. With a properly implemented SEO strategy, your users are more likely to be interested in your site's content. A lower bounce rate is important to be able to complete the next step: more customers</p>
	<h5>More Customers</h5>
	<p>This is what SEO is all about for small businesses. It's why you build a website and spend time optimizing it for search engines. Be sure to ask customers (or track them through analytics) to get a sense of how they found you. There is nothing more amazing than the feeling of people becoming customers all because of finding you through a search engine. Those customers have intent and were relatively inexpensive to market to!</p>
	<h4>Don't Give Up</h4>
	<p>If you don't see success right away, don't fret too much. Search engine optimization takes effort and can take tweaking to get to your maximum potential. The key it to keep going and recognize your successes when your tactics work, even at a small level. Keep learning and working and the big wins are sure to come.</p>
	<p><em>Ready to get started with your small business website and put some of these search engine optimization techniques to use? Sign up with Onepager and start building your website right now!</em></p>
</article >