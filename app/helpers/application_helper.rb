module ApplicationHelper
  def build_main_navigation(items)
    pagename = params[:section].blank? ? params[:pagename] : params[:section]
    items.collect { |item| pagename == item[1].gsub(/\/\S*\//, '') ? link_to(item[0], item[1], :class => 'on') : link_to(item[0], item[1]) }.join()
  end
  def build_sub_navigation(items)
    items.collect { |item| params[:pagename] == item[1].gsub(/\/\S*\//, '') ? link_to(item[0], item[1], :class => 'on') : link_to(item[0], item[1]) }.join()
  end
  
  def sub_nav_on_state( pagename )
    if params[:pagename] == pagename
      return ' class="on"'
    end
  end
  
  def google_webfonts( fonts = [] )
    collection = Site::FONTS['basic'] + Site::FONTS['fancy']
    collection = fonts.collect { |f| collection.assoc(f) }.compact if !fonts.blank?
    
    fonts = collection.collect { |f| (f[2] == 'normal') ? f[0] : (f[0].gsub(/Bold/, '').strip + ':' + f[2]) }
    fonts = fonts.collect { |n| n.gsub(/\s/, '+') }.join('|')
  end
  
  def font_select( fonts = [] )
    collection = []
    Site::FONTS.values.collect { |c| collection += c }
    fonts = collection if fonts.blank?
    fonts = fonts.collect { |f| f[0] }
  end
  
  def full_font_list
    collection = []
    fonts = Site::FONTS.values.collect { |c| collection += c }
    fonts = collection
  end

  def get_font_stack( font = 'Arial' )
    font_stack = Site::FONTS.values.collect { |set| set.assoc(font) }.compact.flatten[1]
  end
  
  def get_font_weight( font = 'Arial' )
    font_weight = Site::FONTS.values.collect { |set| set.assoc(font) }.compact.flatten[2]
  end
  
  def hex_to_rgb(hex)
    hex = hex.upcase
    hex_alpha = "0123456789ABCDEF"
    rgb = Array.new
    i = 0
    
    while i < 6 do
      rgb_uno = hex_alpha.index(hex[i])
      rgb_dos = hex_alpha.index(hex[i+1])
      rgb << (rgb_uno * 16) + rgb_dos
      i += 2
    end
    
    return rgb.join(', ')
  end
  
  def dashboard_navigation(menu)
    html = ""
    
    menu.each do |menu_item|
      if controller_name == menu_item[:controller]
        html += link_to(menu_item[:name], menu_item[:url], :class => menu_item[:class] + " on")
      else
        html += link_to(menu_item[:name], menu_item[:url], :class => menu_item[:class])
      end
    end
    return html
  end

  def account_menu_items
    arr = [ { :url => account_settings_path, :class => 'profile_link', :name => 'Profile', :controller => 'settings' } ]
    arr << { :url => account_users_path, :class => 'users_link', :name => 'Users', :controller => 'users' } if current_user.paying_user?
    arr << { :url => account_billing_path, :class => 'billing_link', :name => 'Billing', :controller => 'billings' } if current_user.paying_user?
    arr << { :url => account_affiliate_path, :class => 'affiliates_link', :name => 'Affiliates', :controller => 'affiliates' } if current_user.payment_plan != 'Child'
    arr << { :url => account_whitelabel_path, :class => 'whitelabel_link', :name => 'Private Label', :controller => 'whitelabels' } if current_user.paying_user?
    return arr
  end

  # convert the plain links into HTML anchor links (with <a> tags) and return the string back
  # uses String's gsub method for regular expression
  def display_content_with_links(text)
    text = text.gsub(/(http:\/\/[a-zA-Z0-9\/\.\+\-_:?&=]+)/) {|a| "<a href=\"#{a}\" target='_blank'>#{a}</a>"}
    # text = text.gsub(/@\w+/) {|a| "<a href=\"http://twitter.com/#{a}\" target='_blank'>#{a}</a>"}
    text = text.gsub(/@(\w+)/, "<a href=\"https://twitter.com/\\1\" target='_blank'>&#64;\\1</a>")
    text = text.gsub(/\B#(\w*[a-zA-Z]+\w*)/, "<a href=\"https://twitter.com/search?q=%23\\1&src=hash\" target='_blank'>&#35;\\1</a>")
  end
end