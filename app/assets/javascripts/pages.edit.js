//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require jquery.remotipart

//= require lib/modernizr
//= require lib/extentions

//= require lib/colorpicker
//= require lib/dropdown
//= require lib/toolbar

//= require_self

//= require pages/session
//= require pages/settings
//= require pages/helpers
//= require pages/compatibility
//= require pages/theme
//= require pages/widget
//= require pages/content
//= require pages/content.info
//= require pages/site

var Onepager = window.Onepager || {};
	Onepager.debug = false;

if( !Onepager.debug ) {
	$( window ).bind( 'beforeunload', function() { // Prompt the user to save if they leave without saving
		if( Onepager.Session.open ) {
			return "Some changes you made might not be saved.";
		}
	}); 
}