(function( Uno, $ ) {
	
	Uno.Users = (function() {
		
		return {
			add: function( params ) {
				Uno.Users.createPopup( $.extend(params, { title: 'Add a User' }) );
			},
			create: function( params ) {
				var new_user = $( '<div />' ).html( params['user'] ),
					call_to_action = $( params['call_to_action'] );
				
				var dialog = $( '.account_user_form' );
				
				dialog
					.html( params['confirm_message'] )
					.dialog( 'option', { 'title': 'New User' } );
				
				$( '.notice' ).remove();
				$( '.users article p:not(.call_to_action p)' ).remove();
				$('.users table').show().find( 'tbody' ).append( new_user.html() );
				$( '.call_to_action' ).html( call_to_action.html() );
			},
			edit: function( params ) {
				Uno.Users.createPopup( $.extend(params, { title: 'Edit User' }) );
			},
			update: function( params ) {
				var editted_user = $( params['user'] );
				
				$( '.account_user_form' ).dialog( 'close' );
				$( '.notice' ).remove();
				$( '.users_listing tr' ).filter( '#user_' + params['id'] ).html( editted_user.html() );
			},
			createPopup: function( params ) {
				var popup = $( params['content'] ),
					settings = {
						modal: true,
						title: params['title'],
						resizable: false,
						draggable: false,
						closeOnEscape: false,
						closeText: '',
						width: 535,
						position: ['center', 150],
						open: function() {
							$( window ).resize( function() {
								popup.dialog( 'option', { position: ['center', 150] });
							} );
						},
						close: function() {
							popup.remove();
						}
					};
					
				popup
					.appendTo( 'body' )
					.dialog( settings );
					
				popup
					.delegate( '.checkboxes li:eq(0) a', 'click', function( e ) {
						e.preventDefault();
						$( this ).parents( 'ul' ).find( 'input[type="checkbox"]' ).attr( 'checked', true )
					} )
					.delegate( '.form_actions a:not(a[href^="mailto:"])', 'click', function( e ) {
						e.preventDefault();
						popup.dialog( 'close' );
					} )
					.delegate( 'form', 'ajax:error', function( e, xhr, status, response ) {
						$( '.errors' ).remove();
						
						var message = xhr.status === 500 ? 'An error occurred. Refresh the page or click "cancel" at the end of this form.' : xhr.responseText,
							modal_errors = $( '<div class="errors" />' ).html( message );
							
						$( e.target ).before( modal_errors );
					} );
			}
		}
		
	})();
	
})( Onepager, jQuery );