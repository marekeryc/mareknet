( function( Uno, $ ) {
	
	Uno.Analytics = ( function() {
		
		return {
			show: function() {
				$( '.time_travel' ).bind( 'change', function() {
					$( this ).parents('form').submit();
				} );
			},
			update: function() {
				
			}
		};
		
	} )();

} )( Onepager, jQuery );