(function( Uno, $ ) {
	
	Uno.Site = (function() {
		
		return {
			edit: function(section) {
				
				Uno.Site[section]();
				
			},
			domains: function() {
				var container = $( '.check_domains' );
				
				container.find( 'input[name*="[purchased_domain]"]:not(:disabled)' ).domainchecker({
					url: '/signup/check_domain_availability'
				});
				
				container.delegate( 'input[name*="[domain_type]"]', 'change', function() {
					var selector = '.' + this.value,
						tabs = $( '.domain_tabs span' ),
						button = $( this ).filter( ':checked' ).parent( 'span' ),
						billing = $( '.payment_details' );
					
					container
						.find( 'section' ).hide()
						.filter( selector ).show();
						
					tabs
						.removeClass( 'selected' )
						.filter( button ).addClass( 'selected' );
					
					if( this.value == "new_custom_domain" && billing.length > 0 ) {
						billing.show();
					} else {
						billing.hide();
					}
				} );
				
				container.find( 'input[name$="[domain_type]"]:checked:not(:disabled)' ).trigger( 'change' );
			},
			settings: function() {
				var container = $( '.settings' );
				
				container.delegate( 'input[name="site[password_protected]"]', 'change', function() {
					$( this ).is(':checked') ? $( '.authorization' ).show() : $( '.authorization' ).hide();
				} );
				
				container.find( 'input[name="site[password_protected]"]' ).trigger( 'change' );
			}
		}
		
	})();
	
	
})( Onepager, jQuery );