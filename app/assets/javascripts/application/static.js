( function( Uno, $ ) {
	
	/* -------------------
	MISC PAGES STUFF
	------------------- */
	Uno.Static = ( function() {
		
		return {
			toggleBoth: function( e ) {
				e.preventDefault();
				
				var button = $( this ),
					toggles = $( '.toggles a' ),
					device = button.data('device');
					
				toggles
					.unbind( 'click' )
					.bind( 'click', function( e ) { e.preventDefault(); } )
					.filter('.on')
						.removeClass( 'on' )
						.addClass('disabled');
					
				button.addClass( 'on' );
			
				var devices = {
					mobile: function() {
						if( $('.mobile:hidden').length > 0 ) {
							Uno.Static.toggleLaptops( 'hide' );
						} else {
							Uno.Static.toggleMobile( 'show' );
						}
					},
					laptop: function() {
						if( $('.laptop:hidden').length > 0 ) {
							Uno.Static.toggleMobile( 'hide' );
						} else {
							Uno.Static.toggleLaptops( 'show' );
						}
					}
				};
				
				devices[device]();
				
			},
			toggleLaptops: function( view ) {
				var laptops = $( '.laptop' ),
					extend = typeof(extend) !== 'undefined' ? 1000 : 0,
					toggles = $( '.toggles a' );
				
				if( view === 'show' ) {
					laptops.show().css('margin-top', 370);
				}
				
				$.each( laptops, function( i, e ) {
					var margin = view === 'show' ? [0,30,30] : [370,370,370],
						delay = [0,150,300],
						order = [1,0,2],
						easing = view === 'show' ? 'easeOutBack' : 'easeInBack';
						
					setTimeout(function() {
						laptops.filter(':eq(' + order[i] + ')').show().animate({
							'margin-top': margin[i]
						}, 250, easing, function() {
							if( i === 2 ) {
								if( view === 'hide' ) {
									laptops.hide();
									Uno.Static.toggleMobile( 'show' );
								}
								if(view === 'show') {
									toggles
										.bind( 'click', Uno.Static.toggleBoth )
										.removeClass( 'disabled' );
								}
							}
						});
					}, delay[i])
				} );
			},
			toggleMobile: function( view ) {
				var mobiles = $( '.mobile' ),
					margin = view === 'show' ? [0,105,105,30,30] : [370,370,370,370,370],
					toggles = $( '.toggles a' );
					
				if( view === 'show' ) {
					mobiles.show().css({
						'margin-top': 370
					});
				}
				
				$.each( mobiles, function( i, e ) {
					var delay = [0,100,200,300,400],
						order = [2,3,1,4,0],
						easing = view === 'show' ? 'easeOutBack' : 'easeInBack';
	
					setTimeout(function() {
						mobiles.filter(':eq(' + order[i] + ')').show().animate({
							'margin-top': margin[i]
						}, 250, easing, function() {
							if( i === 4 ) {
								if( view === 'hide' ) {
									mobiles.hide();
									Uno.Static.toggleLaptops( 'show' );
								}
								if(view === 'show') {
									toggles
										.bind( 'click', Uno.Static.toggleBoth )
										.removeClass( 'disabled' );
								}
							}
						});
					}, delay[i])
				} );
			},
			show: function() {
				
				// GLOBAL
				// 1. Feedback links create popups
				Uno.Helpers.feedback();
				
				// 2. TOC jumps to section on same page
				$( '.main_introduction a[href*="#"]' ).bind( 'click', function( e ) {
					$( 'html, body' ).animate({ scrollTop: ($( this.href.match(/\#\S+$/)[0] ).position().top - 40) })
				} )
				
				
				// HOMEPAGE
				// 1. Intro animation
				var laptops = $( '.laptop' ),
					mobiles = $( '.mobile' ),
					toggles = $( '.toggles a' );
				
				mobiles.hide();
				laptops.show().css('margin-top', 370);
				toggles.bind( 'click', Uno.Static.toggleBoth );
				
				// 1.b Run initial animation 1 sec after page load
				setTimeout( function() {
					toggles.filter('[data-device="laptop"]').trigger('click');
				}, 1000 )
				
			}
		}
		
	} )();
	
	Uno.Video = ( function() {
		
		return {
			play: function() {
				var player = $f( $( '.agency_video iframe' )[0] ),
					dialog = $( '.agency_video' );
				
				$( '.play_video' ).click( function( e ) {
					e.preventDefault();
					dialog.dialog({
						modal: true,
						resizable: false,
						draggable: false,
						show: 'fade',
						width: 934,
						closeText: '',
						dialogClass: 'agency_video_dialog',
						open: function( e, ui ) {
							$( '.ui-widget-overlay' ).click( function() {
								dialog.dialog( 'close' );
							} );
							$( window ).resize( function() {
								dialog.dialog( 'option', {'position': ['center', 'center']} );
							} );
							setTimeout( function() { player.api( 'play' ); }, 800);
						},
						close: function() {
							player.api( 'pause' )
						}
					});
				} );
			}
		}
		
	} )();
	
} )( Onepager, jQuery );