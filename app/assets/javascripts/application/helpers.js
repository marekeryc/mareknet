( function( Uno, $ ) {

	/* -------------------
	APPLICATION HELPERS
	------------------- */
	
	Uno.Helpers = ( function() {
		
		return {
			displayLoader: function( fosho ) {
				var img = $( '<img src="/assets/pages/loader-doggie-style.gif" alt="" />' ),
					messages = ["Going the distance!", "Giving it our all!", "Hustlin'!", "Racing to the finish!", "Whoa, Nelly!", "Hold your horses!", "Who's walking who?", "We're giving it all she's got!"],
					message = messages[Math.floor(Math.random()*messages.length)],
					loader = $( '.loader' ).length > 0 ? $( '.loader, .loader_overlay' ) : $( '<div class="loader" /><div class="loader_overlay" />' ).appendTo( 'body' ).hide().filter( '.loader' ).html( img ).append( message );
	
				fosho || typeof( fosho ) === 'undefined' ? loader.fadeIn( 100 ) : loader.fadeOut( 100 );
			},
			udpateTotalPrice: function( params ) {
				// 1. Manipulate the price table
				var no_domain_registration = ['existing_custom_domain', 'no_custom_domain'],
					domain_registration = ['new_custom_domain'];
					
				if( $.inArray( params['item'], domain_registration ) > -1 ) $( '.register_domain' ).show();
				if( $.inArray( params['item'], no_domain_registration ) > -1 ) $( '.register_domain' ).hide();
					
				// 2. Check subscription price
				var isAnnual = (params['period'] && params['period'] == 'Yearly'),
					price = (params['subscription'] || parseInt( $( '.subscription .price' ).text().match(/\d+/)[0] )),
					subscription = isAnnual ? price * 12 : price,
					registriation = parseInt( $( '.register_domain:visible .price' ).text().match(/\d+/) ) || 0,
					percentage = (params['discount'] || $( '.coupon:visible .price' ).text().match(/\d+/) || 0),
					discount = (percentage * (subscription + registriation))/100,
					total = subscription + registriation - discount;
					
				if( discount > 0 ) {
					$( '.coupon' ).remove();
					$( '.register_domain, .subscription' ).last().after( '<tr class="coupon"><td>Discount</td><td class="price">' + percentage + '% Off</td></tr>' );
				}
				
				$( '.subscription .price' ).text( '$' + subscription );
				$( '.totals span' ).text( '$' + total );
			},
			feedback: function() {
				$( 'a[href*="feedback.onepagerapp.com"]' ).bind( 'click', function( e ) {
					e.preventDefault();
					
					UserVoice.showPopupWidget();
					$( '#uvw-dialog-close' ).bind( 'click', function() {
						UserVoice.hidePopupWidget();
					} );
				} )
			}
		}
		
	} )();
	
} )( Onepager, jQuery );