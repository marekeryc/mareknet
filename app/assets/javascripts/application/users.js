( function( Uno, $ ) {
	
	/* -------------------
	ACCOUNT CREATION
	------------------- */
	Uno.Users = ( function() {
		
		return {
			create: function() {
				Uno.Helpers.displayLoader( false );
				
				// PLANS SELECTION
				var plans = $( '.plans' ),
					hidden_frequency = $( 'input:hidden[name="user[payment_frequency]"]' );
				
				plans
					.delegate( '.limited_features', 'mouseenter mouseleave', function( e ) {
					
						var methods = {
							mouseenter: function() {
								plans.find( '.tooltip_bubbles' ).stop(true,true).fadeIn( 150 );
							},
							mouseleave: function() {
								plans.find( '.tooltip_bubbles' ).stop(true,true).fadeOut( 150 );
							}
						};
					
						methods[e.type]();
					
					} )
					.delegate( '.tabs a', 'click', function( e ) {
						e.preventDefault();
						
						var period = $( this ).data( 'period' ),
							plans = $( '.paid' ),
							prices = $.map( plans, function( e ) { return $(e).data( period ); } );
							
						$.each( plans, function( i, e ) {
							$( e ).find( '.plan_price strong' ).html( '<sup>$</sup>' + prices[i] );
							$( e ).find( 'input[name="user[payment_plan]"]' ).data( 'price', prices[i] );
							
							var button = $( e ).find( '.more, .start' );
							
							if( button.length > 0 ) {
								var old_href = button.attr('href');
							
								period == 'monthly' ? button.attr('href', old_href.replace(/Yearly/, 'Monthly')) : button.attr('href', old_href.replace(/Monthly/, 'Yearly'));
							}
						} );
							
						$( '.tabs a' ).removeClass( 'on' ).filter( this ).addClass( 'on' );
						
						hidden_frequency.val( period.capitalize() );
						$( 'input[name="user[payment_plan]"]:checked' ).trigger( 'change' );
					} )
					.delegate( 'input[name="user[payment_plan]"]', 'change', function() {
						Uno.Helpers.udpateTotalPrice( {
							subscription: $( this ).data( 'price' ),
							period: $( 'input[name="user[payment_frequency]"]' ).val()
						} );
					} );
					
				plans.find( '.tabs a.on' ).trigger( 'click' );
				
				// SIGN UP AND CHECKOUT
				var signup = $( '.signups' );
				
				signup.find( '.new_custom_domain input[name*="[purchased_domain]"]:not(:disabled)' ).domainchecker({
					url: '/signup/check_domain_availability'
				});
				
				signup
					.delegate( 'input[name*="[domain_type]"]', 'change', function() {
						var selector = '.' + this.value,
							tabs = $( '.domain_tabs span' ),
							button = $( this ).filter( ':checked' ).parent( 'span' ),
							billing = $( '.payment_details' ),
							container = $( '.check_domains' );
					
						container
							.find( 'section' ).hide()
							.filter( selector ).show();
						
						tabs
							.removeClass( 'selected' )
							.filter( button ).addClass( 'selected' );
					
						if( this.value == "new_custom_domain" && billing.length > 0 ) {
							billing.show();
						} else {
							billing.hide();
						}
						
						Uno.Helpers.udpateTotalPrice( { item: this.value } );
					} )
					.delegate( 'input[name="apply_code"]', 'click', function() {
						var classname = 'promo_code_response',
							build = $( '<span />' ).text( 'Invalid promo code' ).addClass( classname ),
							remove_response =  function() {
								signup.find( '.' + classname ).fadeOut( 100, function() {
									$( this ).remove();
								} )
							};
						
						signup.find( '.' + classname ).remove();
						
						$.ajax({
							url: '/signup/validate_promo_code',
							data: 'promo_code=' + signup.find( 'input[name*="[promo_code]"]' ).val(),
							success: function( data ) {
								Uno.Helpers.udpateTotalPrice( { discount: parseInt(data) } );
								
								build.text( 'You got ' + data + '%' )
								signup.find( 'input[name*="apply_code"]' ).after( build );
								setTimeout( remove_response, 2000 );
							},
							error: function( xhr, status, error ) {
								signup.find( 'input[name*="apply_code"]' ).after( build );
								setTimeout( remove_response, 2000 );
							}
						});
					} )
					.delegate( 'input#user_zip', 'keyup', function() {
						var zip = this.value;
						
						if( zip !== '' && zip.length > 4 ) {
							$.ajax({
								url: '/signup/get_city_state',
								data: 'zip=' + zip,
								success: function( data ) {
									$( 'input#user_city' ).val( data['city'] );
									$( 'input#user_state' ).val( data['state'] );
								}
							});
						}
					} )
					.delegate( 'input[name*="[security_code]"]', 'focus blur', function( e ) {
						var tooltip = $( this ).parents( 'fieldset' ).find( '.tooltip_bubbles' ),
							actions = {
								focusin: function() { tooltip.fadeIn( 'fast' ); },
								focusout: function() { tooltip.fadeOut( 'fast' ); }
							}[e.type]();
					} )
					.delegate( 'form', 'submit', function( e ) {
						Uno.Helpers.displayLoader( true );
					} );
				
				signup.find( 'input[name$="[domain_type]"]:checked:not(:disabled)' ).trigger( 'change' );
			}
		}
		
	} )();
	
} )( Onepager, jQuery );