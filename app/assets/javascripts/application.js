//= require jquery
//= require jquery-ui
//= require jquery_ujs

//= require lib/modernizr
//= require lib/extentions

//= require lib/dropdown
//= require lib/domainchecker

//= require_self

//= require application/helpers
//= require application/static
//= require application/users

var Onepager = window.Onepager || {};
	Onepager.debug = true;