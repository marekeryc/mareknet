( function( Uno, $ ) {
	
	
	/* ---------------------------
	REMINDER ALERT ABOUT SUPPORT
	---------------------------- */
	Uno.Support = (function() {
		
		return {
			show: function() {
				var disclaimer = $( '<div class="compatibility_dialog" />' ),
					build = '';
				
				build += '<p>We want to offer the best experience for our users by taking advantage of the greatest features in current web technologies. This is why <strong>older browsers</strong> such as Internet Explorer 8 do not support certain features within Onepager.</p>';
				build += '<p>We recommend upgrading to the most current version of <a href="https://www.google.com/chrome">Internet Explorer</a> or downloading the most current version of <a href="http://firefox.com">Firefox</a> or <a href="https://www.google.com/chrome">Google Chrome</a>.</p>';
				build += '<img src="/assets/pages/onboarding/logo-browsers.png" alt="" />';
				
				disclaimer
					.html( build )
					.dialog({
						modal: true,
						resizable: false,
						draggable: false,
						width: 700,
						closeText: '',
						position: ['center', 100],
						title: 'Get the best experience out of Onepager!',
						buttons: [ {
							text: "I'll take my chances",
							click: function() {
								disclaimer.dialog("close");
							}
						} ],
						open: function() {
							$( window ).resize( function() {
								disclaimer.dialog( 'option', { position: ['center', 100] });
							} );
						},
						close: function() {
							disclaimer.remove();
						}
					});
			},
			placeholder: function() {
				var placeholders = $( 'input[placeholder], textarea[placeholder]' );

				$.each( placeholders, function( i, e ) {
					var placeholder = $( e ).attr( 'placeholder' );

					if( e.value == '' ) {
						e.value = placeholder;
					}

					var field_focus = function() {
						if( this.value == placeholder ) {
							this.value = '';
						}
					};

					var field_blur = function() {
						if( this.value == '' ) {
							this.value = placeholder;
						}
					};

					$( e )
						.bind( 'focus', field_focus )
						.bind( 'blur', field_blur );
				} )
			}
		}
		
	})();
	
	
	/* ----------------------
	BACKGROUND SIZE SUPPORT
	---------------------- */
	Uno.BGSize = (function() {
		
		var container_styles = {
			'position': 'fixed',
			'z-index': '-1',
			'top': 0,
			'left': 0,
			'width': '100%',
			'height': '100%',
			'text-align': 'center',
			'overflow': 'hidden'
		};
		
		return {
			updateImageRatio: function( bg ) {
				var image = bg.find( 'img' ).width( 'auto' ).height( 'auto' ), // Reset dimensions
					image_ratio = image.width() / image.height(),
					view_ratio = $( window ).width() / $( window ).height();
					
				if( view_ratio < image_ratio )
					image.height( '100%' );
				else
					image.width( '100%' );
			},
			create: function() {
				var img = $('body').css('background-image').split('url(')[1].split(')')[0].replace( /\"/g, '' ),
					build = $( '<div id="background_image" />' ).html( $( '<img />' ).attr( 'src', img ) );
				
				build.css( container_styles ).prependTo( 'body' ); // cover
				Uno.BGSize.updateImageRatio( build );
				$( window ).resize( function() {
					Uno.BGSize.updateImageRatio( build );
				} );
        
        build.css({ 'left': '50%', 'margin-left': (build.find('img').width()/2)*-1 }); // Centers the image
			},
			destroy: function() {
				$( '#background_image' ).remove();
			}
		}
		
	})();
	
	/* ----------
	RGBA SUPPORT
	---------- */
	Uno.RGBA = ( function() {
		
		return {
			update: function( hex, opacity ) {
				var ieOpacity = opacity == 0 ? '00' : Math.floor(parseFloat(opacity) / 100 * 255).toString(16);
				return 'progid:DXImageTransform.Microsoft.Gradient(GradientType=1, StartColorStr="#' + ieOpacity + hex + '", EndColorStr="#' + ieOpacity + hex + '")';
			}
		}
		
	} )();
	
} )( Onepager, jQuery );