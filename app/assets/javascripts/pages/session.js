( function( Uno, $ ) {
	
	/* --------------------
	   SESSION SETTINGS
	--------------------- */
	Uno.Session = ( function() {
		
		return {
			open: false,
			fonts: [],
			uid: null,
			theme: {
				'id': '',
				'background_colour': '',
				'content_background_colour': '',
				'content_background_opacity': '',
				'background_image': '',
				'background_image_alignment': '',
				'background_image_enabled': false,
				'background_image_fixed': true,
				'header_type': '',
				'header_colour': '',
				'header_size': '',
				'subheader_type': '',
				'subheader_colour': '',
				'subheader_size': '',
				'body_type': '',
				'body_colour': '',
				'body_size': '',
				'layout_width': '',
				'layout_alignment': ''
			},
			create: function( sid, pid ) {
				// Get everything related to site theme
				$.ajax({
					url: '/sites/' + sid + '/pages/' + pid,
					type: 'GET',
					dataType: 'json',
					success: function(data, status, xhr) {
						$.extend( Uno.Session, data );
						$( '.toolbox' ).trigger( 'session:success', [data, status, xhr] );
					}
				});
			},
			update: function( params ) {
				if( !params['sync'] || typeof( params['sync'] ) !== 'undefined' ) return;
				
				$.extend( Uno.Session.theme, params['theme'] );
			}
		}
		
	} )();
	
} )( Onepager, jQuery );