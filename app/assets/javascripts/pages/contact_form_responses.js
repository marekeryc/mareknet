( function( Uno, $ ) {

	Uno.ContactFormResponses = ( function() {
		
		return {
			create: function() {
				var form = $( '.new_contact_form_responses' );
				
				form.bind( 'ajax:complete', function( e, xhr, status ) {
					Uno.Helpers.displayMessage( status, $.parseJSON( xhr.responseText )['message'] );
					
					if(status.toLowerCase() === 'success') setTimeout(function() { form[0].reset(); }, 1000);
				} );
				
				form.find( '.onepager_submit' )
					.show()
					.next( '.done_loader' ).remove();
			}
		}
		
	} )();

} )( Onepager, jQuery );