( function( Uno, $ ) {

	/* --------------------------------
	ONEPAGER THEME AND CONTENT HELPERS
	--------------------------------- */
	
	Uno.Helpers = ( function() {
		
		return {
			destroyContentListItem: function( item ) {
				var item = $( item ).parent( 'li' ),
					destroy = item.find( 'input' ).filter( function() { return (this.name).match(/\[_destroy\]/) !== null; } );
				
				item.slideUp( 50, function() {
					if( item.parents( '.content_list' ).find( '.upload_field' ).length < 1 ) // Uno.Uploads should all be deleteable
						Uno.Helpers.displayContentListItemDestroy( item.parents( 'ul' ) );
						
					destroy.length > 0 ? destroy.val( 'true' ) : item.remove();
				} );
			},
			createContentListItem: function( container ) {
				var og = container.find( 'li:visible:last' ),
					new_item = og.clone(),
					inputs = new_item.find( 'input, select' ),
					hidden = inputs.filter( function() { return ((this.name).match(/\[_destroy\]/) !== null) || (this.name).match(/\[id\]/) !== null; } ),
					copies = inputs.not( hidden ),
					index = $( 'li', container ).length;
						
				hidden.remove();
				
				/* Special hours select changes */
				if( copies.filter( 'select' ).length > 0 ) {
					
					new_item.find( '.jq_dropdown' ).remove(); // delete old styled dropdown
					
					var values = $.map( og.find( 'select' ), function( e, i ) {
						return $( 'option', e ).index( $( 'option:selected', e ) );
					} );
				
					if ( values[0] < copies.filter('select').find('option').length ) values[0] += 1;
				
					$.each( copies.filter( 'select' ), function( i, e ) {
						$( e )
							.attr( 'name', (e.name).replace( /\[\d+\]/, '[' + index + ']' ) )
							.find( 'option:eq(' + values[i] + ')' ).attr( 'selected', true );
						$( e ).dropdown();
					} );
					
				}
				
				if( copies.filter( 'input' ).length > 0 ){
					$.each( copies.filter( 'input' ), function( i, e ) {
						e.name = (e.name).replace( /\[\d+\]/, '[' + index + ']' );
						e.value = '';
						
						var social_media = {
							'facebook': 'http://facebook.com/zuck',
							'twitter': 'http://twitter.com/ev',
							'linkedin': 'http://linkedin.com/company/linkedin',
							'blog': 'http://googleblog.blogspot.com/',
							'tumblr': 'http://yahoo.tumblr.com/',
							'youtube': 'http://www.youtube.com/youtube',
							'vimeo': 'https://vimeo.com/channels/staffpicks',
							'pinterest': 'http://www.pinterest.com/pinterest/',
							'yelp': 'http://yelp.com/biz/landmark-coffee-shop-new-york',
							'foursquare': 'https://foursquare.com/v/beekman-lane/4d0ab56a6c51a14374e3a535',
              'google+': 'https://plus.google.com/+MattCutts/',
              'instagram': 'http://instagram.com/instagram',
              'vine': 'https://vine.co/dom',
              'medium': 'https://medium.com/@ev',
              'periscope': 'https://www.periscope.tv/kayvon'
						}[$(e).prev('.jq_dropdown').find('.selected').text().toLowerCase()];
						
						if( social_media ) e.placeholder = 'e.g. ' + social_media;
						
						if( e.type === 'checkbox' )
							$( e ).attr( 'id', 'checkbox_' + i ).next( 'label' ).attr( 'for', 'checkbox_' + i );
						else
							e.id = '';
							
					} );
				}
				
				new_item
					.appendTo( container ).hide()
					.slideDown( 50, function() {
						Uno.Helpers.displayContentListItemDestroy( new_item.parents( 'ul' ) );
					} );
			},
			displayContentListItemDestroy: function( container ) {
				var all = container.find( 'li' ),
					remaining = all.filter(':visible');
				
				if ( remaining.length === 1 )
					remaining.find( '.remove_item' ).hide();
				else
					all.find( '.remove_item' ).show();
			},
			sortableContentListItem: function( list ) {
				list.sortable({
						forcePlaceholderSize: true,
						containment: 'parent',
						tolerance: 'pointer',
						axis: 'y'
					});
			},
			sortableContentTypes: function( fosho ) {
				var sortables = $( '.main_content, .sidebar' ),
					settings = {
						items: 'section:not(.sample_box)',
						connectWith: '.main_content, .sidebar',
						forcePlaceholderSize: true,
						placeholder: 'ui-state-highlight',
						delay: 100,
						tolerance: 'pointer',
						start: function( e, ui ) {
							ui.placeholder.height( ui.helper.find('.editable').outerHeight() );
							ui.item.find( '.addable' ).hide();
						},
						sort: function( e, ui ) {
							ui.item.find( '.addable' ).hide();
						},
						update: function( e, ui ) {
							if(ui.item.parent().is( this )) {
								var container = ui.item,
									positions = $.map( Uno.Helpers.findAllPositions( container ), function( e, i ) {
										if (typeof(e.model) !== 'undefined') return '{ "model" : "' + e.model + '", "id" : "' + e.id + '", "position" : "' + e.position + '"}';
									} );
									
								$.ajax( {
									url: container.parents( 'article' ).find( 'header form' )[0].action + '/update_positions',
									type: 'POST',
									data: { "_method": "PUT", "order": positions },
									error: function( xhr, status, error ) {
										container.sortable('cancel');
									}
								} );
							
								Uno.Helpers.displaySampleBox();
								if( container.is('.text_areas') ) {
									Uno.Helpers.updateCKEditor( container.attr('id').match(/\d+/)[0] );
								}
							}
						}
					};
				
				sortables
					.sortable( settings )
					.sortable( 'enable' );
				
				if( fosho !== true && typeof( fosho ) !== 'undefined' ) sortables.sortable( 'disable' );
			},
			findAllPositions: function( item ) {
				var items = $.map( $( 'article section:not(.sample_box)' ), function( e, i ) {
					var is_main_content = $(e).parents('.main_content, .sidebar').is( '.main_content' ),
						main_content = $( '.main_content section:not(.sample_box):visible' ),
						sidebar = $( '.sidebar section:not(.sample_box):visible' );
					
					return {
						'model': $( e ).data( 'model-class' ),
						'id': e.id ? e.id.match(/\d+/)[0] : '',
						'position': is_main_content ? main_content.index( e ) : sidebar.index( e ) + 100
					};
				} );
				return items;
			},
			convertHexToRGBA: function( params ) {
				var hex = params['hex'].toUpperCase(),
					opacity = params['opacity'],
					hex_alpha = '0123456789ABCDEF',
					rgb = [], rgb_uno, rgb_dos;

				for(i = 0; i < 6; i++) {
					rgb_uno = hex_alpha.indexOf(hex.charAt(i));
					rgb_dos = hex_alpha.indexOf(hex.charAt(i + 1));
					rgb.push((parseInt(rgb_uno) * 16) + parseInt(rgb_dos));
					i += 1;
				}
				if(Modernizr.rgba) // rgba()
					return 'rgba(' + rgb.join(', ') + ', ' + parseFloat(opacity)/100 + ')';
				else // no rgba() support
					return Uno.RGBA.update( hex, opacity );
			},
			buildFakeUploader: function( inputs ) {
				$.each( inputs, function( i, e ) {
					var input = $( e );
					if( input.parent()[0].className === 'uploader' ) return;
				
					var text = (input[0].name).match( /\[]/ ) ? 'Upload files' : 'Upload file',
						button = $( '<span class="browse" />' ).text( text ),
						styles = input.attr( 'style' ),
						shell = $( '<span class="uploader" />' ).attr( 'style', styles );
				
					input
						.wrap( shell )
						.removeAttr( 'style' )
						.after( button );
				} );
			},
			updateTextAreas: function() {
				for ( instance in CKEDITOR.instances ) {
					CKEDITOR.instances[instance].updateElement();
				}
			},
			updateCKEditor: function( id, show ) {
				var textarea = id ? $( '#text_area_' + id ).find( 'textarea' ) : $( '.text_areas textarea' );
				
				if( id ) {
					var content = textarea.val();
					if( CKEDITOR.instances[textarea.attr('id')] ){
						CKEDITOR.instances[textarea.attr('id')].destroy();
						textarea.val(content);
					}
					if( show || typeof(show) === 'undefined' ) CKEDITOR.replace(textarea.attr('id'));
				} else {
					$.each( textarea, function( i, e ) {
						var content = e.value;
						if( CKEDITOR.instances[e.id] ){
							 CKEDITOR.instances[e.id].destroy();
							e.value = content;
						}
						if( show || typeof(show) === 'undefined' ) CKEDITOR.replace(e.id);
					} );
				}
			},
			displayLoader: function( fosho ) {
				var img = $( '<img src="/assets/pages/loader-doggie-style.gif" alt="" />' ),
					messages = ["Going the distance!", "Giving it our all!", "Hustlin'!", "Racing to the finish!", "Whoa, Nelly!", "Hold your horses!", "Who's walking who?", "We're giving it all she's got!"],
					message = messages[Math.floor(Math.random()*messages.length)],
					loader = $( '.loader' ).length > 0 ? $( '.loader, .loader_overlay' ) : $( '<div class="loader" /><div class="loader_overlay" />' ).appendTo( 'body' ).hide();
				
				fosho || typeof( fosho ) === 'undefined' ? loader.fadeIn( 100 ).filter( '.loader' ).html( img ).append( message ) : loader.fadeOut( 100 );
			},
			displaySaver: function( fosho ) {
				var actions = $( '.toolbox' ).find( '.form_actions' ),
					bubble = $( '.response_bubble' ).length > 0 ? $( '.response_bubble' ) : $( '<span class="response_bubble" />' );
					
				if( fosho || typeof( fosho ) === 'undefined' )
					bubble.text( 'Saving...' ).insertBefore( actions ).animate( { 'top': -50 }, 300 );
				else
					setTimeout( function() {
						bubble.text( 'All done!' ).delay(450).animate( { 'top': 4 }, 300, function() { $( this ).remove(); } );
					}, 500 );
			},
			displaySampleBox: function( fosho ) {
				var full_page = $( '.' + Uno.ClassNames.widths[2] ),
					main_content = full_page.find( '.main_content' ),
					sidebar = full_page.find( '.sidebar' ),
					visible = typeof( fosho ) !== 'undefined' ? fosho : (sidebar.find( 'section:not(.sample_box):visible' ).length === 0 && full_page.length > 0);
					
				var editable = $( '<div class="editable" />' ).html( '<p>Drag or add content here</p>' ),
					addable = $( '<div class="addable" />' ).hide().html( '<a href="#" class="form_add">+ Add Content</a>' ),
					box = $( '.sample_box' ).length > 0 ? $( '.sample_box' ) : $( '<section class="sample_box" />' ).html( editable ).append( addable ).hide().prependTo( sidebar ),
					height = full_page.length > 0 ? ( main_content.height() > sidebar.height() ? main_content.height() : sidebar.height() ) - box.height() : 0;
			
				if( visible )
					box.delay( 100 ).slideDown( 100 );
				else
					box.hide();
					
				$( '.main_content, .sidebar' ).css( 'min-height', height );
			},
			displayErrors: function( message, target ) {
				var message = $( '<p />' ).text( message ),
					form = $( target ),
					build = form.find( '.content_type_errors' ).length > 0 ? form.find( '.content_type_errors' ) : $( '<div class="content_type_errors" />' );
				
				build
					.hide()
					.html( message )
					.insertBefore( form.find( '.form_actions' ) )
					.stop().slideDown( 200 );
			},
			displayThemeError: function( message ) {
				var message = $( '<p />' ).text( message ),
					build = $( '.toolbox' ).find( '.theme_error' ).length > 0 ? $( '.toolbox' ).find( '.theme_error' ) : $( '<div class="theme_error" />' );
				
				build
					.hide()
					.html( message )
					.prependTo( '.toolbox' )
					.stop().fadeIn( 100 );
					
				build.css( 'margin-left', build.outerWidth() / 2 * -1 );
				
				setTimeout( function() { build.fadeOut( 200, function() { $(this).remove(); } ); }, 4000 )
			},
			findFontFamily: function( font ) {
				return $.map( Uno.Session.fonts, function( e ) {
					if( $.inArray( font, e ) >= 0 ) return e;
				} )[1];
			},
			findFontWeight: function( font ) {
				return $.map( Uno.Session.fonts, function( e ) {
					if( $.inArray( font, e ) >= 0 ) return e;
				} )[2];
			},
			updateThemeStyles: {
				layout_width: function( params ) {
					var classname = {
							450: Uno.ClassNames.widths[0],
							650: Uno.ClassNames.widths[1],
							835: Uno.ClassNames.widths[2]
						}[params['value']];
						
					$( 'body' ).removeClass().addClass( classname );
					Uno.Helpers.displaySampleBox();
					
					return [
						{ 'selector': (params['selector'] || 'article'), 'property': 'width', 'style': params['value'] }
					];
				},
				layout_alignment: function( params ) {
					var margin = params['value'] === 'none' ? 'auto' : 25;
					return [
						{ 'selector': (params['selector'] || 'article'), 'property': 'margin-left', 'style': margin },
						{ 'selector': (params['selector'] || 'article'), 'property': 'margin-right', 'style': margin },
						{ 'selector': (params['selector'] || 'article'), 'property': 'float', 'style': params['value'] }
					];
				},
				content_background: function( hex, opacity, selector ) {
					var property = Modernizr.rgba ? 'background-color' : 'filter';
					return [
						{ 'selector': (selector || 'article'), 'property': property, 'style': Uno.Helpers.convertHexToRGBA( { hex: hex, opacity: opacity } ) }
					]
				},
				content_background_opacity: function( params ) {
					return Uno.Helpers.updateThemeStyles['content_background']( $( 'input[name="site[content_background_colour]"]' ).val(), parseInt(params['value']), params['selector'] );
				},
				content_background_colour: function( params ) {
					return Uno.Helpers.updateThemeStyles['content_background']( params['value'], $( 'input[name="site[content_background_opacity]"]' ).val(), params['selector'] );
				},
				background_colour: function( params ) {
					return [
						{ 'selector': (params['selector'] || 'body'), 'property': 'background-color', 'style': '#' + params['value'] }
					];
				},
				background_image_alignment: function( params ) {
					var size = params['value'] === 'cover' ? params['value'] : 'auto',
						repeat = params['value'] === 'center' ? 'no-repeat' : 'repeat';
					
					if( Modernizr.backgroundsize ) {
						return [
							{ 'selector': (params['selector'] || 'body'), 'property': 'background-size', 'style': size },
							{ 'selector': (params['selector'] || 'body'), 'property': '-moz-background-size', 'style': size },
							{ 'selector': (params['selector'] || 'body'), 'property': '-webkit-background-size', 'style': size },
							{ 'selector': (params['selector'] || 'body'), 'property': 'background-repeat', 'style': repeat }
						];
					} else {
						size === 'cover' ? Uno.BGSize.create() : Uno.BGSize.destroy();
						return [
							{ 'selector': (params['selector'] || 'body'), 'property': 'background-repeat', 'style': repeat }
						];
					}
				},
				background_image_enabled: function( params ) { // TODO use background_image
					if( !Modernizr.backgroundsize ) Uno.BGSize.destroy();
					var image = params['value'] ? params['value'] : 'none';
					
					return [
						{ 'selector': (params['selector'] || 'body'), 'property': 'background-image', 'style': image }
					];
				},
				background_image_fixed: function( params ) {
					return [
						{ 'selector': (params['selector'] || 'body'), 'property': 'background-attachment', 'style': (params['value'] ? 'fixed' : 'scroll') }
					];
				},
				background_image: function( params ) {
					// TODO modernizr stuff
					var image = params['value'] ? 'url('+ params['value'] +')': 'none';
					
					return [
						{ 'selector': (params['selector'] || 'body'), 'property': 'background-image', 'style': image }
					];
				},
				header_size: function( params ) {
					return [
						{ 'selector': (params['selector'] || 'h1'), 'property': 'font-size', 'style': parseInt(params['value']) }
					];
				},
				subheader_size: function( params ) {
					return [
						{ 'selector': (params['selector'] || 'h2'), 'property': 'font-size', 'style': parseInt(params['value']) },
						// { 'selector': 'h3', 'property': 'font-size', 'style': (parseInt(params['value']) * 0.65) }
					];
				},
				body_size: function( params ) {
					return [
						{ 'selector': (params['selector'] || 'body'), 'property': 'font-size', 'style': parseInt(params['value']) }
					];
				},
				header_colour: function( params ) {
					return [
						{ 'selector': (params['selector'] || 'h1'), 'property': 'color', 'style': '#' + params['value'] }
					];
				},
				subheader_colour: function( params ) {
					return [
						{ 'selector': (params['selector'] || 'h2,h3'), 'property': 'color', 'style': '#' + params['value'] }
					];
				},
				body_colour: function( params ) {
					return [
						{ 'selector': (params['selector'] || 'body'), 'property': 'color', 'style': '#' + params['value'] }
					];
				},
				header_type: function( params ) {
					return [
						{ 'selector': (params['selector'] || 'h1'), 'property': 'font-family', 'style': Uno.Helpers.findFontFamily( params['value'] )  },
						{ 'selector': (params['selector'] || 'h1'), 'property': 'font-weight', 'style': Uno.Helpers.findFontWeight( params['value'] )  }
					];
				},
				subheader_type: function( params ) {
					return [
						{ 'selector': (params['selector'] || 'h2,h3'), 'property': 'font-family', 'style': Uno.Helpers.findFontFamily( params['value'] )  },
						{ 'selector': (params['selector'] || 'h2,h3'), 'property': 'font-weight', 'style': Uno.Helpers.findFontWeight( params['value'] )  }
					];
				},
				body_type: function( params ) {
					return [
						{ 'selector': (params['selector'] || 'body'), 'property': 'font-family', 'style': Uno.Helpers.findFontFamily( params['value'] )  },
						{ 'selector': (params['selector'] || 'body'), 'property': 'font-weight', 'style': Uno.Helpers.findFontWeight( params['value'] )  }
					];
				},
				all: function( params ) {
					Uno.Helpers.updateThemeFields( params );
					
					var all = $.map( params, function( e, i ) {
						
						if( typeof( Uno.Helpers.updateThemeStyles[i] ) !== 'undefined' ) {
							if( i !== 'content_background_colour' && i !== 'content_background_opacity' ) {
								return Uno.Helpers.updateThemeStyles[i]( { 'name': i, 'value': e } );
							}
						}
						
					} );
					
					$.merge( all, Uno.Helpers.updateThemeStyles['content_background']( params['content_background_colour'], params['content_background_opacity'] ) )
					return all;
				}
			},
			updateThemeID: function( id, params ) {
				var new_id = id === 'custom' ? '' : id,
					old_id = Uno.Session.theme['id'],
					was_preset = !(old_id === '' || isNaN(Number(old_id))),
					is_preset = !(id === '' || isNaN(Number(id)));
				
				$( '.theme input[name="site[theme_id]"]' ).val( new_id );
				$.extend( Uno.Session.theme, { id: new_id } );
				
				if( id === 'custom' ) return;
				
				if( was_preset && !is_preset ) {
					var target = $( '.theme_collection a' ).filter( function( index ) { 
						var _regex = new RegExp( "themes\/" + old_id );
						return this.href.match(_regex);
					} );
					
					$.ajax( {
						url: target.attr('href'),
						success: function(data, status, xhr) {
							var new_params = { id: '' };
							new_params[params['name']] = params['value'];
							$.extend( Uno.Session.theme, data['theme'], new_params );
						}
					} );
				}
			},
			updateThemeFields: function( params ) {
				var containers = $( 'fieldset.styles, fieldset.layout' ),
					inputs = $.map( containers.find( 'input[name]:not([name="commit"]):not([type="radio"]):not([type="checkbox"]):not([type="file"])' ), function( e, i ) { return e.name.match(/\[.*\]/)[0].replace(/\W/g, ''); } ),
					checkboxes = $.map( containers.find( 'input[type="checkbox"][name]' ), function( e, i ) { return e.name.match(/\[.*\]/)[0].replace(/\W/g, ''); } ),
					dropdowns = $.map( containers.find( 'select[name]' ), function( e, i ) { return e.name.match(/\[.*\]/)[0].replace(/\W/g, ''); } ),
					scrollbars = ['content_background_opacity', 'header_size', 'subheader_size', 'body_size'],
					colors = ['background_colour', 'header_colour', 'subheader_colour', 'body_colour', 'content_background_colour'];
			
				$.each( params, function( name, value ) {
					if( $.inArray( name, inputs ) > -1 ) {
						containers.find('input[name$="[' + name + ']"]').val( value );
					}
					if( $.inArray( name, checkboxes ) > -1 ) {
						containers.find('input[name$="[' + name + ']"]').val( value ).attr( 'checked', value );
					}
					if( $.inArray( name, dropdowns ) > -1 ) {
						containers.find( 'select[name$="[' + name + ']"]' ).dropdown( 'update', value );
					}
					if( $.inArray( name, scrollbars ) > -1 ) {
						$( '.scrollbar[data-name="' + name + '"]' ).slider( 'value', parseInt(value) );
					}
					if( $.inArray( name, colors ) > -1 ) {
						$( '.colorpicker[data-field="' + name + '"]' ).colorpicker( 'update', value );
					}
				} );
				
				// For image param
				var background_image = (params['background_image'] && params['background_image_enabled']) ? window.location.protocol + '//' + window.location.host + params['background_image'] : '';
				$( 'input[type="hidden"][name*="[remote_background_image_url]"]' ).val( background_image );
			},
			showUpdateDomainReminder: function( temp ) {
				var disclaimer = $( '.reminder_dialog' );
				
				if( disclaimer.length > 0 ) {
					disclaimer
						.dialog({
							modal: true,
							resizable: false,
							draggable: false,
							width: 600,
							closeText: '',
							position: ['center', 100],
							title: 'Create a custom URL!',
							buttons: [ {
								text: "Change it later",
								click: function() {
									disclaimer.dialog("close");
								}
							} ],
							open: function() {
								$( window ).resize( function() {
									disclaimer.dialog( 'option', { position: ['center', 100] });
								} );
							},
							close: function() {
								disclaimer.remove();
							}
						});
				}
			},
			showGumroadMessage: function() {
				var message_dialog = $('.gumroad_message');
	
				if( message_dialog.length > 0 ) {
					message_dialog.dialog({
						modal: true,
						title: 'Selling with Onepager',
						resizable: false,
						draggable: false,
						show: 'fade',
						closeText: '',
						closeOnEscape: false,
						width: 505,
						position: ['center', 100],
						buttons: [ {
								text: "OK, that’s easy",
								click: function() {
									message_dialog.dialog("close");
								}
						} ],
						open: function() {
							$( window ).resize( function() {
								message_dialog.dialog( 'option', { position: ['center', 100] });
							} );
						},
						close: function() {
							message_dialog.remove();
						}
					});
				}
			}
		}
		
	} )();
	
} )( Onepager, jQuery );