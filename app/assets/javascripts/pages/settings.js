( function( Uno, $ ) {
	
	/* -------------------
	GLOBAL EASING SETTINGS
	------------------- */
	Uno.Easing = {
		out: 'easeInOutSine',
		yin: 'easeInOutSine'
	};
	
	Uno.ClassNames = {
		widths: ['page_narrow', 'page_standard', 'page_wide']
	};
	
} )( Onepager, jQuery );