( function( Uno, $ ) {
	
	/* ------------------------
	CREATE, EDIT Theme
	------------------------ */
	Uno.Theme = (function() {
		
		/* jQuery UI SETTINGS */
		
		var colorpicker_settings = {
			set: [
					'ff0100', 'fffc00', '03fc14', '00ffff', '002bff', 'ff2bff', 'ffffff', 'ebebeb', 'e1e1e1', 'd7d7d7', 'cccccc', 'c2c2c2', 'b7b7b7', 'acacac', 'a0a0a0', '959595',
					'ed1c24', 'fff200', '00a651', '00aef0', '2e3092', 'ed138c', '898989', '7d7d7d', '707070', '626262', '555555', '464646', '363636', '262626', '111111', '000000',
					'f69679', 'f9ad81', 'fdc68a', 'fff89a', 'c4df9b', 'a3d49d', '83ca9c', '7bcdc7', '6dcff6', '7da7d8', '8594ca', '8882be', 'a086bd', 'bc8dbf', 'f49ac1', 'f6989d', 
					'f26c4e', 'f68e55', 'faae5c', 'fff567', 'acd372', '7cc576', '3cb878', '1bbbb4', '00bff3', '448ccb', '5574b8', '5f5ca8', '855fa8', 'a863a8', 'f06ea9', 'f16d7d', 
					'ed1c24', 'f26522', 'f8951e', 'fff200', '8dc740', '39b44a', '00a550', '00a99d', '00aef0', '0072bc', '0055a7', '2f3192', '652c91', '922790', 'ec128b', 'ed145a', 
					'9e0a0f', 'a0410d', 'a36209', 'aba000', '598627', '197b30', '007236', '00746a', '0076a3', '004b80', '003471', '1b1464', '440e61', '620960', '9e095e', '9e043a', 
					'790000', '7b2e00', '7d4900', '837b00', '406619', '005e20', '005926', '005952', '005b7e', '003664', '002157', '0c054c', '32064b', '4b0549', '7b0546', '7a0226', 
					'c7b299', '988575', '736358', '534741', '362f2c', 'c69d6e', 'a67c51', '8c633a', '754d24', '603813'
			],
			onselect: function(trigger, color) {
				var attributes = $( trigger ).data(),
					is_content = attributes['field'].match( /^content/ ), 
					is_background = attributes['field'].indexOf( 'background' ) >= 0;
				
				// 1. Set button background color
				$( trigger ).find('a').css( 'background-color', '#' + color );
				
				// 2. Set element styles
				Uno.Theme.update( {
					selector: attributes['element'],
					name: attributes['field'],
					value: color
				} );
			}
		};
		var slider_settings = {
			slide: function( e, ui ) {
				var attributes = $( e.target ).data();
				
				Uno.Theme.update( {
					selector: attributes['element'],
					name: attributes['name'],
					value: ui.value
				} );
			},
			stop: function( e, ui ) {
				var attributes = $( e.target ).data();
				
				Uno.Theme.update( {
					selector: attributes['element'],
					name: attributes['name'],
					value: ui.value
				} );
			}
		};
				
		return {
			show: function( option ) {
				var container = $( '.toolbox' ),
					tabs = container.find( 'nav a:not(.button_show):not(.upgrade)' ),
					current = tabs.filter( '.on' ),
					section = typeof( option ) === 'boolean' ? ( current.length > 0 ? current.data( 'section' ) : 'theme' ) : option,
					clicked = tabs.filter( function( i ) { return $( this ).data( 'section' ) === section; } );
				
				// Switch tab on state
				current.removeClass( 'on' );
				clicked.addClass( 'on' );
				
				// Switch viewable fieldset
				container.find( 'fieldset').hide().filter( '.' + section ).show();
					
				// Slide up or down
				if( typeof( option ) !== 'boolean' ) option = true;
				
				var settings = option ? { 'height': 189, 'antonym': 'hide' } : { 'height': 39, 'antonym': 'show' },
					button = container.find( '.button_show, .button_hide' );
				
				container.animate( { 'height': settings['height'] }, 250 );
				button
					.removeClass()
					.addClass( 'button_' + settings['antonym'] );
			},
			edit: function( sid, pid ) {
				var container = $( '.toolbox' );
				
				/* Disable global ajax events */
				$.ajaxSetup({ global: false });
				
				/* Store theme settings */
				Uno.Session.create( sid, pid );
				
				/* Show user how we raise the bar */
				setTimeout( function() { Uno.Theme.show( true ); }, 300 );
				
				/* Create some UI elements */
				$('.colorpicker').colorpicker( colorpicker_settings );
				
				$( '.toolbox select' ).dropdown({
					natural: function() {
						return this.name.indexOf('background_image_alignment') >= 0;
					}
				});
				
				Uno.Helpers.buildFakeUploader( $( 'input[type="file"][name="site[background_image]"]' ) );
				
				$.each( $( '.scrollbar' ), function( i, e ) {
					var is_opacity = $( e ).data( 'element' ) === 'article',
						is_body = $( e ).data( 'element' ) === 'body';
					
					$( e ).slider( $.extend( {}, slider_settings, {
						value: $( e ).data( 'value' ),
						min: is_opacity ? 0 : 9,
						max: is_opacity ? 100 : 120
					} ) );
				} );
				
				/* EVENT DELEGATION FOR UI INTERACTIONS IN THEME EDITOR */
				container
				
					// 0. Before all else, set UI elements with new Session info
					.bind( 'session:success', function( data, status, xhr ) {
						var session = Uno.Session;
						
						if( parseInt( session.theme.layout_width ) === 835 ) { // After we get the response, set width stuff
							$( 'body' ).addClass( Uno.ClassNames.widths[2] );
							Uno.Helpers.displaySampleBox();
						}
						
						$.each( $( '.fontsets .jq_dropdown a' ), function( i, e ) {
							$( e ).css( {
								'font-family': Uno.Helpers.findFontFamily( $( e ).data( 'value' ) ),
								'font-weight': Uno.Helpers.findFontWeight( $( e ).data( 'value' ) )
							} );
						} )
					} )
					
					// 1. Theme editor show/hide and tabs
					.delegate( 'nav a:not(.button_show, .button_hide, .upgrade)', 'click', function( e ) {
						e.preventDefault();
						Uno.Theme.show( $( this ).data( 'section' ) );
					} )
					.delegate( '.button_show, .button_hide', 'click', function( e ) {
						e.preventDefault();
						Uno.Theme.show( this.className === 'button_show' );
					} )
					.delegate( '.save_page', 'click', function() {
						Uno.Helpers.displaySaver( true );
						Uno.Theme.show( true );
					} )
					.delegate( '.close_page', 'click', function() {
						Uno.Helpers.displaySaver( true );
						$( this ).parents('form').attr( 'data-dashboard-url', '/dashboard' )
					} )
					.delegate( 'nav a.upgrade', 'click', function( e ) {
						e.preventDefault();
						Uno.Helpers.displaySaver( true );
						$( this ).parents('form')
							.attr( 'data-upgrade-url', this.href )
							.trigger( 'submit.rails' );
					} )
					
					// 2. Theme selector scrolling
					.delegate( '.theme_limiter a', 'click', function( e ) {
						e.preventDefault();
						
						if( !Uno.Theme.spazzclick )
							Uno.Theme.spazzclick = true;
						else
							return;
						
						/* selectors */
						var direction = $( this ).data( 'direction' ),
							list = $( '.theme_collection ul' ),
							items = list.find( 'li' );
						
						/* math */
						var grid = items.filter( ':first' ).outerWidth( true ),
							max = ( items.length * 90 - list.parent().width() ) / 90,
							jump = 5 > max ? max : 5,
							distance = parseInt( list.css( 'margin-left' ) ) + ( grid * jump * -1 );
						
						/* animation */
						var movement = {
							left: function() {
								items.filter(':gt(' + ( items.length - jump ) + ')').prependTo( list );
								list
									.css( { 'margin-left': distance } )
									.animate( { 'margin-left': 0 }, 400, function() {
										Uno.Theme.spazzclick = false;
									} );
							},
							right: function() {
								list.animate( { 'margin-left': distance }, 400, function() {
									items.filter(':lt('+jump+')').appendTo( list );
									list.css( { 'margin-left': 0 } );
									Uno.Theme.spazzclick = false;
								} );
							}
						};
						
						movement[direction]();
					} )
					
					// 3a. Theme selector hovering
					.delegate( '.theme_collection a:not([data-id="custom"])', 'mouseenter mouseleave', function( e ) {
						
						var thumb = $( this ),
							screenshot = thumb.find( 'img' ).clone(),
							build = $( '.theme_tooltip' ).length > 0 ? $( '.theme_tooltip' ) : $( '<div class="theme_tooltip" />' );
						
						var tooltip = {
							mouseenter: function() {
								build
									.hide()
									.prependTo( 'fieldset.theme' )
									.html( screenshot );
									
								var left = ( thumb.parents('.theme_collection').position().left + thumb.position().left );
									left += ( thumb.outerWidth(true) / 2 );
									left -= ( ( thumb.find('img').width() + parseInt( build.css('padding-left') ) + parseInt( build.css('padding-right') ) ) / 2 );
									
								build
									.css( 'left', left )
									.stop( true, true ).delay( 200 ).fadeIn( 150, Uno.Easing.yin );
							},
							mouseleave: function() {
								build.stop( true, true ).fadeOut( 100, Uno.Easing.out );
							}
						};
						
						tooltip[e.type]();
						
					} )
					
					// 3b. Theme editor preset selection
					.delegate( '.theme_collection a', 'ajax:success', function( e, data, status, xhr ) {
						$( '.theme_tooltip' ).delay(100).fadeOut( 100, Uno.Easing.out );
						Uno.Theme.update( $.extend( {}, data['theme'], { id: data['theme']['id'], sync: false } )  );
					} )
					.delegate( '.theme_custom', 'click', function( e ) {
						e.preventDefault();
						$( '.theme_tooltip' ).delay(100).fadeOut( 100, Uno.Easing.out );
						$( '.button_styles' ).trigger( 'click' );
						Uno.Theme.update( $.extend( {}, Uno.Session.theme, { id: 'custom', sync: false } ) );
					} )
					
					// 4. Layout width and alignment changing
					.delegate( '.layout_settings a', 'click', function( e ) {
						e.preventDefault();
						
						var button = $( this ),
							attributes = button.data();
							
						Uno.Theme.update( {
							selector: 'article',
							name: attributes['name'],
							value: attributes['value']
						} );
						
						button.parents( '.layout_settings' ).find( 'a' ).removeClass( 'enabled' );
						button.addClass( 'enabled' );
					} )
					
					// 5. All interactions with select dropdowns
					.delegate( 'select', 'dropdown:change', function() {
						Uno.Theme.update( {
							selector: $( this ).data( 'element' ),
							name: this.name.substring( this.name.indexOf('[') + 1, this.name.indexOf(']') ),
							value: this.value
						} );
					} )
					
					// 6a. Toggling between background image uploader or alignment settings
					.delegate( '.disable_background_image', 'click', function( e ) {
						e.preventDefault();
						
						$( '.background_body .jq_dropdown, .background_body p' ).hide();
						$( '.background_body .uploader' ).show();
						
						Uno.Theme.update( {
							selector: 'body',
							name: 'background_image_enabled',
							value: false
						} );
					} )
					
					// 6b. Toggling between fixed or not fixed background-attachment
					.delegate( 'input[name="site[background_image_fixed]"]', 'change', function( e ) {
						Uno.Theme.update( {
							selector: 'body',
							name: 'background_image_fixed',
							value: this.checked
						} );
					} )
					
					// 7. When a new background image has been selected
					.delegate( 'input:file[name="site[background_image]"]', 'change', function() {
						// AJAX submitting the form with just the file upload
						var form = $( this ).parents( 'form' );
						
						form
							.addClass( 'uploading_background' )
							.find( 'fieldset input:not(:file), fieldset select' ).attr( 'disabled', true )
							.filter( '[name="site[background_image_enabled]"]' ).attr( 'disabled', false ).val( 'true' );
							
						form.find( 'input[name="site[remote_background_image_url]"]' ).attr( 'disabled', false ).val('');
							
						form.trigger( 'submit.rails' );
						
						$( '.background_body .uploader' ).hide();
						$( '.background_body .jq_dropdown, .background_body p' ).show();
						
						Uno.Theme.update({
							selector: 'body',
							name: 'background_image_enabled',
							value: true
						});
					} )
					
					// 8a. Also save all unsaved content
					.delegate( 'form', 'ajax:beforeSend', function( e ) {
						if( !$( e.target ).is('.uploading_background') ) {
							return Uno.Content.updateAll();
						}
					} )
					
					// 8b. Figure out if new user or existing when saving
					.delegate( 'form', 'ajax:success', function( e, data, status, xhr ) {
						if( e.target.tagName.toLowerCase() === 'form' && !$( e.target ).is('.uploading_background') && Uno.Session.fresh ) {
							$( window ).unbind( 'beforeunload' );
							window.location = '/signup';
						}
						else if( $( e.target ).is('form[data-upgrade-url]') ) {
							$( window ).unbind( 'beforeunload' );
							window.location = $( e.target ).data('upgrade-url');
						}
						else if( $( e.target ).is('form[data-dashboard-url]') ) {
							$( window ).unbind( 'beforeunload' );
							window.location = $( e.target ).data('dashboard-url');
						}
						else {
							Uno.Helpers.displaySaver( false );
						}
					} )
					
					// 8c. Brings the form back to previous state after form upload
					.delegate( 'form', 'ajax:complete', function( e, xhr, status ) {
						$(this)
							.removeClass( 'uploading_background' )
							.find( 'fieldset input, fieldset select' ).attr( 'disabled', false )
							.filter( ':file' ).val( '' );
					} )
					
					// 9. 500 vs 422 Error treatment
					.delegate( 'form', 'ajax:error', function( e, xhr, status, response ) {
						var message = xhr.status === 500 ? 'An error occurred. Save all content and refresh the page.' : xhr.responseText;
						
						$( '.response_bubble' ).delay(450).animate( { 'top': 4 }, 200, function() {
							$( this ).remove();
							Uno.Helpers.displayThemeError( message );
						} );
					} );
				
				$( 'body' ).ajaxError( function( e, xhr, settings, response ) {
					if( response == 'canceled' ) return;
					
					$( '.response_bubble' ).delay(450).animate( { 'top': 4 }, 200, function() {
						$( this ).remove();
						Uno.Helpers.displayThemeError( "Some of your content contains errors.  Please fix before saving again." );
					} );
					$( 'article' ).data( 'status', 'errors' );
				} );
				
				$( 'body' ).ajaxStop( function() {
					if( $( 'article' ).data( 'status' ) != 'errors' ) {
						setTimeout( function() {
							$( '.toolbox form' ).trigger( 'submit.rails' );
						}, 250 ); // Delay is for content modules to fadeout first
					}
					
					$.ajaxSetup({ global: false });
				} );
			},
			update: function( params ) {
				var sync = typeof( params['sync'] ) === 'undefined' ? true : params['sync'],
					type = typeof( params['name'] ) === 'undefined' ? 'all' : params['name'],
	 				styles = Uno.Helpers.updateThemeStyles[type]( params );
				
				Uno.Helpers.updateThemeID( (params['id'] || ''), params );
				
				$( '.styles input[type="hidden"], .layout input[type="hidden"]' )
					.filter( function() {
						return this.name.indexOf( '[' + params['name'] + ']' ) >= 0;
					} )
					.val( params['value'] );
				
				$.each( styles, function( i, style ) {
					$( style['selector'] ).css( style['property'], style['style'] );
				} );
				
				Uno.Session.open = true;
				
				if( sync && (Uno.Session.theme['id'] === '' || isNaN(Number(Uno.Session.theme['id']))) ) {
					Uno.Session.theme[params['name']] = params['value'];
				}
			}
		}
		
	})();
	
} )( Onepager, jQuery );