( function( Uno, $ ) {
	
	/* --------------------------------
	CREATE, EDIT, DELETE content types
	-------------------------------- */
	Uno.Widget = (function() {
		
		return {
			element: $( '.widget' ),
			addable: true,
			show: function( previous ) {
				
				Uno.Helpers.displaySampleBox( false );
				
				var widget = Uno.Widget.element.hide(),
					addable = previous.find( '.addable' );
				
				previous.is('header') ? widget.prependTo( '.main_content' ) : widget.insertAfter( previous );
				widget
					.slideDown( 200, function() {
						var position = widget.parents( '.main_content, .sidebar' ).is( '.main_content' ) ? $( '.main_content section:visible' ).index( widget ) : $( '.sidebar section:visible' ).index( widget ) + 100;
						$.each( widget.find( '.widget_contents a:not(.disabled)' ), function( i, e ) {
							e.href = (e.href).split( "&position=" )[0] + '&position=' + position;
						} );
					} )
					.delegate( '.form_close', 'click', function( e ) {
						e.preventDefault();
						Uno.Widget.destroy();
						Uno.Helpers.sortableContentTypes( $( 'article' ).find( 'fieldset:visible:not(.editable fieldset)' ).length < 1 );
					} );
				
				addable.slideUp(10);
			},
			destroy: function() {
				var container = $( 'article' ),
					widget = Uno.Widget.element;
				
				widget.slideUp( 100, function() {
					container.before( this );
					
					var canDoStuff = container.find( 'fieldset:visible:not(.editable fieldset)' ).length < 1;
							
					Uno.Widget.addable = canDoStuff;
					Uno.Helpers.sortableContentTypes( canDoStuff );
					Uno.Helpers.displaySampleBox();
				} );
			}
		}
		
	})();
	
} )( Onepager, jQuery );