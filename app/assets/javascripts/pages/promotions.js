( function( Uno, $ ) {

	Uno.Promotions = ( function() {
		
		return {
			create: function() {
				var form = $( '.promotion_form' );
				
				form.bind( 'ajax:complete', function( e, xhr, status ) {
					Uno.Helpers.displayMessage( status, $.parseJSON( xhr.responseText )['message'] );
					setTimeout(function() { form[0].reset(); }, 1000);
				} );
				
				form.find( '.onepager_submit' )
					.show()
					.next( '.done_loader' ).remove();
			}
		}
		
	} )();

} )( Onepager, jQuery );