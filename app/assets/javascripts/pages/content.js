( function( Uno, $ ) {
	
	/* ------------------------
	CREATE, EDIT, DELETE Content
	------------------------ */
	Uno.Content = ( function() {
		var selector = 'article fieldset:visible:not(.editable fieldset)';
		
		return {
			sortable: true,
			load: function( params ) {
				// Always hide a single .remove_item
				Uno.Helpers.displayContentListItemDestroy( $( '.content_list ul' ) );
				
				if( !params ) {
					Uno.Company.load();
					Uno.TextArea.load();
					Uno.Services.load();
					Uno.Hours.load();
					Uno.GalleryImages.load();
					Uno.Downloads.load();
					Uno.SocialButtons.load();
					Uno.ContactFormFields.load();
					Uno.Product.load();
					Uno.SocialFeeds.load();
					Uno.PaypalButton.load();
					return;
				}
				
				switch ( params['type'] ) {
					case 'company':
						Uno.Company.load( params['id'] );
						break;
					case 'text_area':
						Uno.TextArea.load( params['id'] );
						break;
					case 'services':
						Uno.Services.load( params['id'] );
						break;
					case 'hours':
						Uno.Hours.load( params['id'] );
						break;
					case 'galleries':
						Uno.GalleryImages.load( params['id'] );
						break;
					case 'social_buttons':
						Uno.SocialButtons.load( params['id'] );
						break;
					case 'contact_form_fields':
						Uno.ContactFormFields.load( params['id'] );
						break;
					case 'download_sections':
						Uno.Downloads.load( params['id'] );
						break;
					case 'product':
						Uno.Product.load( params['id'] );
						break;
					case 'social_feed':
						Uno.SocialFeeds.load( params['id'] );
						break;
					case 'paypal_button':
						Uno.PaypalButton.load( params['id'] );
						break;
				}
			},
			add: function( type, build ) {
				$( '#' + type + '_new' ).remove(); // Remove any other new forms before creating
				
				var new_form = $( build ).hide(),
					widget = Uno.Widget.element;
				
				widget
					.after( new_form )
					.fadeOut( 200, Uno.Easing.out, function() {
						new_form.find( 'fieldset' ).show();
						new_form.fadeIn( 200, Uno.Easing.yin );
						Uno.Content.load( { 'type': type, 'id': 'new' } );
					} );
				Uno.Widget.addable = false;
				Uno.Session.open = true;
			},
			create: function( type, params ) {
				var new_form = $( '#' + type + '_new' ),
					created_container = $( params['content'] ).hide(),
					created_content = created_container.html();
					
				new_form.fadeOut( 200, Uno.Easing.out, function() {
					created_container
						.insertAfter( new_form )
						.html( created_content )
						.fadeIn( 200, Uno.Easing.yin );
						
					Uno.Content.load( { 'type': type, 'id': params['id'] } );
					new_form.remove();
					
					var canDoStuff = $( selector ).length < 1;
					
					Uno.Widget.addable = canDoStuff;
					Uno.Helpers.sortableContentTypes( canDoStuff );
					Uno.Session.open = !canDoStuff;
					
					// Re-apply inline-styles
					if( !isNaN(Number(Uno.Session.theme['id'])) && Number(Uno.Session.theme['id']) > 0 ) {
						$( '.theme_collection a[href$="/themes/' + Uno.Session.theme['id'] +'"]' ).trigger( 'click' )
					} else {
						Uno.Theme.update( $.extend( {}, Uno.Session.theme, { sync: false } ) );
					}
				} );
			},
			update: function( type, params ) {
				var parent = $( '#' + type + '_' + params['id'] ),
					fields = parent.find( 'fieldset' ),
					new_html = $( params['content'] ).html(),
					new_class = $( params['content'] ).attr( 'class' );
					
				fields.fadeOut( 200, Uno.Easing.out, function() {
					if( type === 'text_area' ) Uno.Helpers.updateCKEditor( params['id'], false );
					
					parent
						.attr( 'class', new_class )
						.html( new_html )
						.find( '.editable' ).hide()
							.fadeIn( 200, Uno.Easing.yin );
					
					var canDoStuff = $( selector ).length < 1;
					
					Uno.Content.load( { 'type': type, 'id': params['id'] } );
					Uno.Widget.addable = canDoStuff;
					Uno.Helpers.sortableContentTypes( canDoStuff );
					Uno.Session.open = !canDoStuff;
					
					// Re-apply inline-styles
					if( !isNaN(Number(Uno.Session.theme['id'])) && Number(Uno.Session.theme['id']) > 0 ) {
						$( '.theme_collection a[href$="/themes/' + Uno.Session.theme['id'] +'"]' ).trigger( 'click' )
					} else {
						Uno.Theme.update( $.extend( {}, Uno.Session.theme, { sync: false } ) );
					}
				} );
			},
			destroy: function( type, id ) {
				var container = $( '#' + type + '_' + id );
				
				container.fadeOut( 200, Uno.Easing.out, function() {
					container.remove();
					
					var canDoStuff = $( selector ).length < 1;
					
					Uno.Widget.addable = canDoStuff;
					Uno.Helpers.sortableContentTypes( canDoStuff );
					Uno.Session.open = !canDoStuff;
					Uno.Helpers.displaySampleBox();
				} )
				
			},
			updateAll: function() {
				var full_page = $( 'article' ),
					open_forms = full_page.find( 'fieldset:visible' ).not( '.editable fieldset' );
					
				if( open_forms.length > 0 ) {
					$.ajaxSetup({ global: true }); // should only be true if content forms are open
					
					Uno.Helpers.updateTextAreas();
					$( 'article' ).data( 'status', 'saving' );
					open_forms.trigger( 'submit.rails' );
					
					return false;
				} else {
					$.ajaxSetup({ global: false });
					
					return true;
				}
			}
		}
		
	} )();
	
	Uno.Company = ( function() {
		
		return {
			load: function( id ) {
				Uno.Helpers.buildFakeUploader( $( 'input[name="site[logo_image]"]' ) );
				
				$( 'article' )
					.delegate( 'input[name="site[logo_image]"]', 'change', function( e ) {
						var filename = this.value.match(/[^\\]*$/)[0],
							container = $( this ).parents('.uploader'),
							textbox = container.find( '.textbox' )[0] || $( '<span />' ).addClass( 'textbox' )[0];
					
						$( textbox ).text( filename ).appendTo( container );
					} )
					.delegate( 'header .remove_item', 'click', function( e ) {
						e.preventDefault();
						
						var container = $( this ).parents( 'li' );
							container.find( 'input[name*="[remove_logo_image]"]' ).val( true );
							container.find( 'img' ).remove();
							$(this).remove();
					} )
					.delegate( 'header input:file', 'change', function() {
						var container = $( this ).parents( 'li' );
							container.find( 'input[name*="[remove_logo_image]"]' ).val( false );
					} );
			},
			update: function( params ) {
				$( 'footer span' ).text(params['name']);
				Uno.Content.update( 'company', params );
				
				var truncated_name = params['name'].truncate(30);
				
				$( '.account_dropdown dd a' ).filter( '[href="/sites/' + params['id'] + '"]' ).html( truncated_name );
				$( '.account_dropdown .company_name' ).html( truncated_name );
			}
		}
		
	} )();
	
	Uno.TextArea = (function() {
		
		return {
			load: function( id ) {
				Uno.Helpers.updateCKEditor( id );
			},
			add: function( build ) {
				Uno.Content.add( 'text_area', build );
			},
			create:function( params ) {
				Uno.Content.create( 'text_area', params );
			},
			update: function( params ) {
				Uno.Content.update( 'text_area', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'text_area', id );
			}
		}
		
	})();
	
	Uno.Services = ( function() {
		
		return {
			load: function( id ) {
				var list = id ? $( '#services_' + id ).find( '.content_list ul' ) : $( '.services' ).find( '.content_list ul' );
				Uno.Helpers.sortableContentListItem( list );
			},
			add: function( build ) {
				Uno.Content.add( 'services', build );
			},
			create: function( params ) {
				Uno.Content.create( 'services', params );
			},
			update: function( params ) {
				Uno.Content.update( 'services', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'services', id );
			}
		}
		
	} )();
	
	Uno.Hours = ( function() {
		
		return {
			load: function( id ) {
				var selects = id ? $( '#hours_' + id ).find( 'select' ) : $( '.hours' ).find( 'select' );
				
				// if( typeof(selects.data('jq_dropdown')) == 'undefined' ) selects.dropdown();
				$.each( selects, function( i, e ) {
					if( typeof($(e).data('jq_dropdown')) == 'undefined' ) $(e).dropdown();
				} )
			},
			add: function( build ) {
				Uno.Content.add( 'hours', build );
			},
			create: function( params ) {
				Uno.Content.create( 'hours', params );
			},
			update: function( params ) {
				Uno.Content.update( 'hours', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'hours', id );
			}
		}
		
	} )();
	
	Uno.SocialButtons = ( function() {
		
		return {
			load: function( id ) {
				var selects = id ? $( '#social_buttons_' + id ).find( 'select' ) : $( '.social_buttons' ).find( 'select' ),
					list = id ? $( '#social_buttons_' + id ).find( '.content_list ul' ) : $( '.social_buttons' ).find( '.content_list ul' );
					
				$.each( selects, function( i, e ) {
					if( typeof($(e).data('jq_dropdown')) == 'undefined' ) $(e).dropdown();
				} )
				Uno.Helpers.sortableContentListItem( list );
			},
			add: function( build ) {
				Uno.Content.add( 'social_buttons', build );
			},
			create: function( params ) {
				Uno.Content.create( 'social_buttons', params );
			},
			update: function( params ) {
				Uno.Content.update( 'social_buttons', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'social_buttons', id );
			}
		}
		
	} )();
	
	Uno.ContactFormFields = ( function() {
		
		return {
			load: function( id ) {
				var selects = id ? $( '#contact_form_fields_' + id ).find( 'select' ) : $( '.contact_form_fields' ).find( 'select' ),
					list = id ? $( '#contact_form_fields_' + id ).find( '.content_list ul' ) : $( '.contact_form_fields' ).find( '.content_list ul' );
					
				$.each( selects, function( i, e ) {
					if( typeof($(e).data('jq_dropdown')) == 'undefined' ) $(e).dropdown();
				} )
				Uno.Helpers.sortableContentListItem( list );
			},
			add: function( build ) {
				Uno.Content.add( 'contact_form_fields', build );
			},
			create: function( params ) {
				Uno.Content.create( 'contact_form_fields', params );
			},
			update: function( params ) {
				Uno.Content.update( 'contact_form_fields', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'contact_form_fields', id );
			}
		}
		
	} )();
	
	Uno.Newsletters = ( function() {
		
		return {
			add: function( build ) {
				Uno.Content.add( 'newsletters', build );
			},
			create: function( params ) {
				Uno.Content.create( 'newsletters', params );
			},
			update: function( params ) {
				Uno.Content.update( 'newsletters', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'newsletters', id );
			}
		}
		
	} )();
	
	Uno.Button = ( function() {
		
		return {
			add: function( build ) {
				Uno.Content.add( 'button', build );
			},
			create: function( params ) {
				Uno.Content.create( 'button', params );
			},
			update: function( params ) {
				Uno.Content.update( 'button', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'button', id );
			}
		}
	} )();
	
	Uno.ContactSection = ( function() {
		
		return {
			add: function( build ) {
				Uno.Content.add( 'contact_section', build );
			},
			create: function( params ) {
				Uno.Content.create( 'contact_section', params );
			},
			update: function( params ) {
				Uno.Content.update( 'contact_section', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'contact_section', id );
			}
		}
		
	} )();
	
	Uno.Embed = ( function() {
		
		return {
			add: function( build ) {
				Uno.Content.add( 'embed', build );
			},
			create: function( params ) {
				Uno.Content.create( 'embed', params );
			},
			update: function( params ) {
				Uno.Content.update( 'embed', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'embed', id );
			}
		}
		
	} )();
	
	Uno.Product = ( function() {
		
		return {
			load: function( id ) {
				var uploader = id ? $( '#product_' + id ).find( 'input:file' ) : $( '.products' ).find( 'input:file' ),
					selects = id ? $( '#product_' + id ).find( 'select' ) : $( '.products' ).find( 'select' );
				
				Uno.Helpers.buildFakeUploader( uploader );
				$.each( selects, function( i, e ) {
					if( typeof($(e).data('jq_dropdown')) == 'undefined' ) $(e).dropdown();
				} )
			},
			add: function( build ) {
				Uno.Content.add( 'product', build );
			},
			create: function( params ) {
				Uno.Content.create( 'product', params );
			},
			update: function( params ) {
				Uno.Content.update( 'product', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'product', id );
			}
		}
		
	} )();

	Uno.PaypalButton = ( function() {
		
		return {
			load: function( id ) {
				var selects = id ? $( '#paypal_button_' + id ).find( 'select' ) : $( '.paypal_buttons' ).find( 'select' ),
						uploader = id ? $( '#paypal_button_' + id ).find( 'input:file' ) : $( '.paypal_buttons' ).find( 'input:file' );;
					
				$.each( selects, function( i, e ) {
					if( typeof($(e).data('jq_dropdown')) == 'undefined' ) $(e).dropdown();
				} );
				Uno.Helpers.buildFakeUploader( uploader );
			},
			add: function( build ) {
				Uno.Content.add( 'paypal_button', build );
			},
			create: function( params ) {
				Uno.Content.create( 'paypal_button', params );
			},
			update: function( params ) {
				Uno.Content.update( 'paypal_button', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'paypal_button', id );
			}
		}
		
	} )();

	Uno.Promotion = ( function() {
		
		return {
			add: function( build ) {
				Uno.Content.add( 'promotion', build );
			},
			create: function( params ) {
				Uno.Content.create( 'promotion', params );
			},
			update: function( params ) {
				Uno.Content.update( 'promotion', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'promotion', id );
			}
		}
	} )();
	
	Uno.SocialFeeds = ( function() {
		
		return {
			load: function( id ) {
				var selects = id ? $( '#social_feed_' + id ).find( 'select' ) : $( '.social_feeds' ).find( 'select' );
				
				$.each( selects, function( i, e ) {
					if( typeof($(e).data('jq_dropdown')) == 'undefined' ) $(e).dropdown();
				} );
			},
			add: function( build ) {
				Uno.Content.add( 'social_feed', build );
			},
			create: function( params ) {
				Uno.Content.create( 'social_feed', params );
			},
			update: function( params ) {
				Uno.Content.update( 'social_feed', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'social_feed', id );
			}
		}
		
	} )();

	Uno.Iframe = ( function() {
		
		return {
			add: function( build ) {
				Uno.Content.add( 'iframe', build );
			},
			create: function( params ) {
				Uno.Content.create( 'iframe', params );
			},
			update: function( params ) {
				Uno.Content.update( 'iframe', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'iframe', id );
			}
		}
	} )();
	
	Uno.ScriptEmbed = ( function() {
		
		return {
			add: function( build ) {
				Uno.Content.add( 'script_embed', build );
			},
			create: function( params ) {
				Uno.Content.create( 'script_embed', params );
			},
			update: function( params ) {
				Uno.Content.update( 'script_embed', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'script_embed', id );
			}
		}
	} )();
	
	/* ------------------------------
	CREATE, EDIT, DELETE Uploadables
	----------------------------- */
	
	Uno.Uploads = ( function() {
		
		return {
			load: function( params ) {
				var uploader = params['id'] ? $( '#' + params['type'] + '_' + params['id'] ).find( 'input:file' ) : $( '.' + params['type'] ).find( 'input:file' ),
					list = params['id'] ? $( '#' + params['type'] + '_' + params['id'] ).find( '.content_list ul' ) : $( '.' + params['type'] ).find( '.content_list ul' );

				if( uploader.length > 0 ) {
					Uno.Helpers.buildFakeUploader( uploader );
				}
				if( list.length > 0 ) {
					Uno.Helpers.sortableContentListItem( list );
				}
			},
			create: function( params ) {
				var form = $( '#' + params['type'] + '_' + params['id'] + ' form' );
				
				// Display html
				form.find( '.content_list ul' ).append( params['content'] );
				
				if( params['errors'] !== '' )
					Uno.Helpers.displayErrors( params['errors'], form[0] );
			}
		}
		
	} )();
	
	Uno.Galleries = ( function() {
		
		return {
			create: function( params ) {
				Uno.Content.create( 'galleries', params );
			},
			add: function( build ) {
				Uno.Content.add( 'galleries', build );
			},
			update: function( params ) {
				Uno.Content.update( 'galleries', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'galleries', id );
			}
		}
		
	} )();
	
	Uno.GalleryImages = ( function() {
		
		return {
			load: function( id ) {
				var selects = id ? $( '#galleries_' + id ).find( 'select' ) : $( '.galleries' ).find( 'select' );
				
				$.each( selects, function( i, e ) {
					if( typeof($(e).data('jq_dropdown')) == 'undefined' ) $(e).dropdown();
				} );
				
				Uno.Uploads.load( { 'type': 'galleries', 'id': id } );
			},
			create: function( params ) {
				Uno.Uploads.create( $.extend( {}, params, { 'type': 'galleries' } ) )
			}
		}
		
	} )();
	
	Uno.DownloadSections = ( function() {
		
		return {
			create: function( params ) {
				Uno.Content.create( 'download_sections', params );
			},
			add: function( build ) {
				Uno.Content.add( 'download_sections', build );
			},
			update: function( params ) {
				Uno.Content.update( 'download_sections', params );
			},
			destroy: function( id ) {
				Uno.Content.destroy( 'download_sections', id );
			}
		}
		
	} )();
	
	Uno.Downloads = ( function() {
		
		return {
			load: function( id ) {
				Uno.Uploads.load( { 'type': 'download_sections', 'id': id } );
			},
			create: function( params ) {
				Uno.Uploads.create( $.extend( {}, params, { 'type': 'download_sections' } ) )
			}
		}
		
	} )();
	
	return Uno;
	
} )( Onepager, jQuery );