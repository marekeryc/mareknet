( function( Uno, $ ) {
	
	/* ------------------------
	CREATE, EDIT, DELETE Site
	------------------------ */
	Uno.Site = (function() {
		
		return {
			add: function( params ) {
				var onboarding = $( '.onboarding' );
				
				onboarding.dialog({
					modal: true,
					title: 'A few quick Onepager tips',
					resizable: false,
					draggable: false,
					autoOpen: false,
					show: 'fade',
					closeText: '',
					width: 984,
					position: ['center', 100],
					buttons: [ {
							text: "Got it, let's build!",
							click: function() {
								onboarding.dialog("close");
							}
					} ],
					open: function() {
						$( window ).resize( function() {
							onboarding.dialog( 'option', { position: ['center', 100] });
						} );
					},
					close: function() {
						Onepager.Theme.edit( params['sid'], params['pid'] );
						Onepager.Site.edit( params['sid'] );
					}
				});
				
				Uno.Session.fresh = true;
				
				if(!params['visited']) {
					setTimeout( function() { onboarding.dialog( 'open' ); }, 200 )
				} else {
					Onepager.Theme.edit( params['sid'], params['pid'] );
					Onepager.Site.edit( params['sid'] );
				}
			},
			edit: function( is_new ) {
				var container = $( 'article' ),
					widget = Uno.Widget.element = $( '.widget' );
					
				// 0. $(selection).data('name', 'value') is causing <= IE8 to break
				// We can add more tests here and display notice
				if( !Modernizr.rgba || !Modernizr.backgroundsize ) {
					Onepager.Support.show();
				}
				
				// 1.a Hide content type adding widget
				widget.hide();
				
				// 1.b Create some UI elements
				Uno.Content.load();
				
				// 1c. Make content types sortable
				Uno.Helpers.sortableContentTypes();
				
				/* WHERE MOST OF THE EVENT DELEGATION HAPPENS HERE FOR CONTENT EDITING */
				
				// 2. Show form fields on content click
				container
					.delegate( '.editable:not(.sample_box .editable)', 'click', function( e ) {
						e.stopPropagation();
						e.preventDefault();
						
						var contents = $( this ),
							fields = contents.parents('section, header').find( 'fieldset' );
						
						Uno.Widget.destroy();
						contents.next( '.addable' ).stop().slideUp( 100 );
						contents.fadeOut( 200, Uno.Easing.out, function() {
							fields.fadeIn( 200, Uno.Easing.yin );
							Uno.Widget.addable = false;
							Uno.Helpers.sortableContentTypes( false );
							Uno.Session.open = true;
						} );
					} )
				
				// 3. Show add button on content hover
					.delegate( '.editable, .sample_box', 'mouseenter mouseleave', function( e ) {
						var addable = $( this ).next( '.addable' );
						
						if( !Uno.Widget.addable ) return false;
						e.type === 'mouseenter' ? addable.stop().slideDown( 150 ) : addable.stop().slideUp( 100 );
					} )
					.delegate( '.addable', 'mouseenter mouseleave', function( e ) {
						var addable = $( this );
						
						e.type === 'mouseenter' ? addable.stop().show() : addable.stop().slideUp( 100 );
					} )
				
				// 4. Hide form fields
					.delegate( '.form_close', 'click', function( e ) {
						e.stopPropagation();
						e.preventDefault();

						var button = $( this ),
							parent = button.parents( 'section, header' );
							
						var contents = parent.find( '.editable' ),
							fields = parent.find( 'fieldset:not(.editable fieldset)' );
							
						fields.fadeOut( 200, Uno.Easing.out, function() {
							parent.find('form')[0].reset();
							parent
								.find( '.content_list li:hidden' ).show()
									.find( 'input[name$="[_destroy]"]' ).val('false');
							
							contents.length > 0 ? contents.fadeIn( 200, Uno.Easing.yin ) : parent.remove();
							
							var canDoStuff = container.find( 'fieldset:visible:not(.editable fieldset)' ).length < 1;
							
							Uno.Widget.addable = canDoStuff;
							Uno.Helpers.sortableContentTypes( canDoStuff );
							Uno.Helpers.displaySampleBox();
						} );
					} )
					
				// 5a. Show new content types buttons
					.delegate( '.form_add', 'click', function( e ) {
						e.preventDefault();
						
						var button = $( this ),
							parent = button.parents( 'section, header' );
							
						Uno.Widget.addable = false;
						Uno.Helpers.sortableContentTypes( false );
						Uno.Widget.show( parent );
					} )
				
				// 5b. Disabled tooltip
					.delegate( '.widget_contents .disabled', 'mouseenter mouseleave', function( e ) {
						var tooltip = '<span class="disabled_tooltip">' + this.title + '</span>';
						
						e.type === 'mouseenter' ? $( this ).append( tooltip ) : $( this ).find( '.disabled_tooltip' ).remove();
					} )
					.delegate( '.widget_contents a.disabled', 'click', function( e ) {
						e.preventDefault();
						Uno.Helpers.displaySaver( true );
						$( '.toolbox form' )
							.attr( 'data-upgrade-url', this.href )
							.trigger( 'submit.rails' );
					} )
					
				// 6a. Loader button when click Done
					.delegate( 'form', 'ajax:beforeSend ajax:remotipartSubmit', function( e, xhr, settings ) {
						if( settings.type.toLowerCase() !== 'delete' ) {
							$( this ).find('input:submit')
								.hide()
								.after( '<span class="done_loader">Saving...</span>' );
						}
					} )
					
				// 6b. Delete content type on second click
					.delegate( 'a.form_remove[data-confirm]', 'click', function( e ) {
						e.preventDefault();
						e.stopImmediatePropagation();
						
						var ele = $( this ),
							animation = {
								'text-indent': 0,
								'width': 'auto'
							};

						ele.animate( animation, 70, function() {
							ele.removeAttr( 'data-confirm' )
						});
					} )
					
				// 7. Global error message treatment
					.delegate( 'form', 'ajax:error', function( e, xhr, status, response ) {
						var message = xhr.status === 500 ? 'An error occurred. Refresh the page or click "cancel" at the end of this form.' : xhr.responseText;
						Uno.Helpers.displayErrors( message, e.target );
						
						var form = $( this );
						form.find('input:submit').show();
						form.find( '.done_loader' ).remove();
					} )
					
				// 8. delegate content_list insertion / removal
					.delegate( '.content_list .remove_item', 'click', function( e ) {
						e.preventDefault();
						Uno.Helpers.destroyContentListItem( this );
					} )
					.delegate( '.content_list .add_item', 'click', function( e ) {
						e.preventDefault();
						Uno.Helpers.createContentListItem( $( this ).parents( '.content_list' ).find( 'ul' ) );
					} )
					
				// 9a. Delegate multiple file uploads on input:file change
					.delegate( '.galleries input:file, .download_sections input:file', 'change', function() {
						var input = $( this );
						
						if( input.val().length > 0 ) {
							// original input intp to cloned form, cloned input into real form
							var clone = input.clone().insertBefore( input ),
								form = input.parents( 'form' ).clone(),
								format = form.find( 'select[name="container[format]"]' );
							
							form
								.append( input ).append( format )
								.attr( 'action', input.data( 'url' ) )
								.addClass( 'hack_form' ).hide().appendTo( container )
								.find( 'fieldset' ).remove();
								
							form.find( 'input[name="_method"]' ).val( '' );
							form.trigger( 'submit.rails' );
							Uno.Helpers.displayLoader( true );
						}
					} )
					.delegate( '.hack_form', 'ajax:complete', function( e, data, status, xhr ) {
						$( e.target ).remove(); // remove cloned uploader form
						Uno.Helpers.displayLoader( false );
					} )
				
				// 9b. file stuff for gumroad module
					.delegate( 'select[name="product[product_type]"]', 'dropdown:change', function() {
						var upload_field = $( this ).parents('li').next( '.upload_field' );
						this.value === "Digital Download" ? upload_field.show() : upload_field.hide();
					} )
					.delegate( '.products .use_link', 'click', function( e ) {
						e.preventDefault();
					
						var link = $( this ),
							displayed = link.data( 'type' ),
							file_field = link.parents( '.upload_field' ).find( 'input[type="file"]' );
					
						if( displayed == 'file' || file_field.filter(':disabled').length == 0 ) {
							file_field
								.attr( 'disabled', true )
								.parents('.uploader').hide()
									.next('input[name="product[url]"]')
										.show()
										.attr( 'disabled', false );
									
							$( this ).data( 'type', 'text' ).text( 'or upload a file' );
						} else {
							file_field
								.attr( 'disabled', false )
								.parents('.uploader').show()
									.next('input[name="product[url]"]')
										.hide()
										.attr( 'disabled', true );
						
							$( this ).data( 'type', 'file' ).text( 'or use a link' )
						}
					} )
					.delegate( '.products input:file, .paypal_buttons input:file', 'change', function() {
						var filename = this.value.match(/[^\\]*$/)[0],
							container = $( this ).parents('.uploader'),
							textbox = container.find( '.textbox' )[0] || $( '<span />' ).addClass( 'textbox' )[0];
						
						$( textbox ).text( filename ).appendTo( container );
					} )
					.delegate( 'input:checkbox[name="paypal_button[customize_price]"]', 'change', function( e ) {
						var price_input = $( this ).parents('.checkbox').prev('li');
						if( this.checked ) {
							price_input.hide();
						} else {
							price_input.show();
						}
					} )
					.delegate( '.product_file .remove_item, .preview_file .remove_item', 'click', function( e ) {
						e.preventDefault();
						
						var item = $( this ).parent( 'li, div' ),
							destroy = item.find( 'input' ).filter( function() { return (this.name).match(/\[remove_/) !== null; } );
				
						item.slideUp( 50, function() {
							destroy.length > 0 ? destroy.val( 'true' ) : item.remove();
							item.next( '.upload_field' ).show();
						} );
				
					} )
					.delegate( '.new_product', 'ajax:success', function() {
						Uno.Helpers.showGumroadMessage();
					} )
					
				// 10. Dropdown change for gallery images
					.delegate( 'select[name="container[format]"]', 'dropdown:change', function() {
						var classname = this.value;
						$( this ).parents( 'section' ).find( '.content_list li div' ).hide().filter( '.' + classname ).show();
					} )
					
				// 12a. More info popup
					.delegate( '.form_info', 'click', function( e ) {
						e.preventDefault();
						
						var type = $( this ).data( 'content-type' ),
							container = $( this ).parents( '.form_header' ),
							build = container.find( '.form_details' ).length === 0 ? $( '<div />' ).addClass( 'form_details' ) : container.find( '.form_details' ),
							info = Uno.Content.info;
						
						if( build.filter(':visible').length > 0 )
							build.hide().remove();
						else
							build.html( info[type] ).hide().appendTo( container ).fadeIn(75);
					} );
					
				// 12b. Close more info
				$( document ).bind( 'click', function( e ) {
					if( !$( e.target ).is( container.find( '.form_info' ) ) ) container.find( '.form_details' ).fadeOut(75, function() { $(this).remove(); });
				} );
			},
			update: function( params ) {
				Uno.Content.update( 'company', params );
			}
		}
		
	})();
	
} )( Onepager, jQuery );