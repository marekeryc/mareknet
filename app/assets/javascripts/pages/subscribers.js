( function( Uno, $ ) {

	Uno.Subscribers = ( function() {
		
		return {
			create: function() {
				var form = $( '.new_subscriber' );
				
				form.bind( 'ajax:complete', function( e, xhr, status ) {
					Uno.Helpers.displayMessage( status, $.parseJSON( xhr.responseText )['message'] );
					setTimeout(function() { form[0].reset(); }, 1000);
				} );

				form.bind( 'ajax:before', function( xhr, settings ) {
					form.find('input[name="subscriber[crosscheck]"]').val("negative37892");
				} );

				form.find( '.onepager_submit' )
					.show()
					.next( '.done_loader' ).remove();
			}
		}
		
	} )();

} )( Onepager, jQuery );