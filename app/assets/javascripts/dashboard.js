//= require jquery
//= require jquery-ui
//= require jquery_ujs

//= require lib/domainchecker
//= require lib/toolbar

//= require_self

//= require dashboard/analytics
//= require dashboard/newsletters
//= require dashboard/forms
//= require dashboard/site
//= require dashboard/users

var Onepager = window.Onepager || {};