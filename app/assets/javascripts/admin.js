//= require jquery
//= require jquery-ui
//= require jquery_ujs

//= require lib/modernizr
//= require lib/extentions

//= require lib/colorpicker

var OnepagerAdmin = window.OnepagerAdmin || {};

( function( Admin, $ ) {
	
	Admin.Theme = ( function() {
		
		return {
			load: function() {
				$( '.theme_list ol' ).sortable({
					forcePlaceholderSize: true,
					containment: 'parent',
					tolerance: 'pointer',
					distance: 1,
					update: function( event, ui ) {
					//	$.post( '/admin/themes/update_positions', '_method=put&' + $( this ).sortable( 'serialize' ) );
						$.ajax({
							type: 'post',
							url: ui.item.parent().data('url'),
							data: '_method=PUT&' + $( this ).sortable( 'serialize' )
						});
					}
				});
			},
			edit: function() {
				$( 'input[name*="_colour]"]' ).colorpicker( {
					set: [
							'ff0100', 'fffc00', '03fc14', '00ffff', '002bff', 'ff2bff', 'ffffff', 'ebebeb', 'e1e1e1', 'd7d7d7', 'cccccc', 'c2c2c2', 'b7b7b7', 'acacac', 'a0a0a0', '959595',
							'ed1c24', 'fff200', '00a651', '00aef0', '2e3092', 'ed138c', '898989', '7d7d7d', '707070', '626262', '555555', '464646', '363636', '262626', '111111', '000000',
							'f69679', 'f9ad81', 'fdc68a', 'fff89a', 'c4df9b', 'a3d49d', '83ca9c', '7bcdc7', '6dcff6', '7da7d8', '8594ca', '8882be', 'a086bd', 'bc8dbf', 'f49ac1', 'f6989d', 
							'f26c4e', 'f68e55', 'faae5c', 'fff567', 'acd372', '7cc576', '3cb878', '1bbbb4', '00bff3', '448ccb', '5574b8', '5f5ca8', '855fa8', 'a863a8', 'f06ea9', 'f16d7d', 
							'ed1c24', 'f26522', 'f8951e', 'fff200', '8dc740', '39b44a', '00a550', '00a99d', '00aef0', '0072bc', '0055a7', '2f3192', '652c91', '922790', 'ec128b', 'ed145a', 
							'9e0a0f', 'a0410d', 'a36209', 'aba000', '598627', '197b30', '007236', '00746a', '0076a3', '004b80', '003471', '1b1464', '440e61', '620960', '9e095e', '9e043a', 
							'790000', '7b2e00', '7d4900', '837b00', '406619', '005e20', '005926', '005952', '005b7e', '003664', '002157', '0c054c', '32064b', '4b0549', '7b0546', '7a0226', 
							'c7b299', '988575', '736358', '534741', '362f2c', 'c69d6e', 'a67c51', '8c633a', '754d24', '603813'
					],
					onselect: function(trigger, color) {
						$( trigger ).val( color );
					}
				} );
				
				$( 'input[name="theme[background_image]"]' ).change( function() {
					
					$( 'input[type="checkbox"][name="theme[background_image_enabled]"]' ).attr( 'checked', ($( this ).val() !== '') );
					
				} );
				
				$( '.display_toggle' ).click( function( e ) {
					e.preventDefault();
					
					var form = $( this ).parents( '.theme_selector' ).find( 'form' );
					
					form.filter(':visible').length > 0 ? form.slideUp( 100 ) : form.slideDown( 100 );
				} );
			}
		}
		
	} )();
	
	Admin.User = ( function() {
		
		return {
			edit: function() {
				$( '#cancellation_date' ).datepicker({
					dateFormat: "yy-mm-dd"
				});
			}
		}
		
	} )();

} )( OnepagerAdmin, jQuery );