/* Custom jQuery Dropdown v1 | YYC
 * Source: https://github.com/yinychan/jQuery-Dropdown
 */

( function( $, undefined ) {
	
	// Namespace
	var Dropdown = new Object();
	
	// Private stuff for the plug-in use only
	Dropdown.setup = function() {
		
		return {
			
			settings: {
				select: null,
				collection: new Array(),
				options: new Array(),
				optgroups: new Array(),
				build: null,
				classname: 'jq_dropdown',
				highlight: 'highlight',
				empty: 'Select one...',
				natural: true,
				truncate: 31
			},
			
			kickoff: function( select, config ) {
				var dropdown = this;
				
				$.extend( dropdown.settings, config );
				
				if( typeof( dropdown.settings.natural ) !== 'boolean' ) {
					dropdown.settings.natural = dropdown.settings.natural.call( select );
				}
				
				//////////////////////////////
				// Variables I'll need l8r //
				/////////////////////////////
				var collection = new Array(),
					optgroups = $.map( select.childNodes, function( e, i ) { if( e.nodeName.toLowerCase() === 'optgroup' ) return e; } ),
					options = get_options( select ),
					select_class = select.className;
				
				// We need this stored before we build
				if( optgroups.length > 0 ) {
					$.extend( dropdown.settings, { optgroups: $.map( optgroups, function( e ) { return e.label; } ) } );
					$.merge( options, $.map( optgroups, function( grp ) { return get_options( grp ); } ) );
				}
				
				/////////////////////////////////////////////
				// Helper functions only for use in kickoff //
				////////////////////////////////////////////
				function add_option( o ) { // will need to take into account optgroup
					var option = {
						value: o.value,
						text: o.innerHTML,
						selected: o.selected,
						disabled: o.disabled,
						parent_id: select.id,
						optgroup: $( o ).data( 'group' ) || ''
					};
					collection.push( option );
				}
				function get_options( group ) {
					var label = group.label || '';
					
					return $.map( group.childNodes, function( e ) {
						if( typeof( label ) !== 'undefined' ) {
							$( e ).attr( 'data-group', label );
						}
						
						if( e.nodeName.toLowerCase() === 'option' ) {
							add_option( e ); // pop it into collection
							return e; // pop it into option array
						};
					} );
				}
			
				// Store the new options and collection arrays for accessibility
				$.extend( dropdown.settings, {
					select: select,
					collection: collection,
					options: options,
					build: dropdown.build( collection ).addClass( select.className )
				} );
				
				$( dropdown.settings.select )
					.after( dropdown.settings.build )
					.hide();
				
				/** Disabled selects' corresponding dropdown will be hidden **/
				if( $( dropdown.settings.select ).filter( ':disabled' ).length > 0 ) {
					dropdown.settings.build.hide();
				}
				
				/** Things user can do to the dropdown **/
				dropdown.settings.build
					.undelegate( '.selected, a', 'click' )
					.delegate( '.selected', 'click', function( e ) {
						$( '.open.' + dropdown.settings.classname )
							.removeClass( 'open' )
							.find( '.options' )
								.scrollTop( 0 )
								.slideUp( 30 );
								
						e.preventDefault();
						dropdown.toggle();
					} )
					.delegate( 'a', 'click', function( e ) {
						e.stopPropagation();
						e.preventDefault();
						dropdown.update( $( this ).data( 'value' ) );
					} );
				
				/** Bind click event to close **/
				$( document ).bind( 'click', function( e ) {
					if( !$( e.target ).parent().is( '.' + dropdown.settings.classname ) ) dropdown.close();
				} );
			},
			build: function( collection ) {
				
				var dropdown = this,
					shell = $( '<span class="' + dropdown.settings.classname + '"><span class="selected"></span><span class="options"></span></span>' ),
					selected = $( '.selected', shell ),
					is_grouped = dropdown.settings.optgroups.length > 0;
					
				if( is_grouped ) {
					$.each( dropdown.settings.optgroups, function( i, e ) {
						var group = $( '<span class="optgroup">' + e + '</span>' ).data( 'label', e );
						shell.find( '.options' ).append( group );
					} );
				}
				
				$.each( collection, function( i, e ) {
					
					var element = $( '<a href="#"></a>' );
					
					e.text = e.text || dropdown.settings.empty;
					
					element
						.attr( 'data-value', e.value )
						.attr( 'data-optgroup', e.optgroup )
						.html( e.text );
					
					if( e.selected ) {
						element
							.addClass( dropdown.settings.highlight )
							.attr( 'data-selected', 'true' );
							
						selected.html( e.text.truncate(dropdown.settings.truncate) ); // truncate function in application.js
					}
					if( e.disabled ) {
						element.addClass( 'disabled' );
					}
					
					if( e.optgroup !== '' ) {
						var this_group = shell.find( '.optgroup' ).filter( function( ind, ele ) { return $( ele ).data( 'label' ) === e.optgroup; } ),
							next_group = this_group.nextAll( '.optgroup' ).filter( ':first' );
						
						( next_group.length > 0 ) ? element.insertBefore( next_group ) : element.appendTo( shell.find( '.options' ) );
					} else {
						element.text() === dropdown.settings.empty ? element.prependTo( shell.find( '.options' ) ) : element.appendTo( shell.find( '.options' ) );
					}
					
				} );
				
				if( selected.text().length == 0 ) {
					selected.text( $( 'a:eq(0)', shell ).text() )
				}
				
				return shell;
			},
			toggle: function() {
				var dropdown = this,
					fake_options = dropdown.settings.build.find( '.options' );
				
				dropdown.close();
				fake_options.focus();
				
				if( !dropdown.settings.natural ) fake_options.css( 'top', fake_options.outerHeight() * -1 );
				
				fake_options.filter( ':hidden' ).slideDown( 10, function() {
					var top_pos = fake_options.find('.'+dropdown.settings.highlight).length > 0 ? fake_options.find('.'+dropdown.settings.highlight).position().top - 4 : 0;
					fake_options.scrollTop( top_pos );
				} );
				
				dropdown.settings.build.addClass( 'open' );
				
				$( document ).keydown( function( e ) {
					dropdown.keydown.call( dropdown, e );
				} );
			},
			update: function( val, silent ) {
				var dropdown = this,
					build = dropdown.settings.build,
					select = dropdown.settings.select,
					options = $( select ).find( 'option' ),
					selected = options.filter( function( i, e ) { return e.value == val; } ),
					fake_selected = build.find( 'a' ).filter(  function( i, e ) { return $( e ).data( 'value' ) == val; }  );
				
				/** This updates the value on select field **/
				select.value = val;
				select.selectedIndex = $( options ).index( selected );
				
				options
					.attr( 'selected', false )
					.filter( selected )
						.attr( 'selected', true );
				
				$( select ).trigger( 'change' );
				
				if (!silent) {
					$( select ).trigger( 'dropdown:change' );
				}
				
				/** This changes text and highlighted value **/
				if ( selected.length > 0 ) dropdown.settings.build.find( '.selected' ).html( selected.html().truncate(31) );
				build.find( 'a' ).removeData( 'selected' ).removeClass();
				fake_selected.data( 'selected', 'true' ).addClass( dropdown.settings.highlight );
				
				dropdown.close();
			},
			close: function() {
				var dropdown = this,
					selector = dropdown.settings.build;
				
				selector
					.removeClass( 'open' )
					.find( '.options' )
						.scrollTop( 0 )
						.fadeOut( 30 );
				
				selector.find( 'a' ).removeClass( dropdown.settings.highlight );
				
				$.each( $( 'a', selector ), function( i, e ) {
					if( $( e ).data( 'selected' ) == 'true' ) {
						e.className = dropdown.settings.highlight;
					}
				} );
				
				$( document ).unbind( 'keydown' );
			},
			keydown: function( e ) {
				e.preventDefault();
				
				var dropdown = this,
					code = (e.keyCode ? e.keyCode : e.which),
					options = dropdown.settings.build.find( '.options a' ),
					highlight = '.' + dropdown.settings.highlight;
			
					function jumpto( highlighted ) {
						var top_pos = options.filter( highlight ).length > 0 ? options.filter( highlight ).position().top - 4 : 0,
							container = options.parents( '.options' );

						container.scrollTop( container.scrollTop() + top_pos, 10 );
					}
					
				switch( code ) {
					case 38: // UP
						var highlighted = options.filter( highlight ),
							previous = highlighted.length > 0 ? (highlighted.prevAll('a').filter( ':first' ).length > 0 ? highlighted.prevAll('a').filter( ':first' ) : options.filter( ':last' ) ) : options.filter( ':last' );
						
						if( previous.length > 0 ) {
							highlighted.removeClass( dropdown.settings.highlight );
							previous.addClass( dropdown.settings.highlight );
						}
						jumpto( previous );
						
						break;
					case 40: // DOWN
						var highlighted = options.filter( highlight ),
							next = highlighted.length > 0 ? (highlighted.nextAll('a').filter( ':first' ).length > 0 ? highlighted.nextAll('a').filter( ':first' ) : options.filter( ':first' ) ) : options.filter( ':first' );

						if( next.length > 0 ) {
							highlighted.removeClass( dropdown.settings.highlight );
							next.addClass( dropdown.settings.highlight );
						}
						jumpto( next );

						break;
					case 13: // ENTER
						if( options.filter( highlight ).length > 0 ) {
							dropdown.update( options.filter( highlight ).data( 'value' ) );
						}
					
						break;
					case 27: // ESC
						dropdown.close();
						break;
				}
			}
		};
		
	};
	
	// Methods users can call
	Dropdown.methods = {
		update: function( val ) {
			var dropdown = new Dropdown.setup(),
				settings = $( this ).data( 'jq_dropdown' );
				
			$.extend( dropdown, { 'settings': settings } );
			
			dropdown.update( val, true );
		}
	};
	
	$.fn.dropdown = function() {
		var args = Array.prototype.slice.call( arguments );
		
		if( typeof(args[0]) === 'string' ) { // this runs a public method
			return Dropdown.methods[args[0]].call( this, args[1] || '' );
		}
		
		return $.each( this, function() {
			var dropdown = new Dropdown.setup();
			dropdown.kickoff( this, args[0] || {} );
			
			$( this )
				.data( 'jq_dropdown', dropdown.settings )
				.trigger( 'dropdown:create', [dropdown.settings] );
		} );
		
	};
	
} )( jQuery );