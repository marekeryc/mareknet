// Custom date formatting
Date.prototype.dateForForms	 = function() {
	return this.getFullYear().toString() + '-' + ( this.getMonth() < 10 ? '0' + (this.getMonth()+1) : (this.getMonth()+1).toString() ) + '-' + ( this.getDate() < 10 ? '0' + this.getDate() : this.getDate().toString() );
};

Date.prototype.jumpMonths = function( skip ) {
	return (new Date()).setMonth(this.getMonth() + skip);
};

Date.prototype.longDateFormat = function( abbrev ) {
	var trim = typeof( abbrev ) === 'undefined' ? 25 : parseInt( abbrev ),
		months = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];
	
	return months[this.getMonth()].substr(0,trim) + ' ' + this.getDate() + ', ' + this.getFullYear();
};

String.prototype.convertToDate = function() {
	var year = this.substring(0, 4),
		month = parseInt( this.substring(5, 7) ) - 1,
		day = this.substring(8, 10),
		hour = this.substring(11, 13),
		minutes = this.substring(14, 16);
		
	return new Date(year, month, day, hour, minutes);
};

String.prototype.makeReadable = function() {
	var statement = this.replace( /_/g, ' ' ).toLowerCase().replace(/csr/gi, 'CSR').replace( /ivr/gi, 'IVR' ).replace(/^.|\s\S/g, function(a) { return a.toUpperCase(); }).replace(' Or ', ' or ');
	return statement;
};

String.prototype.truncate = function( len ) {
	var word = $.trim(this), // jQuery trim
		length = len || 35,
		truncated = word.substring(0,length);

	if( word.length > length ) truncated += '...';
	
	return truncated;
};

String.prototype.capitalize = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
};

Array.prototype.unique = function() {
	var r = new Array();
	
	o:for(var i = 0; i < this.length; i++) {
		for(var x = 0; x < r.length; x++) {
			if(r[x]==this[i]) {
				continue o;	
			}
		}
		r[r.length] = this[i];
	}
	return r;
};