var DomainChecker = window.DomainChecker || {};

( function( DC, $ ) {
	
	DC.create = function() {
		
		return {
			
			settings: {
				'url': ''
			},
			
			checkTerm: function( input, params ) {
				var checker = this;
				
				$.extend( checker.settings, params, { term: input.value } );
				$( input ).keyup( validate ).keyup().focus( validate );
				
				function validate() {
					// TODO move this if statement to app-level
					if( $( 'input[name*="[domain_type]"][value="new_custom_domain"]' ).is( ':checked' ) ) {
						checker.validate( this );
					}
				}
			},
			
			validate: function( el ) {
				var checker = this,
					classname = 'domain_checker',
					input = $( el ),
					domain = input.val().toLowerCase(),
					build = $( '<span />' ).addClass( classname );
				
				input.next( '.' + classname ).remove();
				
				if( domain.length > 3 ) {
					var tldList = ['com', 'net', 'org', 'biz', 'co', 'us'], //set of valid tld's
						tld = domain.search('[^a-zA-Z0-9\.\-]') == -1 ? domain.split('.')[ (domain.split('.').length - 1) ] : -1, //Checks for invalid characters before .TLD and return start position of tld or false
						validTld = $.inArray( tld, tldList ),
						messages = { 'error': 'Invalid URL', 'available': 'You got it!', 'not_available': 'It\'s taken', 'processing': 'Checking...' };
						message = messages['processing'];
						
					build.text( message ).addClass( 'processing' ).insertAfter( input );
					
					if(validTld >= 0) { // Hits the server only if valid TLD
						$.ajax({
							url: checker.settings.url,
							data: 'domain=' + domain, 
							dataType: 'script',
							success: function( data ) {
								var result = data == 'true' ? 'available' : 'not_available';
									message = messages[result];
								
								build.text( message ).removeClass().addClass( result + ' ' + classname );
							},
							error: function() {
								build.text( messages['error'] ).removeClass().addClass( 'error ' + classname );
							}
						});
					} else {
						setTimeout( function() {
							if(validTld < 0)
								build.text( messages['error'] ).removeClass().addClass( 'error ' + classname );
							else
								checker.validate( el );
						}, 1000 );
					}
				}
			}
			
		};
		
	};
	
} )( DomainChecker, jQuery );

( function( $ ) {
	
	$.fn.domainchecker = function() {
		var args = Array.prototype.slice.call( arguments );
		
		return $.each( this, function() {
			var domainchecker = new DomainChecker.create();
				domainchecker.checkTerm( this, args[0] || {} );
		} );
		
	};
	
} )( jQuery );