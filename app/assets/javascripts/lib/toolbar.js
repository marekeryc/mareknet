var Toolbar = window.Toolbar || {};

( function( TB, $ ) {
	
	TB.Feedback = ( function() {
		
		return {
			create: function( email ) {
				var feedback = $( '.question_dropdown' ),
					form = feedback.find( 'form' );
				
				feedback
					.addClass( 'open' )
					.find( '.dropdown' ).prepend( "<p>We just received your message and we will respond within 24 hours to " + email + ".</p>" );
				form.hide();
				
				setTimeout( function() {
					feedback
						.removeClass( 'open' )
						.find( 'p' ).remove();
					form[0].reset()
					form.fadeIn( 100 );
				}, 5000 )
			}
		}
		
	} )();
				
} )( Toolbar, jQuery );