/*
=================================
Onepager Color Picker for themes
=================================
*/

// YYC to fix:
// 1. do something to show the hex error

var Colorpicker = window.Colorpicker || {};

( function( CP, $ ) {
	
	CP.Methods = {
		update: function( value ) {
			$( this ).find( 'a' )
				.removeAttr( 'style' )
				.css( 'background-color', '#' + value )
		}
	};
	
} )( Colorpicker, jQuery );

(function($) {
	$.fn.colorpicker = function(options) {
		
		// Update method passed in
		var args = Array.prototype.slice.call( arguments );
		if( typeof(args[0]) === 'string' ) { // this runs a public method
			return Colorpicker.Methods[args[0]].call( this, args[1] || '' );
		}
		
		var config = {
			set: [],
			path: '/images/color-picker/',
			filetype: 'gif',
			onselect: function(trigger, color) { }
		};
		$.extend(config, options);
		
		//some utils for later
		var colorset = new Array(),
			pickr = '#colorpicker',
			hexAlpha = '0123456789ABCDEF';
		
		//generate the list of preset colors based on user's settings
		$.each(config.set, function(i, e) { //TO DO make better builder, try storing data attributes on the object
			colorset += '<li style="background-color:#' + e + ';">#' + e + '</li>'
		});
		
		//widget builder
		var widget = '<div id="colorpicker">' +
						'<ul>' + colorset + '</ul>' +
						'<form>' +
							'<label>Hex Value:</label>' +
							'<input type="text" value="000000" maxlength="6" />' +
						'</form>' +
					'</div>';
		
		//create the widget if it doesn't already exist
		if($(pickr).length < 1) {
			$(widget)
				.hide()
				.css({ 'position': 'fixed', 'z-index': '10' })
				.appendTo('body');
		}
		
		//hide the widget if it's not in use
		$( document ).bind( 'click', function( e ) {
			if( $( e.target ).parent().length === 0 || $( e.target ).parent()[0].className !== 'colorpicker' )
				$(pickr).hide();
		} );
		
		//move it to the button that has been clicked
		function positionPickr(left, top, trigger) {
			trigger = $(trigger);
			
			var name = trigger.data( 'field' ) ? '[' + trigger.data( 'field' ) + ']' : trigger.attr('name');
				colour = $( 'input[name*="' + name + '"]' ).val(); // hardcoded for now
			
			left = left - 65;
			top = top - $(pickr).outerHeight();
			
			$('input', pickr)[0].value = colour;
			
			$(pickr)
				.css({ left: left, top: top })
				.show();
		}
		
		//stuff that happens with you choose for the color list or enter your own hex values
		function submitColor(trigger) {
			$( pickr )
				.undelegate( 'li', 'click' )
				.undelegate( 'form', 'click' )
				.undelegate( 'input', 'keyup' )
				.delegate( 'li', 'click', function() {
					$('input', pickr)[0].value = $(this).text().replace( '#', '' ) == 0 ? '000000' : $(this).text().replace( '#', '' );
					$('form', pickr).submit();
				} )
				.delegate( 'form', 'click', function( e ) {
					e.stopPropagation();
				} )
				.delegate( 'input', 'keyup', function( e ) {
					var code = (e.keyCode ? e.keyCode : e.which);
					
					if(this.value.length == 6) {
						$('form', pickr).submit();
					}
					
					if(code === 13 || code === 27) {
						$(pickr).hide();
					}
				} );
				
			$(pickr)
				.undelegate( 'form', 'submit')
				.delegate( 'form', 'submit', function() {
					var errors = 0;
					
					for(i = 0; i < 6; i++) {
						//checks each character to make sure its a hex value
						var character = $('input', this)[0].value.toUpperCase().charAt(i);
						errors += hexAlpha.search(character) < 0 ? 1 : 0;
					}
					
					if(errors < 1) {
						//runs the user's callback only if there are no errors
						config.onselect( trigger, $('input', this)[0].value.toString() );
					} else {
						//TBD: display error message
					//	onepagr.displayNotice( 'An error occurred.' );
					}
					return false;
				});
		}
				
		return this.each(function(i, e) { //just do it, like nike
			$( e ).bind( 'click focus', function( e ) {
				e.preventDefault();
				
				var left = $(this).offset().left,
					top = $(this).offset().top - $(window).scrollTop();
					
				positionPickr(left, top, this);
				submitColor(this);
			});
		});
	};
}(jQuery));
//end colourpickr