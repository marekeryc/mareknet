//= require jquery
//= require jquery-ui
//= require jquery_ujs

//= require_self

//= require pages/contact_form_responses
//= require pages/subscribers
//= require pages/promotions
//= require pages/compatibility

var Onepager = window.Onepager || {};

( function( Uno, $ ) {
	
	Uno.Helpers = ( function() {
		
		return {
			displayMessage: function( type, msg ) {
				var ignore = $( '<a href="#" class="ignore">Ignore</a>' ),
					notice = $( '.notice' ).length > 0 ? $( '.notice' ) : $( '<div class="notice" />' );
				
				notice
					.removeClass().addClass( type + ' notice' )
					.html( '<p>' + msg + '</p>' )
					.append( ignore )
					.hide()
					.insertBefore( 'article' )
					.slideDown( 50 );
				
				ignore.bind( 'click', function( e ) {
					e.preventDefault();
					
					notice.slideUp( 50, function() {
						$( this ).remove();
						ignore.unbind( 'click' );
					} );
				} );
				
				setTimeout( function() { ignore.trigger( 'click' ); }, 8000 );
			},
			autoSlideshow: function(whichTrigger) {
				
				var slideshow_arrow = typeof(whichTrigger) !== 'undefined' ? whichTrigger : '.gallery_slideshow .next_image';
				
				setTimeout(function() {
					$(slideshow_arrow).trigger('click');
				}, 5000);
				
			},
			turnAutoSlideshowOn: false
		};
		
	} )();
	
	Uno.Pages = ( function() {
		
		var imagegallery = {
			modal: true,
			resizable: false,
			draggable: false,
			show: 'fade',
			width: 800,
			minHeight: 600,
			closeText: ''
		};
		
		return {
			show: function( sid, pid ) {
				
				// 1a. Image Gallery creater
				$( '.thumbnails img' ).click( function( e ) {
					e.preventDefault();
					
					var img = $( this ),
						enlarged = img.data( 'enlarged' ),
						dialog = $( '.image_gallery' ).length > 0 ? $( '.image_gallery' ) : $( '<div class="image_gallery" />' ),
						left = $( '<a href="#" />' ).addClass( 'previous_image' ).data( 'direction', 'previous' ).text( 'Previous' ),
						right = $( '<a href="#" />' ).addClass( 'next_image' ).data( 'direction', 'next' ).text( 'Next' ),
						container = img.parents( '.galleries' ),
						all_images = $.map( container.find( 'img' ), function( e, i ) {
							var image = '<img src="' + $( e ).data( 'enlarged' ) + '" alt="' + e.alt + '" />',
								caption = '<h4>' + e.title + '</h4>';
							return '<li>' + image + caption + '</li>';
						} );
					
					dialog
						.html( '<ul>' + all_images.join('\n') + '</ul>' )
						.find( 'li' ).hide()
							.filter( function( index ) {
								return container.find('img').index( img ) === index;
							} ).show();
					
					if( all_images.length > 1 ) dialog.append( left, right );
					
					dialog.dialog(
						$.extend( {}, imagegallery, {
							open: function( e, ui ) {
								$( '.ui-widget-overlay' ).click( function() {
									dialog.dialog( 'close' );
								} );
								$( window ).resize( function() {
									dialog.dialog( 'option', {'position': ['center', 'center']} );
								} );
                
                // Hack to center tall images
                setTimeout(function() {
                  dialog.dialog( 'option', {'position': ['center', 'center']} );
                }, 75)
							},
							close: function( e, ui ) {
								dialog.remove();
							}
						} )
					);
				} );
				
				// 1b. Image Gallery carousel
				$( 'body' ).delegate( '.image_gallery .previous_image, .image_gallery .next_image, .gallery_slideshow .previous_image, .gallery_slideshow .next_image', 'click', function( e ) {
					e.preventDefault();
					
					var gallery = $(this).parents( '.image_gallery, .gallery_slideshow' ),
						child = gallery.hasClass('image_gallery') ? 'li' : 'img',
						images = gallery.find( child ),
						current_image = gallery.find( child + ':visible' ),
						new_image = $( this ).data( 'direction' ) == 'previous' ? ( images.index( current_image ) == 0 ? images.filter( ':last' ) : current_image.prev() ) : ( images.index( current_image ) == images.length - 1 ? images.filter( ':first' ) : current_image.next() );
					
					current_image.fadeOut( 200, function() {
						new_image.fadeIn( 150, function() {
							images.hide(); // make sure they are all hidden from spazz clicking
							new_image.show();
							
							if(Onepager.Helpers.turnAutoSlideshowOn) {
								Onepager.Helpers.autoSlideshow(this);
							}
						} );
					} );
				} );
				$( 'body' ).delegate( '.gallery_slideshow img, .image_gallery img', 'click', function( e ) {
					$( this ).parents( '.gallery_slideshow, .image_gallery' ).find( '.next_image' ).trigger( 'click' );
				} );
				
				$.each( $( '.gallery_slideshow' ), function( i, e ) {
					if( $( e ).find( 'img' ).length > 1 ) {
						$( e ).find( 'img:first' ).one( 'load', function() {
							var height = $( this ).height();
							
							$( e ).height( height );
							$( e ).find( 'img' ).css( {
								'max-height': height,
								'cursor': 'pointer'
							} );
						} )
					}
				} );
				
				// 1c. Image Gallery keydown carousel
				$( document ).keydown( function( e ) {
					var code = (e.keyCode ? e.keyCode : e.which);
					
					if( $( '.image_gallery:visible' ).length > 0 ) {
						
						switch ( code ) {
							case 37:
								$( '.image_gallery a' )
									.filter( function( i ) { return $( this ).data( 'direction' ) === 'previous'; } )
									.trigger( 'click' );
								break;
							case 39:
								$( '.image_gallery a' )
									.filter( function( i ) { return $( this ).data( 'direction' ) === 'next'; } )
									.trigger( 'click' );
								break;
						}
						
					}
				} );
				
				// 2. Newsletter activity
				Uno.Subscribers.create();
				
				// 3. Submission form activity
				Uno.ContactFormResponses.create();
				
				// 4. Promotions form activity
				Uno.Promotions.create();
			}
		}
		
	} )();

} )( Onepager, jQuery );
