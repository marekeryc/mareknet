class ThemesController < ApplicationController
  def show
    @theme = Theme.find(params[:id])
    
    respond_to do |format|
      format.json
    end
  end
end