class ResetPasswordController < ApplicationController
  before_filter :load_user_using_perishable_token, :only => [:edit, :update]
  ssl_required :edit, :update
  layout :set_layout
  
  def new
    respond_to do |format|
      format.html
    end
  end
  
  def create
    @user = User.find_by_email(params[:email])
    if @user && !@user.is_cancelled?
      @user.reset_perishable_token!
      if request.domain == "onepagerapp.com"
        PostOffice.reset_password(@user).deliver
      else
        PostOffice.whitelabel_reset_password(@user).deliver
      end
      flash.now[:notice] = "An email has been sent to you with instructions on how to reset your password."
      render :action => :new
    else
      flash.now[:notice] = "Sorry... we can't seem find that email address.  Please check your entry and try again or feel free to <a href='/contact-us'>contact us</a>."
      render :action => :new
    end
  end
  
  def edit
  end
  
  def update
    @user.password = params[:user][:password]
    @user.reset_password = true
    if @user.save_without_session_maintenance && !@user.password.blank?
      flash[:notice] = "Password successfully updated."
      redirect_to new_signin_url
    else
      flash[:errors] = 'New password is invalid. Enter you email address to try again.'
      render :action => :new
    end
  end
  
private
  def load_user_using_perishable_token
    @user = User.find_using_perishable_token(params[:id])
    unless @user
      flash[:notice] = "Enter your email address above to reset your password."
      redirect_to new_reset_password_url
    end
  end

  def set_layout
    if request.domain == WHITELABEL_DOMAIN
      "whitelabel/application"
    else
      "application"
    end
  end
end