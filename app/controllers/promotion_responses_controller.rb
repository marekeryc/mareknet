class PromotionResponsesController < ApplicationController
  def create
    if params[:comment].blank? # Honey pot CAPTCHA
      @promotion_response = PromotionResponse.new(params[:promotion_response])
    else
      @promotion_response.errors.add(:base, "Please do not enter anything in the Comment field.")
    end
    
    respond_to do |format|
      if @promotion_response.save
        format.json { render :json => '{ "message": "Thank you for your submission" }' }
      else
        format.json { render :json => '{ "message": "' + @promotion_response.errors.full_messages.join(", ") + '" }', status: :unprocessable_entity }
      end
    end
  end
end