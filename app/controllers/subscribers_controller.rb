class SubscribersController < ApplicationController
  layout false
  
  def create
    @subscriber = Subscriber.new(params[:subscriber])
    
    respond_to do |format|
      if @subscriber.save
        format.json { render :json => '{ "message": "Check ' + @subscriber.email + ' to verify your email address." }' }
      else
        format.json { render :json => '{ "message": "' + @subscriber.errors.full_messages.join(", ") + '" }', status: :unprocessable_entity }
      end
    end
  end
  
  def subscribe
    @subscriber = Subscriber.find(Subscriber.decrypt(params[:id]))
    @subscriber.active = true
    @subscriber.crosscheck = "negative37892"
    @subscriber.save
    @site = @subscriber.site
    flash[:notice] = "You are now subscribed to the newsletter."
    render "thankyou"
  end
  
  def unsubscribe
    @subscriber = Subscriber.find(Subscriber.decrypt(params[:id]))
    @subscriber.active = false
    @subscriber.crosscheck = "negative37892"
    @subscriber.save
    @site = @subscriber.site
    flash[:notice] = "You have been unsubscribed from the newsletter."
    render "thankyou"
  end
end