class Dashboard::FormsController < ApplicationController
  before_filter :require_user
  before_filter :trial_dashboard
  before_filter :load_site_and_page
  layout :set_layout
  
  def index
    require 'will_paginate/array'

    @containers = Container.where(:page_id => @site.pages.collect{ |p| p.id }).where("containers.child_object" => "contact_form_fields").where(:page_id => current_user.pages)

    if @containers.length != 0
      if !params[:container_id].blank?
        @container = Container.where(:page_id => current_user.pages).find(params[:container_id])
      else
        @container = @containers.first
      end
      @cf_responses = @container.contact_form_responses.paginate(:page => params[:page], :per_page => 10)
    end


    respond_to do |format|
      format.html
    end
  end

  def show  
    @container = Container.where(:page_id => current_user.pages).find(params[:id])

    respond_to do |format|
      format.csv
    end
  end

private
  def set_layout
    if request.domain == WHITELABEL_DOMAIN
      "whitelabel/dashboard"
    else
      "dashboard"
    end
  end
end