class Dashboard::SubscribersController < ApplicationController
  layout false
  before_filter :require_user
  before_filter :trial_dashboard
  before_filter :load_site_and_page

  def index
    respond_to do |format|
      format.csv { render :text => "email\n" + @site.subscribers.collect {|s| s.email}.join("\n") }
    end
  end
end
