class Dashboard::SettingsController < ApplicationController
  before_filter :require_user
  before_filter :trial_dashboard
  before_filter :load_site_and_page
  layout :set_layout

  def edit
    respond_to do |format|
      format.html
    end
  end
  
  def update
    respond_to do |format|
      if @site.update_attributes(params[:site])
        format.html { redirect_to dashboard_setting_path, notice: 'Your settings were successfully saved.' }
      else
        format.html { render action: "show" }
      end
    end
  end
  
  def destroy
    @site.remove_favicon_image!
    
    respond_to do |format|
      format.html { redirect_to dashboard_setting_path, notice: 'Favicon was successfully deleted.' }
    end
  end

private
  def set_layout
    if request.domain == WHITELABEL_DOMAIN
      "whitelabel/dashboard"
    else
      "dashboard"
    end
  end
end