class Dashboard::SitesController < ApplicationController
  layout 'dashboard'#, :except => :export
  # layout false, :only => :export
  ssl_required :edit, :update, :update_free_domain_user
  before_filter :require_user
  before_filter :load_site_and_page
  before_filter :determine_form_url, :only => [:edit, :update, :update_free_domain_user]
  
  def edit
    @user = current_user
    @payment = Payment.new
  end
  
  def update    
    @user = current_user
    @site.attributes = params[:site]
    @site.domain_name = @site.purchased_domain if @site.domain_type == "new_custom_domain"
    allvalid = false

    if @site.purchased_domain_was.blank? and !@site.purchased_domain.blank?
      @site.domain_name = @site.purchased_domain
      @payment = Payment.new(params[:payment])
      @payment.user = current_user
      if @site.valid? and @payment.valid?
        allvalid = true
      end
    else
      if @site.valid?
        allvalid = true
      end
    end

    respond_to do |format|
      if allvalid
        @site.save
        if (@payment && @payment.valid?)
          account = Recurly::Account.find(current_user.recurly_account_uuid)
          transaction = account.transactions.create(:amount_in_cents => 1500, :description => "Domain registration for #{@site.domain_name}.", :accounting_code => @user.id)
          @site.register_domain
          PostOffice.send_simple_message("#{current_user.email} bought a domain", "An existing user bought a domain", "support@onepagerapp.com", "support@onepagerapp.com")
        end
        format.html { redirect_to edit_dashboard_domain_path, notice: 'Domain information was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end
  
  def update_free_domain_user
    @user = current_user
    @site.attributes = params[:site]
    @site.domain_name = @site.purchased_domain if @site.domain_type == "new_custom_domain"
    allvalid = false

    if @site.purchased_domain_was.blank? and !@site.purchased_domain.blank?
      @site.domain_name = @site.purchased_domain
      if @site.valid?
        allvalid = true
      end
    else
      if @site.valid?
        allvalid = true
      end
    end

    respond_to do |format|
      if allvalid
        if @site.purchased_domain_was.blank? and !@site.purchased_domain.blank?
          @site.save
          @site.register_domain
          PostOffice.send_simple_message("#{current_user.email} bought a domain", "An existing user bought a domain", "support@onepagerapp.com", "support@onepagerapp.com")
        else
          @site.save
        end
        format.html { redirect_to edit_dashboard_domain_path, notice: 'Domain information was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def export
    respond_to do |format|
      format.html { send_data(render_to_string(:layout => false).gsub('/uploads/', 'http://onepagerapp.com/uploads/').gsub('/assets/', 'http://onepagerapp.com/assets/'), :filename => "export.html", :type => "text/html") }
    end
  end
  
private
  def determine_form_url
    if current_user.payment_frequency == "Yearly" and current_user.free_domain_qualified?
      @form_url = update_free_domain_user_dashboard_domain_path
    else
      @form_url = dashboard_domain_path
    end
  end
end