class Dashboard::AnalyticsController < ApplicationController
  load_and_authorize_resource :site, :parent => false
  before_filter :require_user
  before_filter :trial_dashboard
  before_filter :load_site_and_page
  layout :set_layout

  def show
    @page = @site.pages.last

    params[:time_travel] = params[:time_travel] == 'month' ? 'month' : 'week'
    @analytics = Analytic.where(:site_id => session[:site_id], :time_travel => params[:time_travel]).first
    
    respond_to do |format|
      format.html
    end
  end

private
  def set_layout
    if request.domain == WHITELABEL_DOMAIN
      "whitelabel/dashboard"
    else
      "dashboard"
    end
  end
end