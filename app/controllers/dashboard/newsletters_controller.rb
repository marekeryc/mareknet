class Dashboard::NewslettersController < ApplicationController
  before_filter :require_user
  before_filter :trial_dashboard
  before_filter :load_site_and_page
  layout :set_layout
  
  def new
    @newsletter = Newsletter.new
    @subscribers = @site.subscribers
    
    respond_to do |format|
      format.html
    end
  end
  
  def confirm
    @newsletter = Newsletter.new(params[:newsletter])
    @newsletter.site = @site
    @subscribers = @site.subscribers
    
    respond_to do |format|
      if !@newsletter.valid?
        format.html { render action: "new" }
      else
        format.html
      end
    end
  end
  
  def create
    @newsletter = Newsletter.new(params[:newsletter])
    @newsletter.site = @site
    @subscribers = @site.subscribers
    
    respond_to do |format|
      if @newsletter.save
        format.html { redirect_to(new_dashboard_newsletter_path, :notice => 'Congrats! Your newsletter was successfully sent.') }
      else
        format.html { render action: "new" }
      end
    end
  end

private
  def set_layout
    if request.domain == WHITELABEL_DOMAIN
      "whitelabel/dashboard"
    else
      "dashboard"
    end
  end
end