class UserSessionsController < ApplicationController
  before_filter :require_no_user, :only => [:new, :create]
  before_filter :require_user, :only => :destroy
  ssl_required :new, :create
  ssl_allowed :destroy
  layout :set_layout
  
  def new
    @user_session = UserSession.new
  end
  
  def create
    @user_session = UserSession.new(params[:user_session])
    if @user_session.save
      session[:site_id] = @user_session.user.sites.last.id
      if @user_session.user.payment_plan != "Child"
        redirect_back_or_default account_sites_url(:host => "onepagerapp.com")
      else
        redirect_back_or_default account_sites_url
      end      
    else
      render :action => :new
    end
  end
  
  def destroy
    current_user_session.destroy
    reset_session
    flash[:notice] = "Logout successful!"
    redirect_back_or_default new_signin_url
  end

private
  def set_layout
    if request.domain == WHITELABEL_DOMAIN
      "whitelabel/application"
    else
      "application"
    end
  end
end