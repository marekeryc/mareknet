class ApiController < ApplicationController
  skip_before_filter :check_if_simple_admin
  ssl_allowed :create_user, :paypal_callback

  def create_user
    api_key_to_user_id = {
      "1d7e4b8c313f590f6988ac38b3ff754d16f" => 4940
    }

    if api_key_to_user_id[params[:api_key]]
      u = User.new
      u.email = params[:email]
      u.password = params[:password]
      u.payment_frequency = "Child"
      u.payment_plan = "Child"
      u.site_ids = "notnull"
      u.save

      s = Site.new
      if !u.errors.any?
        s = Site.default
        s.company_name = params[:company_name] if !params[:company_name].blank?
        s.user_id = api_key_to_user_id[params[:api_key]]
        s.domain_type = "no_custom_domain"
        s.onepager_address = params[:page_address] || s.trial_address
        s.save

        if !s.errors.any?
          SitePermission.create(:user_id => u.id, :parent_user_id => api_key_to_user_id[params[:api_key]], :site_id => s.id)
        end
      end

      if !u.errors.any? && !s.errors.any?
        head :ok
      else
        render :text => (s.errors.full_messages + u.errors.full_messages).join(","), :status => :unprocessable_entity
      end
    end
  end

  def paypal_callback
    if !params["Field620"].blank? && params["HandshakeKey"] == "HFQ6G1O72KU3S9z5cbhXSsB7kHgkYGzR"
      u = User.new
      # u.email = params[:payer_email]
      u.email = params["Field620"]
      password = (0...8).map{97.+(rand(26)).chr}.join
      u.password = password
      u.payment_frequency = "Child"
      u.payment_plan = "Child"
      u.site_ids = "notnull"
      u.save

      s = Site.new
      s = Site.default
      s.company_name = "My Company Name"
      s.user_id = 11017
      s.domain_type = "no_custom_domain"
      s.onepager_address = s.trial_address
      s.save

      SitePermission.create(:user_id => u.id, :parent_user_id => 11017, :site_id => s.id)

      PostOffice.new_paypal_callback_user(u, s, password).deliver
    end

    head :ok
  end
end
