class ContactFormResponsesController < ApplicationController
  def create
    if params[:comment].blank? # Honey pot CAPTCHA
      @empty_required_fields = ContactFormResponse.create_many(params[:contact_form_responses])
    else
      @empty_required_fields = ["Please do not enter anything in the Comment field."]
    end
    
    respond_to do |format|
      if @empty_required_fields.length == 0
        format.json { render :json => '{ "message": "Thank you for your submission" }' }
      else
        format.json { render :json => '{ "message": "The following fields are required: ' + @empty_required_fields.join(", ") + '" }', status: :unprocessable_entity }
      end
    end
  end
end