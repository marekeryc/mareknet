class ApplicationController < ActionController::Base
  include ::SslRequirement
  protect_from_forgery
  helper_method :current_user_session, :current_user
  before_filter :set_locale, :check_if_simple_admin
  WHITELABEL_DOMAIN = "easysiteadmin.com"
  
  def merge_position_into_params!(params)
    if !params.blank?
      params.each_with_index do |p, i|
        params[p[0]] = p[1].merge({:position => "#{i}"})
      end
    end
  end
  
  def admin_authenticate
    allowed_ips = ["127.0.0.1", "114.43.153.151"]
    # if !allowed_ips.include? request.remote_ip
      # redirect_to "/"
    # end
    authenticate_or_request_with_http_basic do |user_name, password|
      user_name == 'clickbuilder' && password == 'click@builder'
    end
  end
  
  def ssl_required?
    return false if Rails.env == 'development'
    super
  end

  def check_if_simple_admin
    if request.domain == WHITELABEL_DOMAIN
      allowed_actions = {"user_sessions" => ["new", "create", "destroy"], "reset_password" => ["new", "create", "edit", "update"], "sites" => ["index", "show", "create"], "themes" => ["show"], "pages" => ["edit", "update", "css", "css_ie", "show", "update_positions"], "analytics" => ["show"], "newsletters" => ["new", "confirm", "create"], "forms" => ["index", "show"], "settings" => ["show", "edit", "update", "destroy"], "subscribers" => ["create", "subscribe", "unsubscribe"], "contact_form_responses" => ["create"], "promotion_responses" => ["create"]}

      if request.fullpath == "/"
        redirect_to new_signin_url
      elsif !allowed_actions[controller_name].try(:include?, action_name) && !request.fullpath.include?("/content_types")
        render :file => "#{Rails.root}/public/404_whitelabel.html", :layout => false, :status => 404
      end
    else
      host_name
    end
  end
  
  def host_name
    domains = Hash.new
    domains["development"] = ["localhost", "makeitgolden.com", "onepager.local", "machost", "paidlancer.com", "10.1.2.17"]
    domains["test"] = ["test.host"]
    domains["production"] = ["onepagerapp.com", "paidlancer.com"]
    
    allowed_actions = {"subscribers" => ["create", "subscribe", "unsubscribe"], "contact_form_responses" => ["create"], "pages" => ["css", "css_ie"], "static" => ["robots"], "promotion_responses" => ["create"]}
    
    # all requests to application's domain (e.g. onepagerapp.com), skips this block
    # a request to a custom domain that is part of the allowed_actions, skips this block
    # requests to a customdomain.com/anything that isn't part of the allowed_actions, tries to render site/page

    if !domains[Rails.env].include?(request.host) && !allowed_actions[controller_name].try(:include?, action_name)
      @site = Site.where(:domain_name => request.host.gsub("www.", "")).where("domain_type != 'no_custom_domain'").first
      if @site
        if authorized_to_view_page?(@site, params[:q])
          @page = @site.pages.last
          @user = @site.user
          render "pages/show", :layout => 'pages'
        else
          render "pages/password", :layout => false
        end
      else
        render :file => "#{Rails.root}/public/404.html", :layout => false, :status => 404
      end
    end
  end
  
private
  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end
  
  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.user
  end
  
  def require_user
    unless current_user
      store_location
      flash[:notice] = "You must be logged in to access this page"
      redirect_to new_signin_url
      return false
    end
  end

  def require_no_user
    if current_user
      store_location
      flash[:notice] = "You must be logged out to access this page"
      redirect_to '/dashboard' #to do
      return false
    end
  end
  
  def store_location
    session[:return_to] = request.url
  end
  
  def redirect_back_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end
  
  def load_site_and_page
    site_id = session[:site_id] || current_user.sites.last.id
    if Site.exists?(site_id)
      @site = Site.find(site_id)
    else
      session[:site_id] = current_user.sites.last.id
      @site = Site.find(session[:site_id])
    end
    @page = @site.pages.last
  end
  
  def trial_dashboard
    if current_user.payment_frequency == "Trial"
      load_site_and_page
      render "static/trial_dashboard", :layout => "dashboard"
    end
  end
  
  def current_ability
    @current_ability ||= Ability.new(current_user, cookies.signed[:temp_page_id])
  end
  
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to new_signin_url, :notice => exception.message
  end

  def authorized_to_view_page?(site, password)
    if site.password_protected? and password != "sausalito" + site.id.to_s
      return false
    else
      return true
    end
  end

  def set_locale
    if !request.env['HTTP_ACCEPT_LANGUAGE'].blank?
      I18n.locale = request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first || I18n.default_locale
    end
  end
end
