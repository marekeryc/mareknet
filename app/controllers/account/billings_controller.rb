class Account::BillingsController < ApplicationController
  layout "account"
  ssl_required :edit, :update
  before_filter :require_user
  before_filter :load_site_and_page

  def index
    @transactions = current_user.transactions.where("transaction_status != ?", "Expired")
  end

  def show
    @transaction = Transaction.find(params[:id])

    render :layout => false
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    @user.payments[0].last_four_will_change! if !params[:user][:payments_attributes].try(:[], "0").try(:[], "credit_card_number").blank?
    
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to edit_account_billing_path, notice: 'Billing information was successfully updated.' }
      else
        format.html { render "edit" }
      end
    end
  end
end