class Account::UsersController < ApplicationController
  layout "account"
  ssl_required :upgrade, :upgrade_update
  before_filter :require_user
  before_filter :load_site_and_page
  require 'will_paginate/array'
  
  def index
    @child_users = SitePermission.where(:parent_user_id => current_user.id).group(:user_id).collect { |sp| sp.user }
    @all_child_users = SitePermission.where(:parent_user_id => current_user.id).group(:user_id).collect { |sp| sp.user }

    if !params[:search].blank?
      @search_results = Array.new
      @search_results += @child_users.select{ |u| u.first_name.downcase.include? params[:search].downcase }
      @search_results += @child_users.select{ |u| u.last_name.downcase.include? params[:search].downcase }
      @search_results += @child_users.select{ |u| u.email.downcase.include? params[:search].downcase }
      @child_users = @search_results.uniq
    end
    @child_users = @child_users.paginate(:page => params[:page], :per_page => 20)
  end
  
  def new
    @user = User.new
    
    respond_to do |format|
      format.js
    end
  end
  
  def create
    @user = User.new(params[:user])
    @user.payment_frequency = "Child"
    @user.payment_plan = "Child"
    @password = (0...8).map{97.+(rand(26)).chr}.join
    @user.password = @password

    if params[:user][:admin] == "1"
      @user.admin = true
    else
      @user.admin = false
    end

    respond_to do |format|
      if @user.save
        @user.site_ids.each do |sid|
          SitePermission.create(:user_id => @user.id, :parent_user_id => current_user.id, :site_id => sid)
        end
        @child_users = SitePermission.where(:parent_user_id => current_user.id).group(:user_id).collect { |sp| sp.user }
        format.js
      else
        format.js { render text: @user.errors.full_messages.first, status: :unprocessable_entity }
      end
    end
  end
  
  def edit
    @user = User.find(params[:id])

    respond_to do |format|
      format.js
    end
  end
  
  def update
    if SitePermission.where(:user_id => params[:id], :parent_user_id => current_user.id).count > 0
      @user = User.find(params[:id])
    end
    if params[:user][:admin] == "1"
      @user.admin = true
    else
      @user.admin = false
    end
    
    respond_to do |format|
      if @user.update_attributes(params[:user])
        @user.site_permissions.destroy_all
        @user.site_ids.each do |sid|
          SitePermission.create(:user_id => @user.id, :parent_user_id => current_user.id, :site_id => sid)
        end
        format.js
      else
        format.js { render text: @user.errors.full_messages.first, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    if SitePermission.where(:user_id => params[:id], :parent_user_id => current_user.id).count > 0
      u = User.find(params[:id])
      u.destroy
      SitePermission.where(:user_id => u.id, :parent_user_id => current_user.id).destroy_all
      flash[:notice] = "#{u.email} was successfully deleted."
    end
    redirect_to account_users_path
  end
  
  def upgrade
    @user = current_user
    @user.country = "United States"
    @user.payment_frequency = "Yearly"
        
    case @user.payment_plan
      when 'Trial'
        @user.payment_plan = "Plus"
        @user.payments.build if @user.payments.length == 0
      when 'Starter'
        @user.payment_plan = "Plus"
      when 'Plus'
        @user.payment_plan = "Premium"
      when 'Premium'
        @user.payment_plan = "Agency"
      when 'Agency'
        @user.payment_plan = "Agency"
    end
    
    render :layout => 'users'
  end
  
  def upgrade_update
    @user = current_user
    @user.attributes = params[:user]
    @user.payments[0].last_four_will_change! if @user.payments.length > 0
    @user.payments[0].is_upgrading = true

    respond_to do |format|
      if @user.save
        format.html { redirect_to thankyou_signup_url(:plan => @user.payment_frequency, :upgrade => true) }
      else
        format.html { render action: "upgrade", layout: "users" }
      end
    end
  end
  
  def feedback_send
    respond_to do |format|
      if !params[:feedback].blank?
        message = "Question / Feedback: #{params[:feedback]}"
        PostOffice.send_simple_message("#{current_user.email} submitted a question", message, "support@onepagerapp.com", current_user.email)
        format.js
      else
        format.js { head :unprocessable_entity }
      end
    end
  end
end