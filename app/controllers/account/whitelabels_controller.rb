class Account::WhitelabelsController < ApplicationController
  layout "account"
  ssl_required :edit, :update
  before_filter :require_user
  before_filter :load_site_and_page

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to edit_account_whitelabel_path, notice: 'Setting was successfully updated.' }
      else
        format.html { render "edit" }
      end
    end
  end
end