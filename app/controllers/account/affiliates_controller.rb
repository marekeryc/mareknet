class Account::AffiliatesController < ApplicationController
  layout 'account'
  before_filter :require_user
  before_filter :load_site_and_page
  
  def show
    @affiliate = Affiliate.find_by_user_id(current_user.id)
    
    respond_to do |format|
      if @affiliate.blank?
        format.html { redirect_to new_account_affiliate_url }
      else
        @affiliates = User.where("affiliate_of = ?", current_user.id).where("payment_frequency != 'Trial'")
        format.html
      end
    end
  end
  
  def new
    @affiliate = Affiliate.new(:country => "United States")
    
    respond_to do |format|
      format.html
    end
  end
  
  def create
    @affiliate = Affiliate.new(params[:affiliate])
    @affiliate.user_id = current_user.id
    
    respond_to do |format|
      if @affiliate.save
        format.html { redirect_to account_affiliate_path, notice: 'Your affiliate account was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end
end