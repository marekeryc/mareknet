class Account::SettingsController < ApplicationController
  ssl_required :edit, :update
  before_filter :require_user
  before_filter :load_site_and_page
  layout :set_layout

  def edit
    @user = current_user
  end

	def update
    @user = current_user
    
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to account_settings_path, notice: 'Account was successfully updated.' }
      else
        @user.email = @user.email_was
        format.html { render "edit" }
      end
    end
  end

private
  def set_layout
    if request.domain == WHITELABEL_DOMAIN
      "whitelabel/account"
    else
      "account"
    end
  end
end