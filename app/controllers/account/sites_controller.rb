class Account::SitesController < ApplicationController
  before_filter :require_user
  layout :set_layout
  load_and_authorize_resource :only => :export
  
  def index
    @sites = current_user.sites.where('company_name LIKE ? or domain_name LIKE ? or onepager_address LIKE ?', "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%").page(params[:page])
  end

  def create
    s = Site.default
    s.onepager_address = s.trial_address
    s.domain_type = "no_custom_domain"

    if current_user.payment_plan == "Child" && current_user.admin == true
      parent = SitePermission.find_by_user_id(current_user.id).parent_user
      s.user_id = parent.id
      s.save
      sp = SitePermission.create(:user_id => current_user.id, :site_id => s.id, :parent_user_id => parent.id)
    else
      s.user_id = current_user.id
      s.save
    end

    redirect_to edit_site_page_url(s, s.pages.last)
  end
  
  def destroy 
    s = Site.find(params[:id])
    
    s.active = false
    s.onepager_address = s.trial_address
    s.domain_name = nil
    s.domain_type = "no_custom_domain"
    s.save
    
    flash[:notice] = "#{s.company_name} was successfully deleted."
    session[:site_id] = current_user.sites.last.id
    redirect_to account_sites_path
  end
  
private
  def set_layout
    if request.domain == WHITELABEL_DOMAIN
      "whitelabel/account"
    else
      "account"
    end
  end
end