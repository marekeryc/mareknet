class PagesController < ApplicationController
  load_and_authorize_resource :except => [:show, :css, :css_ie]
  
  def show
    if !params[:onepager_address].blank?
      @site = Site.find_by_onepager_address(params[:onepager_address])
      @page = @site.try(:pages).try(:last)
      @user = @site.try(:user)
    else # is for show.json
      @page = Page.find(params[:id])
      @site = @page.site
      @user = @site.user
    end
    
    respond_to do |format|
      if @user
        if authorized_to_view_page?(@site, params[:q]) == false
          format.html { render "password", :layout => false }
          format.json # If page id, then just render show for show.json.erb
        elsif @site.custom_domain? # Redirects to site's custom domain
          format.html { redirect_to "http://" + @site.domain_name, :status => :moved_permanently }
          format.json # If page id, then just render show for show.json.erb
        else
          format.html # Show page by onepager address
          format.json # If page id, then just render show for show.json.erb
        end
      else # Kinda the 404 call for the whole site due to route get ':onepager_address' => 'pages#show'
        format.html { 
          if request.domain == "easysiteadmin.com"
            render :file => "#{Rails.root}/public/404_whitelabel.html", :layout => false, :status => 404
          else
            render :file => "#{Rails.root}/public/404.html", :layout => false, :status => 404
          end
        }
        format.json # If page id, then just render show for show.json.erb
      end
    end
  end
  
  def edit
    @page = Page.find(params[:id])
    @site = @page.site
    session[:site_id] = @site.id
    
    @themes = Theme.order(:position)
  end
  
  def update
    @page = Page.find(params[:id])
    @site = @page.site
    
    respond_to do |format|
      if @site.update_attributes(params[:site])
        format.js
      else
        format.js { render :text => @site.errors.full_messages.join(", ") }
      end
    end
  end
  
  # ----------------
  # Non-CRUD stuff
  # ----------------
  def css
    @page = Page.find(params[:id])
    @site = @page.site
    
    render :layout => false, :content_type => 'text/css'
  end
  
  def css_ie
    @page = Page.find(params[:id])
    @site = @page.site
    @opacity = @site.content_background_opacity.to_f == 0 ? '00' : (@site.content_background_opacity.to_f / 100 * 255).floor.to_s(16)
    
    render :layout => false, :content_type => 'text/css'
  end
  
  def update_positions
    Page.transaction do
      params[:order].each do |x|
        x = JSON.parse(x)
        x["model"].constantize.update_all(["position = ?", x["position"]], ["id = ?", x["id"]])
      end
    end
    
    head :ok
  end
end