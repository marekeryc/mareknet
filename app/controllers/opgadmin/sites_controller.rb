class Opgadmin::SitesController < ApplicationController
  before_filter :admin_authenticate
  ssl_required :destroy, :activate, :clone, :dont_renew_domain

  def destroy
  	s = Site.find(params[:id])
  	s.active = false
  	s.save

  	redirect_to opgadmin_user_url(s.user), notice: "#{s.company_name} site was successfully deactivated."
  end

  def activate
  	s = Site.unscoped.find(params[:id])
  	s.active = true
  	s.save

  	redirect_to opgadmin_user_url(s.user), notice: "#{s.company_name} site was successfully activated."
  end

  def clone
    ActiveRecord::Base.observers.disable :all
    original_site = Site.find(params[:id])
    new_site = original_site.dup
    new_site.domain_type = "no_custom_domain"
    new_site.onepager_address = new_site.trial_address
    new_site.logo_image = File.open(original_site.logo_image.current_path)
    new_site.save

    original_site.pages.each do |p|
      new_page = p.dup
      new_page.site_id = new_site.id
      new_page.save
      p.buttons.each do |b|
        new_button = b.dup
        new_button.page_id = new_page.id
        new_button.save
      end
      p.contact_sections.each do |cs|
        new_cs = cs.dup
        new_cs.page_id = new_page.id
        new_cs.save
      end
      p.containers.each do |c|
        new_container = c.dup
        new_container.page_id = new_page.id

        c.contact_form_fields.each do |cff|
          new_cff = cff.dup
          new_cff.save
          new_container.contact_form_fields << new_cff
        end
        c.downloads.each do |d|
          new_download = d.dup
          new_download.filename = d.filename
          new_download.save
          new_container.downloads << new_download
        end
        c.gallery_images.each do |gi|
          new_gi = gi.dup
          new_gi.filename = File.open(gi.filename.current_path)
          new_gi.save
          new_container.gallery_images << new_gi
        end
        c.hours.each do |h|
          new_hour = h.dup
          new_hour.save
          new_container.hours << new_hour
        end
        c.services.each do |s|
          new_service = s.dup
          new_service.save
          new_container.services << new_service
        end
        if c.social_button
          new_social_button = c.social_button.dup
          new_social_button.container_id = new_container.id
          new_social_button.save
          c.social_button.social_links.each do |sl|
            new_social_link = sl.dup
            new_social_link.social_button_id = new_social_button.id
            new_social_link.save
          end
          new_container.social_button = new_social_button
        end

        new_container.save
      end
      p.embeds.each do |e|
        new_embed = e.dup
        new_embed.page_id = new_page.id
        new_embed.save
      end
      p.iframes.each do |i|
        new_iframe = i.dup
        new_iframe.page_id = new_page.id
        new_iframe.save
      end
      p.social_feeds.each do |sf|
        new_sf = sf.dup
        new_sf.page_id = new_page.id
        new_sf.save
      end
      p.text_areas.each do |t|
        new_text_area = t.dup
        new_text_area.page_id = new_page.id
        new_text_area.save
      end
    end
    ActiveRecord::Base.observers.enable :all

    redirect_to opgadmin_user_url(original_site.user), notice: "#{original_site.company_name} site was successfully cloned."
  end

  def dont_renew_domain
    require 'opensrs'

    server = Site.opensrs_server

    site = Site.find(params[:id])

    response = server.call(
      :action => "modify",
      :object => "domain",
      :domain => site.purchased_domain,
      :attributes => {
        :data => "expire_action",
        :affect_domains => 0,
        :auto_renew => 0,
        :let_expire => 0,
        :domain => site.purchased_domain
      }
    )
    if response.success?
      redirect_to opgadmin_user_url(site.user), notice: "#{site.purchased_domain}'s account was successfully dont renew domain'd."
    else
      redirect_to opgadmin_user_url(site.user), notice: "An error occurred while dont renew domain #{site.purchased_domain}. #{response.response}"
    end
  end
end