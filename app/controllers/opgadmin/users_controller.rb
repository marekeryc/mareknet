class Opgadmin::UsersController < ApplicationController
  layout 'admin'
  before_filter :admin_authenticate
  ssl_required :index, :show, :update, :cancel, :activate, :destroy, :signin_as
  
  def index
    @users = User.where("users.id IS NOT NULL").paginate(:page => params[:page])
    @users = @users.where("payment_plan = :plan", :plan => "#{params[:plan]}") if !params[:plan].blank?
    @users = @users.where("active = :bool", :bool => params[:active]) if !params[:active].blank?
    @users = @users.joins(:sites).where("users.email LIKE :query OR concat(users.first_name, ' ', users.last_name) LIKE :query OR sites.company_name LIKE :query OR sites.onepager_address LIKE :query OR sites.domain_name LIKE :query", :query => "%#{params[:search]}%") if !params[:search].blank?
  end
  
  def show
    @user = User.find(params[:id])
    @sites = @user.sites.unscoped.where("user_id = #{@user.id}")
    @affiliate = Affiliate.find_by_user_id(@user.id) if @user.is_affiliate?
  end
  
  def update
    u = User.find(params[:id])
    u.update_column(:allow_script_embeds, !u.allow_script_embeds)
    redirect_to request.referrer, notice: "#{u.email} was successfully updated."
  end
  
  def cancel
    u = User.find(params[:id])
    u.cancellation_date = params[:cancellation_date]
    u.payment_frequency = "Trial"
    u.payment_plan = "Trial"
    u.sites.each do |s|
      s.domain_type = "no_custom_domain"
      s.onepager_address = s.trial_address
      s.save
    end
    if u.save!
      begin
        p = u.payments.last
        gateway.cancel_recurring(p.subscriber_id_monthly) if !p.subscriber_id_monthly.blank?
        gateway.cancel_recurring(p.subscriber_id_annual) if !p.subscriber_id_annual.blank?
        Recurly::Subscription.find(u.recurly_subscription_uuid).cancel if !u.recurly_subscription_uuid.blank? 
      rescue
        PostOffice.send_simple_message("#{u.email}'s account cancelled with errors", "#{u.email} did not have payments entries in the database but their account was cancelled.  Look into it. #{opgadmin_user_path(u)}")
      end
      redirect_to opgadmin_user_path(u), notice: "#{u.email}'s account was successfully cancelled."
    else
      redirect_to opgadmin_user_path(u), notice: "An error occurred while cancelling #{u.email}'s account."
    end

  end

  def activate
    u = User.find(params[:id])
    u.update_column(:active, true)
    Site.unscoped.where(:user_id => u.id).each do |s|
      s.active = true
      s.save
    end
    redirect_to request.referrer, notice: "#{u.email} was successfully reactivated."
  end

  def destroy
    u = User.find(params[:id])
    u.update_column(:active, false)
    u.sites.each do |s|
      s.update_column(:active, false)
    end

    # u = User.find(params[:id])
    # u.sites.each do |s|
    #   s.pages.each do |p|
    #     p.content.each do |c|
    #       c.destroy
    #     end
    #     p.destroy
    #   end
    #   s.analytics.each do |a|
    #     a.destroy
    #   end
    #   Newsletter.where(:site_id => s.id).each do |n|
    #     n.destroy
    #   end
    #   s.destroy
    # end
    # u.payments.each do |p|
    #   p.destroy
    # end
    # u.destroy
    
    redirect_to request.referrer, notice: "#{u.email} was successfully deactivated."
  end

  def signin_as
    @user = User.find(params[:id])
    UserSession.create(@user)
    redirect_to "/account/sites"
  end

  def gateway
    ActiveMerchant::Billing::Base.gateway('authorize_net').new(:login => APP_CONFIG['authorize_login'], :password => APP_CONFIG['authorize_password'], :test => APP_CONFIG['authorize_test'])
  end
end
