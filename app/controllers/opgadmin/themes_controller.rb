class Opgadmin::ThemesController < ApplicationController
  layout 'admin'
  before_filter :admin_authenticate
  ssl_required :index, :new, :create, :edit, :update, :destroy, :update_positions
  
  def index
    @themes = Theme.order(:position)
  end
  
  def new
    @theme = Theme.new
  end
  
  def create
    @theme = Theme.new(params[:theme])
    
    if @theme.save
      redirect_to opgadmin_themes_path
    else
      render "new"
    end
  end
  
  def edit
    @theme = Theme.find(params[:id])
  end
  
  def update
    @theme = Theme.find(params[:id])
    
    respond_to do |format|
      if @theme.update_attributes(params[:theme])
        format.html { redirect_to [:edit, :opgadmin, @theme], notice: 'Theme was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end
  
  def destroy
    @theme = Theme.find(params[:id])
    @theme.destroy
    
    redirect_to opgadmin_themes_path
  end
  
  
  def update_positions
    theme_ids = params[:theme]
    Theme.transaction do
      theme_ids.each_with_index do |s, i|
        Theme.update_all ["position = ?", i], ["id = ?", s]
      end
    end
    head :ok
  end
end