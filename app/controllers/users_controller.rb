class UsersController < ApplicationController
  ssl_required :new, :create
  ssl_allowed :check_domain_availability, :validate_promo_code, :get_city_state
  
  def new
    @user = User.new(:country => "United States", :payment_plan => params[:plan], :payment_frequency => params[:frequency], :promo_code => params[:promo_code])
    @site = Page.find(cookies.signed[:temp_page_id]).site
    @site.purchased_domain = "#{params[:domain]}.#{params[:tld].gsub(".", "")}" if !params[:domain].blank?
    if params[:ref] == "domainr"
      @site.domain_type = "new_custom_domain"
    elsif params[:ref] == "existing_custom_domain"
      @site.domain_type = "existing_custom_domain"
    else
      @site.domain_type = "no_custom_domain"
    end
    @site.onepager_address = @site.trial_address
    @user.sites << @site
    @payment = @user.payments.build
    
    respond_to do |format|
      format.html
    end
  end
  
  def create
    @user = User.new(params[:user])
    @site = @user.sites.last
    @payment = @user.payments[0]
    @site.domain_name = @site.purchased_domain if @site.domain_type == "new_custom_domain"
    
    success = false
    if @user.valid?
      if @user.payment_frequency == "Trial"
        success = true
      elsif @user.payment_frequency != "Trial"
        @user.recurly_account_uuid = SecureRandom.uuid
        # User is valid, now check to see if payment goes through
        response = @payment.recurring(@user.recurly_plan_code, request.remote_ip, request.env['HTTP_ACCEPT_LANGUAGE'])
        if response[:errors].blank?
          @user.recurly_subscription_uuid = response[:uuid]
          success = true
        else
          @user.errors[:base] << "There was an error processing your order.  Please check your info and try again. (" + response[:errors].to_s + ")"
          success = false
        end
      end
    end
    
    respond_to do |format|
      if success
        format.html {
          @user.affiliate_of = session[:affiliate]
          @user.save
          session[:affiliate] = nil
          cookies.delete(:temp_page_id)
          if @site.domain_type == "new_custom_domain"
            @site.register_domain
            @site.save
            @payment.recurring("domain-registration", request.remote_ip, request.env['HTTP_ACCEPT_LANGUAGE'])
          end
          redirect_to thankyou_signup_url(:plan => @user.payment_frequency)
        }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  def get_city_state
    u = User.get_city_state(params[:zip])
    
    if u
      render :json => u
    else
      render :json => "No data found for this zip code.", status: :unprocessable_entity 
    end
  end
  
  def check_domain_availability
    if Site.domain_available?(params[:domain])
      render :json => true
    else
      render :json => false
    end
  end
  
  def validate_promo_code
    @discount = User::PROMO_CODES[params[:promo_code]]
    
    respond_to do |format|
      if !@discount.blank?
        format.js { render :json => @discount[:user] }
      else
        format.js { head :unprocessable_entity }
      end
    end
  end
  
  def thankyou_survey
    respond_to do |format|
      if !params[:source].blank?
        message = "How'd you hear about Onepager?: #{params[:source]}"
        PostOffice.send_simple_message("#{current_user.email} completed the checkout survey", message)
        format.js
      else
        format.js { head :unprocessable_entity }
      end
    end
  end
end