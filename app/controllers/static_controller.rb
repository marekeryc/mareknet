class StaticController < ApplicationController
  def index
    session[:affiliate] = params[:af] if !params[:af].blank?
  end
  
  def show
    section = params[:section].to_s
    pagename = params[:pagename].to_s
    read_rss if pagename == "resources"

    respond_to do |format|
      if lookup_context.template_exists? ('/static/' + pagename)
        if pagename == 'justabout-and-onepager' || pagename == 'hello-facebook' || pagename == 'hello-reddit' || pagename == 'hello-disruptors' || pagename == 'hello-evening-edition' || pagename == 'hello-rftl' || pagename == 'hello-twitter'
          format.html { render 'static/' + pagename, :layout => false }
        else
          format.html { render pagename }
        end
      elsif lookup_context.template_exists? ('static/' + section + '/' + pagename)
        format.html { render 'static/' + section + '/' + pagename }
      else
        format.html { render :file => "#{Rails.root}/public/404.html", :layout => false, :status => 404 }
      end
    end
  end
  
  def sitemap
    @paying_users_sites = Site.joins(:user).where(["users.payment_frequency != ?", "Trial"])
    respond_to do |format|
      format.xml { render }
    end
  end
  
  def sitemaps
    @site = Site.select("id, domain_name").find(params[:id])
  end
  
  def robots
    @site = Site.select("id, domain_name").find_by_domain_name(request.domain)
  end

  def read_rss
    require 'rss'
    require 'open-uri'
    
    url = 'http://onepagerapp.com/blog/feed'
    open(url) do |rss|
      @feed = RSS::Parser.parse(rss)
      @feed = @feed.items[0..4]
    end
  end
end