class SitesController < ApplicationController
  ssl_allowed :create
  
  def show
    session[:site_id] = params[:id]
    redirect_back_or_default dashboard_root_path
  end
  
  def create
    session[:affiliate] = params[:af] if !params[:af].blank?
    current_user_session.destroy if current_user_session
    if cookies[:temp_page_id].blank?
      s = Site.default
      s.save
      p = s.pages.last
      cookies.permanent.signed[:temp_page_id] = p.id
    else
      p = Page.find(cookies.signed[:temp_page_id])
      s = p.site
    end
    
    if params[:ref] == "domainr"
      redirect_to "/signup?plan=Starter&frequency=Yearly&promo_code=domainr&domain=#{params[:domain]}&tld=#{params[:tld]}&ref=domainr"
    elsif !params[:ref].blank?
      redirect_to "/signup?plan=Starter&frequency=Yearly&promo_code=#{params[:promo_code]}&ref=#{params[:ref]}"
    else
      redirect_to edit_site_page_url(s, p)
    end
  end
end