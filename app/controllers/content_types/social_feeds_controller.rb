class ContentTypes::SocialFeedsController < ApplicationController
  layout false
  load_and_authorize_resource :except => :new
  
  def new
    @social_feed = SocialFeed.new
    
    respond_to do |format|
      format.js
    end
  end
  
  def create
    @social_feed = SocialFeed.new(params[:social_feed])
    
    respond_to do |format|
      if @social_feed.save
        format.js
      else
        format.js { render text: @social_feed.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @social_feed = SocialFeed.find(params[:id])
    
    respond_to do |format|
      if @social_feed.update_attributes(params[:social_feed])
        format.js
      else
        format.js { render text: @social_feed.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @social_feed = SocialFeed.find(params[:id])
    @social_feed.destroy
    
    respond_to do |format|
      format.js
    end
  end
end