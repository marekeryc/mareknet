class ContentTypes::ContactFormFieldsController < ApplicationController
  layout false
  load_and_authorize_resource :container, :parent => false, :except => :new
  
  def new
    @container = Container.new
    @container.default_contact_form_fields
    @container.title = "Contact Us"
    @container.format = current_user.email
    
    respond_to do |format|
      format.js
    end
  end
  
  def create
    merge_position_into_params!(params[:container][:contact_form_fields_attributes])
    @container = Container.new(params[:container])
    @container.child_object = "contact_form_fields"
    
    respond_to do |format|
      if @container.save
        @container.reload
        format.js
      else
        format.js { render text: @container.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @container = Container.find(params[:id])
    merge_position_into_params!(params[:container][:contact_form_fields_attributes])
    
    respond_to do |format|
      if @container.update_attributes(params[:container])
        @container.reload
        format.js
      else
        format.js { render text: @container.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @container = Container.find(params[:id])
    @container.destroy
    
    respond_to do |format|
      format.js
    end
  end
end