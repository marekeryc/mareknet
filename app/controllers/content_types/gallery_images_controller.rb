class ContentTypes::GalleryImagesController < ApplicationController
  layout false
  
  def create
    @gallery_images = Array.new
    @errors = ""
    @format = params[:container][:format] if !params[:container][:format].blank?
    
    params[:gallery_image_files][0..30].each do |p|
      g = GalleryImage.new
      g.filename = p
      g.container_id = params[:container_id] if params[:container_id] != "new"
      g.save
      if g.valid?
        @gallery_images << g
      else
        @errors << g.errors.full_messages.join(", ")
      end
    end
    
    respond_to do |format|
      format.js { render }
    end
  end
end