class ContentTypes::PaypalButtonsController < ApplicationController
  layout false
  load_and_authorize_resource :except => :new
  
  def new
    @paypal_button = PaypalButton.new

    respond_to do |format|
      format.js
    end
  end
  
  def create
    @paypal_button = PaypalButton.new(params[:paypal_button])
        
    respond_to do |format|
      if @paypal_button.save
        format.js
      else
        format.js { render text: @paypal_button.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @paypal_button = PaypalButton.find(params[:id])
    
    respond_to do |format|
      if @paypal_button.update_attributes(params[:paypal_button])
        format.js
      else
        format.js { render text: @paypal_button.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @paypal_button = PaypalButton.find(params[:id])
    @paypal_button.destroy
    
    respond_to do |format|
      format.js
    end
  end
end