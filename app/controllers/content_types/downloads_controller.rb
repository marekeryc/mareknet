class ContentTypes::DownloadsController < ApplicationController
  layout false
  
  def create
    @downloads = Array.new
    @errors = ""
    
    params[:download_files][0..30].each do |p|
      d = Download.new
      d.filename = p
      d.container_id = params[:container_id] if params[:container_id] != "new" # If container already exists, it gets passed in, otherwise its container-less and gets it container on container create
      d.save
      if d.valid?
        @downloads << d
      else
        @errors << d.errors.full_messages.join(", ")
      end
    end
    
    respond_to do |format|
      format.js { render }
    end
  end
end