class ContentTypes::HoursController < ApplicationController
  layout false
  load_and_authorize_resource :container, :parent => false, :except => :new
  
  def new
    @container = Container.new
    @container.hours.build
    @container.title = "Hours"
    
    respond_to do |format|
      format.js
    end
  end
  
  def create
    @container = Container.new(params[:container])
    @container.child_object = "hours"
    
    respond_to do |format|
      if @container.save
        format.js
      else
        format.js { render text: @container.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @container = Container.find(params[:id])
    
    respond_to do |format|
      if @container.update_attributes(params[:container])
        format.js
      else
        format.js { render text: @container.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @container = Container.find(params[:id])
    @container.destroy
    
    respond_to do |format|
      format.js
    end
  end
end