class ContentTypes::PromotionsController < ApplicationController
  layout false
  load_and_authorize_resource :except => :new
  
  def new
    @promotion = Promotion.new
    @promotion.email = current_user.email
    
    respond_to do |format|
      format.js
    end
  end
  
  def create
    @promotion = Promotion.new(params[:promotion])
    
    respond_to do |format|
      if @promotion.save
        format.js
      else
        format.js { render text: @promotion.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @promotion = Promotion.find(params[:id])
    
    respond_to do |format|
      if @promotion.update_attributes(params[:promotion])
        format.js
      else
        format.js { render text: @promotion.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @promotion = Promotion.find(params[:id])
    @promotion.destroy
    
    respond_to do |format|
      format.js
    end
  end
end