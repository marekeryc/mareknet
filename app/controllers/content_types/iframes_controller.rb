class ContentTypes::IframesController < ApplicationController
  layout false
  load_and_authorize_resource :except => :new
  
  def new
    @iframe = Iframe.new
    
    respond_to do |format|
      format.js
    end
  end
  
  def create
    @iframe = Iframe.new(params[:iframe])
    
    respond_to do |format|
      if @iframe.save
        format.js
      else
        format.js { render text: @iframe.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @iframe = Iframe.find(params[:id])
    
    respond_to do |format|
      if @iframe.update_attributes(params[:iframe])
        format.js
      else
        format.js { render text: @iframe.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @iframe = Iframe.find(params[:id])
    @iframe.destroy
    
    respond_to do |format|
      format.js
    end
  end
end