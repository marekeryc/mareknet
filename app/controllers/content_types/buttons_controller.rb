class ContentTypes::ButtonsController < ApplicationController
  layout false
  load_and_authorize_resource :except => :new
  
  def new
    @button = Button.new
    
    respond_to do |format|
      format.js
    end
  end
  
  def create
    @button = Button.new(params[:button])
    
    respond_to do |format|
      if @button.save
        format.js
      else
        format.js { render text: @button.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @button = Button.find(params[:id])
    
    respond_to do |format|
      if @button.update_attributes(params[:button])
        format.js
      else
        format.js { render text: @button.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @button = Button.find(params[:id])
    @button.destroy
    
    respond_to do |format|
      format.js
    end
  end
end