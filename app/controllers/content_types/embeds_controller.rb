class ContentTypes::EmbedsController < ApplicationController
  layout false
  load_and_authorize_resource :except => :new
  
  def new
    @embed = Embed.new
    
    respond_to do |format|
      format.js
    end
  end
  
  def create
    @embed = Embed.new(params[:embed])
    
    respond_to do |format|
      if @embed.save
        format.js
      else
        format.js { render text: @embed.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @embed = Embed.find(params[:id])
    
    respond_to do |format|
      if @embed.update_attributes(params[:embed])
        format.js
      else
        format.js { render text: @embed.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @embed = Embed.find(params[:id])
    @embed.destroy
    
    respond_to do |format|
      format.js
    end
  end
end