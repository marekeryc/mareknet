class ContentTypes::ScriptEmbedsController < ApplicationController
  layout false
  load_and_authorize_resource :except => :new
  
  def new
    @script_embed = ScriptEmbed.new
    
    respond_to do |format|
      format.js
    end
  end
  
  def create
    @script_embed = ScriptEmbed.new(params[:script_embed])
    
    respond_to do |format|
      if @script_embed.save
        format.js
      else
        format.js { render text: @script_embed.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @script_embed = ScriptEmbed.find(params[:id])
    
    respond_to do |format|
      if @script_embed.update_attributes(params[:script_embed])
        format.js
      else
        format.js { render text: @script_embed.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @script_embed = ScriptEmbed.find(params[:id])
    @script_embed.destroy
    
    respond_to do |format|
      format.js
    end
  end
end