class ContentTypes::ProductsController < ApplicationController
  layout false
  load_and_authorize_resource :except => :new
  
  def new
    @product = Product.new
    page = Page.find(params[:page_id])
    if page.site.gumroad_id.blank?
      @show_paypal_email = true
    else
      @show_paypal_email = false
    end

    respond_to do |format|
      format.js
    end
  end
  
  def create
    @product = Product.new(params[:product])

    if @product.page.site.gumroad_id.blank?
      response = @product.create_gumroad_user
      if response["success"]
        @product.page.site.update_column(:gumroad_id, response["user"]["id"])
      else
        @product.errors[:base] << response["errors"][0]
      end
    end
    
    respond_to do |format|
      if @product.errors.any? == false and @product.save
        format.js
      else
        format.js { render text: @product.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @product = Product.find(params[:id])
    
    respond_to do |format|
      if @product.update_attributes(params[:product])
        format.js
      else
        format.js { render text: @product.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    
    respond_to do |format|
      format.js
    end
  end
end