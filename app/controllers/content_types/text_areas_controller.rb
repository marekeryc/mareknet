class ContentTypes::TextAreasController < ApplicationController
  layout false
  load_and_authorize_resource :except => :new
  
  def new
    @text_area = TextArea.new
    
    respond_to do |format|
      format.js
    end
  end
  
  def create
    @text_area = TextArea.new(params[:text_area])
    
    respond_to do |format|
      if @text_area.save
        format.js
      else
        format.js { render text: @text_area.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @text_area = TextArea.find(params[:id])
    
    respond_to do |format|
      if @text_area.update_attributes(params[:text_area])
        format.js
      else
        format.js { render text: @text_area.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @text_area = TextArea.find(params[:id])
    @text_area.destroy
    
    respond_to do |format|
      format.js
    end
  end
end