class ContentTypes::SocialButtonsController < ApplicationController
  layout false
  load_and_authorize_resource :container, :parent => false, :except => :new
  
  def new
    @container = Container.new
    @container.build_social_button
    @container.social_button.social_links.build
    @container.title = "Social"
    
    respond_to do |format|
      format.js
    end
  end
  
  def create
    merge_position_into_params!(params[:container][:social_button_attributes][:social_links_attributes])
    @container = Container.new(params[:container])
    @container.child_object = "social_buttons"
    
    respond_to do |format|
      if @container.save
        format.js
      else
        format.js { render text: @container.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @container = Container.find(params[:id])
    merge_position_into_params!(params[:container][:social_button_attributes][:social_links_attributes])
    
    respond_to do |format|
      if @container.update_attributes(params[:container])
        format.js
      else
        format.js { render text: @container.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @container = Container.find(params[:id])
    @container.destroy
    
    respond_to do |format|
      format.js
    end
  end
end