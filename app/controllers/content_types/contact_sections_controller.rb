class ContentTypes::ContactSectionsController < ApplicationController
  layout false
  load_and_authorize_resource :except => :new
  
  def new
    @contact_section = ContactSection.new
    @contact_section.title = "Contact Us"
    
    respond_to do |format|
      format.js
    end
  end
  
  def create
    @contact_section = ContactSection.new(params[:contact_section])
    @page = @contact_section.page
    
    respond_to do |format|
      if @contact_section.save
        format.js
      else
        format.js { render text: @contact_section.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @contact_section = ContactSection.find(params[:id])
    @page = @contact_section.page
    
    respond_to do |format|
      if @contact_section.update_attributes(params[:contact_section])
        format.js
      else
        format.js { render text: @contact_section.errors.full_messages.join(", "), status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @contact_section = ContactSection.find(params[:id])
    @contact_section.destroy
    
    respond_to do |format|
      format.js
    end
  end
end