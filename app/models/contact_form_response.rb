class ContactFormResponse < ActiveRecord::Base
  attr_accessible :contact_form_field_id, :value
  
  belongs_to :contact_form_field
  
  # Validate the contact form response exists if the contact form field is required
  validates :value, :presence => true, :if => Proc.new { |c| c.contact_form_field.required? }
  
  def self.create_many(crs_params)
    cfr_objects_array = Array.new
    empty_required_fields = Array.new
    # Turns an array of params into objects, adds field name to array if response is not valid
    # "contact_form_responses"=>[{"value"=>"a", "contact_form_field_id"=>"10"}, {"value"=>"b", "contact_form_field_id"=>"11"}]
    crs_params.each do |c|
      cfr = ContactFormResponse.new(c)
      cfr_objects_array << cfr
      empty_required_fields << cfr.contact_form_field.field_name if !cfr.valid?
    end
    
    # If all responses are valid, create them
    if empty_required_fields.length == 0
      ContactFormResponse.transaction do
        cfr_objects_array.each {|cfr| cfr.save}
      end
      PostOffice.contact_form(cfr_objects_array).deliver
    end
    
    return empty_required_fields
  end
end