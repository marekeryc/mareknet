class Product < ActiveRecord::Base
  attr_accessible :page_id, :position, :name, :url, :description, :product_type, :price, :max_purchase_count, :country_available, :product_file, :preview_file, :remove_product_file, :remove_preview_file, :paypal_email
  attr_accessor :paypal_email

  belongs_to :page
  
  PRODUCT_TYPES = ['Physical Product', 'Digital Download', 'Services / Other']
  COUNTRIES_AVAILABLE = [['Anywhere', ''], ['USA', 'US'], ['UK', 'UK'], ['Canada', 'CA'], ['Australia', 'AU'], ['Japan', 'JP']]
  
  validates :name, :presence => true
  validates :price, :numericality => true
  validate :create_gumroad_link, :on => :create, :if => Proc.new { |p| p.errors.empty? }
  validate :update_gumroad_link, :on => :update, :if => Proc.new { |p| p.errors.empty? }
  
  mount_uploader :preview_file, PreviewFileUploader
  mount_uploader :product_file, ProductFileUploader
  
  def require_shipping
    if product_type == "Physical Product"
      return true
    else
      return false
    end
  end
  
  def url
    if product_type == "Digital Download"
      return read_attribute(:url)
    else
      return "http://onepagerapp.com/" + self.page.site.onepager_address
    end
  end
  
  def create_gumroad_user
    u = self.page.site.user
    response = RestClient.post("https://api.gumroad.com/v1/users", :name => self.page.site.company_name, :payment_address => self.paypal_email, :email => self.paypal_email, :token => "d358cd637ad86318e22ad5b8158ab8bf")
    j = JSON.load(response)
    
    return j
  end
  
  def create_gumroad_link
    params = {:token => "d358cd637ad86318e22ad5b8158ab8bf", :name => name, :url => url, :price => price * 100.0, :description => description, :country_available => country_available, :max_purchase_count => max_purchase_count, :require_shipping => require_shipping}
    params.merge!({:file => File.new(product_file.path, 'rb')}) if !product_file.blank?
    params.merge!({:preview => File.new(preview_file.path, 'rb')}) if !preview_file.blank?
    
    response = RestClient.post("https://api.gumroad.com/v1/users/#{self.page.site.gumroad_id}/links", params)
    j = JSON.load(response)
    
    if j["success"]
      self.gumroad_id = j["link"]["id"]
      self.gumroad_url  = j["link"]["short_url"]
    else 
      errors[:base] << j["errors"][0]
    end
  end
  
  def update_gumroad_link
    params = {:token => self.page.site.gumroad_token, :name => name, :url => url, :price => price * 100, :description => description, :country_available => country_available, :max_purchase_count => max_purchase_count, :require_shipping => require_shipping}
    params.merge!({:file => File.new(product_file.path, 'rb')}) if !product_file.blank?
    params.merge!({:preview => File.new(preview_file.path, 'rb')}) if !preview_file.blank?
    
    response = RestClient.put("https://api.gumroad.com/v1/products/#{self.gumroad_id}", params)
    j = JSON.load(response)
    
    if !j["success"]
      errors[:base] << j["errors"][0]
    end
  end
end