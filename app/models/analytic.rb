class Analytic < ActiveRecord::Base
  extend Legato::Model

  filter :for_page_path, &lambda {|path| matches(:page_path, path)}
  filter :remove_keyword, &lambda {|keyword| does_not_match(:keyword, keyword)}
  filter :remove_source, &lambda {|source| does_not_match(:source, source)}

  # require 'analytics/analytics_service'
  
  belongs_to :site
  serialize :deep_dive, Array
  serialize :keywords, Array
  serialize :sources, Array
  
  def sorted_deep_dive
    arr = Array.new
    
    self.deep_dive.each do |dd|
      if dd[:month] == "12" and Date.today.month == 1
        arr << dd.merge(:date => Date.parse("#{Date.today.year - 1}-12-#{dd[:day]}"))
      else
        arr << dd.merge(:date => Date.parse("#{Date.today.year}-#{dd[:month]}-#{dd[:day]}"))
      end
    end
    
    return arr.sort_by { |x| x[:date] }
  end
  
  def self.default(time_travel)
    time_travel = time_travel == :month ? :month : :week
    a_week_ago = 1.week.ago
    a_month_ago = 1.month.ago
    
    # setup initial deep dive month and week defaults (dummy data according to day of week)
    if time_travel == :month
      deep_dive = Array.new(30) do |i|
        current_date = a_month_ago + i.day
        { :month => current_date.strftime('%m'), :day => current_date.strftime('%d'), :visits => 0 }
      end
    else
      deep_dive = Array.new(7) do |i|
        current_date = a_week_ago + i.day
        { :month => current_date.strftime('%m'), :day => current_date.strftime('%d'), :visits => 0 }
      end
    end
     
    Analytic.new(
      :total_views => 0,
      :avg_time_on_site => 0,
      :new_visitors => 0,
      :deep_dive => deep_dive,
      :keywords => [],
      :sources => [],
      :time_travel => time_travel,
    )
  end
end