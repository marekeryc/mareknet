class Theme < ActiveRecord::Base
  # attr_accessible :title, :body
  
  has_many :sites
  
  validates :theme_name, :presence => true
  validates :screenshot_image, :presence => true
  
  hex_format = /^[0-9A-F]+$/i
  validates :header_colour, :presence => true, :format => hex_format
  validates :subheader_colour, :presence => true, :format => hex_format
  validates :body_colour, :presence => true, :format => hex_format
  validates :background_colour, :presence => true, :format => hex_format
  validates :content_background_colour, :presence => true, :format => hex_format
  
  validates :content_background_opacity, :presence => true, :numericality => true
  
  validates :header_type, :presence => true
  validates :subheader_type, :presence => true
  validates :body_type, :presence => true
  
  validates :header_size, :presence => true, :numericality => true
  validates :subheader_size, :presence => true, :numericality => true
  validates :body_size, :presence => true, :numericality => true
  
  mount_uploader :background_image, BackgroundImageUploader
  mount_uploader :screenshot_image, BackgroundImageUploader
end