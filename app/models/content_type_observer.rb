class ContentTypeObserver < ActiveRecord::Observer
  observe :button, :contact_section, :container, :embed, :product, :social_feed, :text_area
  
  def after_create(model)
    move_content(model, "up")
  end

  def after_destroy(model)
    move_content(model, "down")
  end

  def move_content(model, direction)
    pages_content = model.page.content
    
    # Finds all content on page that comes after the content being created or destroyed, in the same column
    if model.position < 100
      pages_content = pages_content.select {|x| x.position >= model.position and x.position < 100 and x != model }
    elsif model.position >= 100
      pages_content = pages_content.select {|x| x.position >= model.position and x.position >= 100 and x != model }
    end
    
    Page.transaction do
      pages_content.each_with_index do |c, i|
        c.position = c.position + 1 if direction == "up"
        c.position = c.position - 1 if direction == "down"
        c.update_column(:position, c.position)
        # c.save
      end
    end
  end
end