class TextArea < ActiveRecord::Base
  attr_accessible :position, :page_id, :title, :body
  
  belongs_to :page
  
  validates :body, :presence => true
  #validates :position, :numericality => true
end
