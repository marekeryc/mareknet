class UserSession < Authlogic::Session::Base
  generalize_credentials_error_messages "Oops! Incorrect email and/or password."
end