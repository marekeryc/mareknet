class Promotion < ActiveRecord::Base
  attr_accessible :page_id, :position, :title, :prize, :require_name, :require_email, :require_phone, :require_facebook_like, :facebook_url, :other, :email
  
  belongs_to :page
  
  validates :title, :presence => true
  validates :prize, :presence => true
  validates :email, :presence => true
  validate :validate_facebook_url
  validate :any_entry_requirement_present?

  def any_entry_requirement_present?
    if %w(require_name require_email require_phone).all?{|attr| self[attr].blank?}
      self.errors.add(:base, "Please select one entry requirement")
    end
  end

  def validate_facebook_url
  	if self.require_facebook_like && self.facebook_url.blank?
  		self.errors.add(:base, "Please enter your Facebook page URL")
  	end
  end
end