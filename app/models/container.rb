class Container < ActiveRecord::Base
  CHILD_OBJECT_TO_VIEWS_PATH = {
    "contact_form_fields" => "contact_form_fields",
    "downloads" => "download_sections",
    "gallery_images" => "galleries",
    "hours" => "hours",
    "newsletters" => "newsletters",
    "services" => "services",
    "social_buttons" => "social_buttons"
  }
  
  DISPLAY_FORMAT = [["Slideshow", "slideshow"], ["Thumbnails + Overlay", "thumbnails"], ["Linkable Images", "linkable"]]
  
  attr_accessible :page_id, :position, :title, :description, :format, :services_attributes, :hours_attributes, :social_button_attributes, :contact_form_fields_attributes, :gallery_images_attributes, :downloads_attributes, :child_object
  belongs_to :page
  
  has_many :contact_form_fields, :dependent => :destroy, :order => 'position'
  accepts_nested_attributes_for :contact_form_fields, :allow_destroy => true, :reject_if => :all_blank
  
  has_many :downloads, :dependent => :destroy, :order => 'position'
  accepts_nested_attributes_for :downloads, :allow_destroy => true, :reject_if => :all_blank
  
  has_many :gallery_images, :dependent => :destroy, :order => 'position'
  accepts_nested_attributes_for :gallery_images, :allow_destroy => true, :reject_if => :all_blank
  
  has_many :hours, :dependent => :destroy
  accepts_nested_attributes_for :hours, :allow_destroy => true, :reject_if => :all_blank
  
  has_many :services, :dependent => :destroy, :order => 'position'
  accepts_nested_attributes_for :services, :allow_destroy => true, :reject_if => :all_blank
  
  has_one :social_button, :dependent => :destroy
  accepts_nested_attributes_for :social_button
  
  validate :must_have_at_least_one_child_object, :if => Proc.new { |c| c.child_object == "services" || c.child_object == "hours" || c.child_object == "gallery_images" || c.child_object == "downloads" || c.child_object == "contact_form_fields" }
  
  def must_have_at_least_one_child_object
    undestroyed_child_objects_count = 0
    child_objects = eval(child_object)
    child_objects.each { |c| undestroyed_child_objects_count += 1 unless c.marked_for_destruction? }
    errors[:base] << 'Please enter at least one ' + child_object.singularize.gsub("_", " ") if undestroyed_child_objects_count < 1
  end
  
  def default_contact_form_fields
    contact_form_fields << ContactFormField.new(:field_name => "Name", :field_type => "text")
    contact_form_fields << ContactFormField.new(:field_name => "Email", :field_type => "text", :required => true)
    contact_form_fields << ContactFormField.new(:field_name => "Message", :field_type => "textarea", :required => true)
  end

  # Returns an array of hashes of responses
  # [{:created_at=>2013-10-05, 1722=>"Arnie", 1723=>"awelson@work.net", 1724=>"Hi"}, {1722=>"Chris", 1723=>"cj@cj.com", 1724=>"Bye"}]

  def contact_form_responses
    output = Array.new
    cfr_groups = ContactFormResponse.where(:contact_form_field_id => self.contact_form_fields).group(:created_at).order("created_at DESC")
    cfr_groups.each do |cfr_group|
      response_hash = {:created_at => cfr_group.created_at}
      cfrs = ContactFormResponse.where(:contact_form_field_id => self.contact_form_fields).where(:created_at => cfr_group.created_at)
      response_hash.merge! Hash[ cfrs.map { |c| [c.contact_form_field_id, c.value] }]
      output << response_hash
    end
    return output
  end
end