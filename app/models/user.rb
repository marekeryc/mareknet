class User < ActiveRecord::Base
  PLANS = {
    "Yearly" => {
      "Starter" => 8.00,
      "Plus" => 15.00,
      "Premium" => 29.00,
      "Double Premium" => 58.00,
      "Agency" => 199.00
    },
    "Monthly" => {
      "Starter" => 10.00,
      "Plus" => 20.00,
      "Premium" => 39.00,
      "Double Premium" => 78.00,
      "Agency" => 239.00
    },
    "Trial" => {
      "Trial" => 0
    }
  }
  PROMO_CODES = {"phillyburbs" => {:user => 10, :affiliate => 20}, "greatscott" => {:user => 10, :affiliate => 0}, "nonprofit" => {:user => 30, :affiliate => 0}, "4040club" => {:user => 40, :affiliate => 0}, "domainr" => {:user => 10, :affiliate => 25}, "first10" => {:user => 10, :affiliate => 0}}
  PLANS_SITES_TOTALS = { "Trial" => 1, "Starter" => 1, "Plus" => 5, "Premium" => 20, "Double Premium" => 40, "Agency" => 150 }
  PLANS_USERS_TOTALS = { "Trial" => 1, "Starter" => 1, "Plus" => 1, "Premium" => 21, "Double Premium" => 41, "Agency" => 151 }
  
  attr_accessor :terms_and_conditions, :old_password, :reset_password, :site_ids

  self.per_page = 100 # NEEDED FOR ADMIN SECTION
  
  has_many :payments, :inverse_of => :user
  accepts_nested_attributes_for :payments
  has_many :sites, :inverse_of => :user
  accepts_nested_attributes_for :sites
  has_many :pages, :through => :sites
  has_many :containers, :through => :pages
  has_many :site_permissions
  has_many :site_permission_sites, :through => :site_permissions, :source => :site
  has_many :transactions
  
  acts_as_authentic do |c|
    c.require_password_confirmation = false
    c.crypto_provider = Authlogic::CryptoProviders::Sha512
  end
  
  with_options :if => :paying_user? do |paying_user|
    paying_user.validates :first_name, :presence => true
    paying_user.validates :last_name, :presence => true
    paying_user.validates :address, :presence => true
    # paying_user.validates :city, :presence => true
    # paying_user.validates :state, :presence => true
    paying_user.validates :zip, :presence => true
    # paying_user.validates :country, :presence => true
  end
  
  validates :payment_frequency, :inclusion => { :in => ["Trial", "Monthly", "Yearly", "Child"] }
  validates :payment_plan, :inclusion => { :in => ["Starter", "Plus", "Premium", "Double Premium", "Agency", "Trial", "Child"] }
  validates :terms_and_conditions, :acceptance => true
  validate :validate_old_password, :on => :update, :unless => [:reset_password], :if => Proc.new { |u| !u.password.blank? || (u.password.blank? && !u.old_password.blank?) }
  validates :site_ids, :presence => true, :if => Proc.new { |u| u.payment_plan == "Child" && !reset_password }

  after_create :send_email
  
  def send_email
    if self.paying_user?
      PostOffice.new_paid_user(self).deliver
      PostOffice.new_user_notification(self).deliver
    elsif self.payment_plan != "Child"
      PostOffice.new_trial_user(self).deliver
      PostOffice.new_user_notification(self).deliver
    end
  end
  
  def paying_user?
    if self.payment_frequency == "Trial" || self.payment_plan == "Child"
      false
    else
      true
    end
  end
  
  def is_cancelled?
    if cancellation_date.blank?
      return false
    elsif cancellation_date >= Date.today
      return false
    elsif cancellation_date < Date.today
      return true
    end
  end
  
  def is_affiliate?
    affiliate = Affiliate.find_by_user_id(self.id)
    if !affiliate.blank?
      return true
    else
      return false
    end
  end
  
  def self.get_city_state(zip)
    u = User.find_by_sql(["SELECT * from zips where id = ? LIMIT 1", zip.to_i])
    if u.length != 0
      return {:city => u[0].city, :state => u[0].state}
    end
  end
  
  def yearly_total(calculate = :with_promo_code)
    yearly_total = 0
    yearly_total += 15.0 if sites.last.domain_type == "new_custom_domain"
    yearly_total += PLANS["Yearly"][payment_plan] * 12.0 if payment_frequency == "Yearly"
    yearly_total = apply_promo_code(yearly_total, promo_code) if !promo_code.blank? && calculate == :with_promo_code
    return yearly_total
  end
  
  def domain_registration_total(calculate = :with_promo_code)
    yearly_total = 0
    yearly_total += 15.0 if sites.last.domain_type == "new_custom_domain"
    yearly_total = apply_promo_code(yearly_total, promo_code) if !promo_code.blank? && calculate == :with_promo_code
    return yearly_total
  end
  
  def plan_only_yearly_total(calculate = :with_promo_code)
    yearly_total = 0
    yearly_total += PLANS["Yearly"][payment_plan] * 12.0 if payment_frequency == "Yearly"
    yearly_total = apply_promo_code(yearly_total, promo_code) if !promo_code.blank? && calculate == :with_promo_code
    return yearly_total
  end
  
  def monthly_total(calculate = :with_promo_code)
    monthly_total = 0
    monthly_total = PLANS["Monthly"][payment_plan] if payment_frequency == "Monthly"
    monthly_total = apply_promo_code(monthly_total, promo_code) if !promo_code.blank? && calculate == :with_promo_code
    return monthly_total
  end

  def recurly_plan_code
    payment_plan + "-" + payment_frequency
  end
  
  def apply_promo_code(value, code)
    discount = User::PROMO_CODES[code]
    if !discount.blank?
      value = value.to_f - (value.to_f * (discount[:user].to_f / 100.0))
    end
    return value
  end
  
  def validate_old_password
    # If you are trying to update your password from manage account, you have to enter in your old password correctly
    if !self.valid_password?(self.old_password)
      errors[:base] << "Incorrect old password, password was not changed."
    end
  end
  
  def sites_remaining
    if payment_plan == "Child" and self.admin == false
      return 0
    elsif payment_plan == "Child" and self.admin == true
      u = SitePermission.find_by_user_id(self.id).parent_user
      return (PLANS_SITES_TOTALS[u.payment_plan] - u.sites.length).to_i
    else
      return (PLANS_SITES_TOTALS[self.payment_plan] - self.sites.length).to_i
    end
  end
  
  def sites_total
    if payment_plan == "Child" and self.admin == true
      u = SitePermission.find_by_user_id(self.id).parent_user
      return PLANS_SITES_TOTALS[u.payment_plan].to_i
    else
      return PLANS_SITES_TOTALS[self.payment_plan].to_i
    end
  end

  def sites
    if self.payment_plan == "Child"
      sp = SitePermission.joins(:site).where(:user_id => self.id).where("sites.active = 1")
      site_ids = sp.collect {|sp| sp.site.id}
      Site.where(:id => site_ids)
    else
      super
    end
  end

  def pages
    if self.payment_plan == "Child"
      sp = SitePermission.joins(:site).where(:user_id => self.id).where("sites.active = 1")
      pages = Array.new
      sp.collect {|sp| pages.concat sp.site.pages}
      return pages
    else
      super
    end
  end

  def white_label_title
    if self.payment_plan == "Child"
      sp = SitePermission.find_by_user_id(self.id)
      if sp.parent_user.white_label_active
        sp.parent_user.white_label_title.blank? ? "Easy Site Admin" : sp.parent_user.white_label_title
      else
        return "Onepager"
      end
    else
      read_attribute(:white_label_title).blank? ? "Easy Site Admin" : super
    end
  end
  
  def free_domain_qualified?
    # This commented out turns off the free domain promotion for everybody in the backend
    # if self.payment_frequency == "Monthly"
    #   return false
    # end
    # self.sites.each do |s|
    #   if s.domain_type == "new_custom_domain"
    #     return false
    #   end
    # end
    # if !self.upgrade_date.nil? and self.upgrade_date >= "2013-12-13"
    #   return true
    # end
    # if self.created_at >= "2013-12-13"
    #   return true
    # end
    return false
  end
end
