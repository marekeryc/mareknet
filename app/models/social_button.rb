class SocialButton < ActiveRecord::Base
  attr_accessible :like_button, :tweet_button, :google_plus_button, :linked_in_button, :social_links_attributes
  
  belongs_to :container
  has_many :social_links, :dependent => :destroy, :order => 'position'
  accepts_nested_attributes_for :social_links, :allow_destroy => true, :reject_if => proc { |attributes| attributes['url'].blank? }
  validate :all_fields_not_blank
  
  def all_fields_not_blank
    if like_button == false and tweet_button == false and google_plus_button == false and social_links.length == 0
      errors[:base] << "You've left everything blank! Please add a social link or button."
    end
  end
end