class ContactSection < ActiveRecord::Base
  attr_accessible :page_id, :title, :description, :email, :phone, :fax, :address, :address2, :city, :state, :zip, :map_enabled, :position
  
  belongs_to :page
  
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, :if => Proc.new { |c| !c.email.blank? }
  validate :all_fields_not_blank
  
  def all_fields_not_blank
    if email.blank? and phone.blank? and fax.blank? and address.blank? and address2.blank? and city.blank? and state.blank? and zip.blank?
      errors[:base] << "You've left all fields blank! Please fill out something in the contact section."
    end
  end
  
  # Turns 1560 Broadway, 10th Floor, New York, NY 10036 into 1560 Broadway<br />10th Floor<br />New York, NY 10013
  def htmlized_address
    addy = ""
    addy += '<span itemprop="streetAddress">' + ActionView::Base.full_sanitizer.sanitize(address) + '</span><br />' if !address.blank?
    addy += ActionView::Base.full_sanitizer.sanitize(address2) + '<br />' if !address2.blank?
    addy += '<span itemprop="addressLocality">' + ActionView::Base.full_sanitizer.sanitize(city) + '</span>' if !city.blank?
    addy += ', ' if !city.blank? && !state.blank?
    addy += '<span itemprop="addressRegion">' + ActionView::Base.full_sanitizer.sanitize(state) + '</span> ' if !state.blank?
    addy += ' ' if ( !city.blank? || !state.blank? ) && !zip.blank?
    addy += '<span itemprop="postalCode">' + ActionView::Base.full_sanitizer.sanitize(zip) + '</span>' if !zip.blank?
    return addy
  end
  
  def full_address
    full_address = ''
    full_address += address + ' ' if !address.blank?
    full_address += address2 + ' ' if !address2.blank?
    full_address += formatted_city_state_zip if !formatted_city_state_zip.blank?
    return full_address
  end

  def formatted_city_state_zip
    fcsz = ""
    fcsz += city if !city.blank?
    fcsz += ', ' if !city.blank? && !state.blank?
    fcsz += state if !state.blank?
    fcsz += ' ' if ( !city.blank? || !state.blank? ) && !zip.blank?
    fcsz += zip if !zip.blank?
    return fcsz
  end

  def map_width
    if self.page.layout_width == '835' and self.position < 100
      return '450'
    elsif self.page.layout_width == '835' and self.position >= 100
      return '290'
    else
      return self.page.layout_width
    end
  end
end