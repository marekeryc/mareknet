class Site < ActiveRecord::Base
  require 'opensrs'
  include OpenSRS
  
  attr_accessible :company_name, :tagline, :logo_image, :remove_logo_image, :background_colour, :background_image, :remote_background_image_url, :background_image_alignment, :background_image_fixed, :background_image_enabled, :content_background_colour, :content_background_opacity, :header_type, :header_colour, :header_size, :subheader_type, :subheader_colour, :subheader_size, :body_type, :body_colour, :body_size, :custom_css, :analytics_property_id, :pages_attributes, :theme_id, :domain_name, :onepager_address, :trial_address, :domain_type, :favicon_image, :password_protected, :custom_footer, :purchased_domain, :deactivate_responsive_styles
  attr_accessor :trial_address
  
  default_scope where(:active => true)
  
  ALIGNMENTS = [
    ['Center Image', 'center'],
    ['Stretch Image', 'cover'],
    ['Tile Image', 'auto']
  ]
  
  FONTS = {
    'safe' => [
      ['Arial', 'Arial, Helvetica, sans-serif', 'normal'],
      ['Arial Bold', 'Arial, Helvetica, sans-serif', 'bold'],
      ['Courier', '"Courier New", Courier, "Lucida Console", monospace', 'normal'],
      ['Georgia', 'Georgia, "Times New Roman", times, serif', 'normal'],
      ['Georgia Bold', 'Georgia, "Times New Roman", times, serif', 'bold'],
      ['Lucida', '"Lucida Sans Unicode", "Lucida Grande", sans-serif', 'normal'],
      ['Lucida Bold', '"Lucida Sans Unicode", "Lucida Grande", sans-serif', 'bold'],
      ['Palatino', '"Palatino Linotype", "Book Antiqua", Palatino, serif', 'normal'],
      ['Palatino Bold', '"Palatino Linotype", "Book Antiqua", Palatino, serif', 'bold'],
      ['Tahoma', 'Tahoma, Verdana, Geneva, sans-serif', 'normal'],
      ['Tahoma Bold', 'Tahoma, Verdana, Geneva, sans-serif', 'bold'],
      ['Times', 'times, "Times New Roman", serif', 'normal'],
      ['Times Bold', 'times, "Times New Roman", serif', 'bold']
    ],
    'basic' => [
      ['Cabin', 'Cabin, sans-serif', '400'],
      ['Cabin Bold', 'Cabin, sans-serif', '700'],
      ['Josefin Sans', '"Josefin Sans", sans-serif', '400'],
      ['Josefin Sans Bold', '"Josefin Sans", sans-serif', '700'],
      ['Oswald', 'Oswald, sans-serif', '400'],
      ['Oswald Bold', 'Oswald, sans-serif', '700'],
      ['Varela', 'Varela, sans-serif', 'normal'],
      ['Josefin Slab', '"Josefin Slab", serif', '400'],
      ['Josefin Slab Bold', '"Josefin Slab", serif', '700'],
      ['Open Sans', '"Open Sans", sans-serif', '400'],
      ['Open Sans Bold', '"Open Sans", sans-serif', '700'],
      ['Ovo', '"Ovo", serif', 'normal'],
      ['PT Serif', '"PT Serif", serif', '400'],
      ['PT Serif Bold', '"PT Serif", serif', '700'],
      ['Wire One', '"Wire One", sans-serif', 'normal'],
      ['Della Respira', '"Della Respira", serif', '400']
    ],
    'fancy' => [
      ['Kameron', 'Kameron, cursive', '400'],
      ['Kameron Bold', 'Kameron, cursive', '700'],
      ['Rokkitt', 'Rokkitt, serif', '400'],
      ['Rokkitt Bold', 'Rokkitt, serif', '700'],
      ['Maiden Orange', '"Maiden Orange", cursive', 'normal'],
      ['Bevan', 'Bevan, serif', 'normal'],
      ['Dancing Script', '"Dancing Script", cursive', 'normal'],
      ['Tangerine', 'Tangerine, cursive', 'normal'],
      ['Pacifico', 'Pacifico, cursive', 'normal'],
      ['Damion', 'Damion, cursive', 'normal'],
      ['Permanent Marker', '"Permanent Marker", cursive', 'normal'],
      ['Fugaz One', '"Fugaz One", cursive', 'normal'],
      ['Amarante', 'Amarante, cursive', '400' ]
    ]
  }
  
  RESERVED_ADDRESSES = %w(about-us press faq contact-us showcase privacy-policy terms-conditions signup upgrade thankyou thankyou-trial signin reset-password settings themes start users stylesheets subscribers check_domain_availability sitemap images dashboard newsletters affiliates features sites contact_form_responses custom content_types en admin sitemaps robots)
  
  self.per_page = 20

  has_many :pages
  accepts_nested_attributes_for :pages
  has_many :analytics
  has_many :containers, :through => :pages
  has_many :subscribers, :conditions => { :active => true }
  belongs_to :user, :inverse_of => :sites
  belongs_to :theme
  
  mount_uploader :background_image, BackgroundImageUploader
  mount_uploader :logo_image, LogoImageUploader
  mount_uploader :favicon_image, FaviconImageUploader
  
  before_save :create_analytics, :if => Proc.new { |s| s.user.try(:payment_frequency) == "Monthly" || s.user.try(:payment_frequency) == "Yearly" }
  before_save :escape_company_name_and_tagline

  with_options :if => Proc.new { |s| !s.user.blank? } do |site_with_user|
    site_with_user.validates :onepager_address, :presence => true, :uniqueness => true, :exclusion => { :in => RESERVED_ADDRESSES, :message => '"%{value}" is reserved. Please pick another Onepager address.' }, :format => /^[\w|-]+$/i
    site_with_user.validates :domain_type, :inclusion => { :in => ["no_custom_domain", "new_custom_domain", "existing_custom_domain"] }, :presence => true
    site_with_user.validates :domain_name, :presence => true, :if => Proc.new { |site| site.domain_type == "new_custom_domain" || site.domain_type == "existing_custom_domain" }
  end
  
  validate :domain_name_uniqueness, :if => Proc.new { |site| site.domain_type != "no_custom_domain" && site.domain_name_changed? } # If domain type is new or existing and domain name is being changed
  validate :check_domain_availability, :if => Proc.new { |site| site.domain_type == "new_custom_domain" && site.purchased_domain_was.blank? }

  def escape_company_name_and_tagline
    self.company_name = company_name.gsub(/[\"<>]/, "") if !company_name.blank?
    self.tagline = tagline.gsub(/[\"<>]/, "") if !tagline.blank?
  end

  def domain_name_uniqueness
    # Only check if a non cancelled user has the same domain name
    if User.count_by_sql(["SELECT COUNT(u.id) FROM users u INNER JOIN sites s ON u.id = s.user_id WHERE s.id != ? AND s.domain_name = ? AND (u.cancellation_date >= ? OR u.cancellation_date IS NULL)", self.id, self.domain_name, Date.today]) > 0
    # if User.where("domain_name = ? AND (cancellation_date >= ? OR cancellation_date IS NULL)", self.domain_name, Date.today).count > 0
      errors.add(:domain_name, "is not available.")
    end
  end
  
  def check_domain_availability
    begin
      if !self.class.domain_available?(self.domain_name)
        errors.add(:domain_name, "is not available.")
      end
    rescue
      errors.add(:domain_name, "- there was an error checking domain availability.")
    end
  end
  
  def self.opensrs_server
    OpenSRS::Server.new(
      :server => APP_CONFIG["opensrs_server"],
      :username => APP_CONFIG["opensrs_username"],
      :key => APP_CONFIG["opensrs_private_key"]
    )
  end
  
  def self.domain_available?(domain)
    s = opensrs_server
    response = s.call(
      :action => "lookup",
      :object => "domain",
      :attributes => {
        :domain => domain
      }
    )
    if response.success?
      if response.response["attributes"]["status"] != 'available'
        return false
      else
        return true
      end
    end
  end
  
  def register_domain
    address = {
      :first_name => self.user.first_name,
      :last_name => self.user.last_name,
      :phone => "+1.9175248172",
      :fax => "",
      :email => "hi+#{self.user.id}@onepagerapp.com",
      :org_name => "Organization",
      :address1 => "2028 E Ben White Blvd #240-1788",
      :address2 => "",
      :address3 => "",
      :postal_code => "78741",
      :city => "Austin",
      :state => "TX",
      :country => "US"
    }
    attrs = {
        :domain => self.domain_name,
        :custom_tech_contact => "0",
        :custom_nameservers => "0",
        :reg_username => self.user.email.split("@")[0].gsub(/[^a-zA-Z0-9]/, "")[0..19],
        :reg_password => "sunshine1a",
        :reg_type => "new",
        :handle =>"process",
        :dns_template => "single_a_record_to_6622841175",
        :f_lock_domain => "1",
        :f_whois_privacy => "0",
        :auto_renew => "1",
        :contact_set => {:owner => address, :admin => address, :billing => address},
        :period => "1"
    }
    response = self.class.opensrs_server.call(
      :action => "sw_register",
      :object => "domain",
      :attributes => attrs
    )
    if !response.success?
      PostOffice.send_simple_message("Automatic domain registration failed", "User ID #{self.user.id} #{self.user.email} tried to register the domain #{self.domain_name}. #{response.response}")
    end
  end
  
  def self.default
    s = Site.new
    t = Theme.first
    s.company_name = "Your Company Name"
    s.tagline = "Enter your tagline here"
    s.background_colour = t.background_colour
    s.background_image = t.background_image
    s.background_image_alignment = t.background_image_alignment
    s.background_image_enabled = t.background_image_enabled
    s.background_image_fixed = t.background_image_fixed
    s.content_background_colour = t.content_background_colour
    s.content_background_opacity = t.content_background_opacity
    s.header_type = t.header_type
    s.header_colour = t.header_colour
    s.header_size = t.header_size
    s.subheader_type = t.subheader_type
    s.subheader_colour = t.subheader_colour
    s.subheader_size = t.subheader_size
    s.body_type = t.body_type
    s.body_colour = t.body_colour
    s.body_size = t.body_size
    p = Page.new
    p.layout_alignment = "none"
    p.layout_width = "450"
    s.pages << p
    t = TextArea.new
    t.position = 0
    t.body = "<p>Enter content that tells your visitors about your company and what you do. You can write about your history, your team, the areas you service, or anything else you'd like to share.</p>"
    p.text_areas << t
    return s
  end
  
  def create_analytics
    if analytics.length == 0
      analytics << Analytic.default(:week) << Analytic.default(:month)
    end
  end
  
  def trial_address
    rand_addr = (0...2).map{97.+(rand(25)).chr}.join + rand(9).to_s + rand(9).to_s + rand(9).to_s + rand(9).to_s
    c = Site.count('id', :conditions => ["onepager_address = ?", rand_addr])
    if c == 0
      return rand_addr
    else
      trial_address
    end
  end
  
  def url_host
    if custom_domain?
      "http://" + domain_name
    else
      "http://onepagerapp.com"
    end
  end
  
  def url_fullpath
    if custom_domain?
      "http://" + domain_name
    else
      "http://onepagerapp.com/" + onepager_address
    end
  end
  
  def custom_domain?
    if (domain_type == "new_custom_domain" && Time.now - self.created_at > 86400 ) || (domain_type == "existing_custom_domain" && dns_confirmed == true)
      return true
    else
      return false
    end
  end
end
