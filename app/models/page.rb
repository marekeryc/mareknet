class Page < ActiveRecord::Base
  belongs_to :site
  has_many :buttons
  has_many :contact_sections
  has_many :containers
  has_many :embeds
  has_many :iframes
  has_many :script_embeds
  has_many :products
  has_many :social_feeds
  has_many :text_areas
  has_many :paypal_buttons
  has_many :promotions
  
  def content
    c = self.text_areas + self.containers + self.contact_sections + self.buttons + self.embeds + self.products + self.social_feeds + self.iframes + self.paypal_buttons + self.promotions
    c += self.script_embeds if (self.site.user && self.site.user.allow_script_embeds?)
    c.sort_by! {|obj| obj.position}
    return c
  end
  
  def title_tag
    if seo_title.blank?
      title_tag = self.site.company_name
      if contact_sections.size > 0
        title_tag += " in " + contact_sections[0].formatted_city_state_zip if !contact_sections[0].formatted_city_state_zip.blank?
      end
      title_tag += " - " + self.site.tagline if !self.site.tagline.blank?
      return title_tag
    else
      return seo_title
    end
  end
  
  def meta_description
    if seo_description.blank?
      if text_areas.size > 0
        return HTMLEntities.new.decode text_areas[0].body.gsub("<p>", "").gsub("\n", "").strip
      end
    else
      return seo_description
    end
  end
  
  def meta_keywords
    if seo_keywords.blank?
      if containers.where(:child_object => "services").size > 0
        containers.where(:child_object => "services").first.services.collect{ |s| s.body }.join(", ")
      end
    else
      return seo_keywords
    end
  end
end
