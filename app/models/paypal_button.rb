class PaypalButton < ActiveRecord::Base
  attr_accessible :page_id, :position, :name, :paypal_email, :price, :description, :preview_file, :remove_preview_file, :currency, :customize_qty, :shipping, :tax_rate, :customize_price

  CURRENCIES = [["U.S. Dollar", "USD"], ["Australian Dollar", "AUD"], ["Brazilian Real", "BRL"], ["Canadian Dollar", "CAD"], ["Czech Koruna", "CZK"], ["Danish Krone", "DKK"], ["Euro", "EUR"], ["Hong Kong Dollar", "HKD"], ["Hungarian Forint", "HUF"], ["Israeli New Sheqel", "ILS"], ["Japanese Yen", "JPY"], ["Malaysian Ringgit", "MYR"], ["Mexican Peso", "MXN"], ["Norwegian Krone", "NOK"], ["New Zealand Dollar", "NZD"], ["Philippine Peso", "PHP"], ["Polish Zloty", "PLN"], ["Pound Sterling", "GBP"], ["Singapore Dollar", "SGD"], ["Swedish Krona", "SEK"], ["Swiss Franc", "CHF"], ["Taiwan New Dollar", "TWD"], ["Thai Baht", "THB"], ["Turkish Lira", "TRY"], ["U.S. Dollar", "USD"]]

  belongs_to :page

  validates :name, :presence => true
  validates :price, :numericality => true, :if => "!customize_price"
  validates :currency, :presence => true
  validates :paypal_email, :format => { :with => Authlogic::Regex.email }

  mount_uploader :preview_file, PreviewFileUploader
end