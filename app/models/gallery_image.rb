require 'file_size_validator'
class GalleryImage < ActiveRecord::Base
  attr_accessible :position, :caption, :filename, :url
  
  belongs_to :container
  
  mount_uploader :filename, GalleryImageUploader
  
  validates :filename, :presence => true, :file_size => { :maximum => 5.megabytes.to_i }
  
  def full_url
    if !url.include? "http://" and !url.include? "https://" and !url.include? "mailto:"
      return "http://" + url
    else
      return url
    end
  end
end
