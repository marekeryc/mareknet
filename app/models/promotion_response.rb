class PromotionResponse < ActiveRecord::Base
  attr_accessible :page_id, :promotion_id, :name, :email, :phone

  belongs_to :page
  belongs_to :promotion

  validate :any_present?

  after_create :send_email

  def any_present?
    if %w(name email phone).all?{|attr| self[attr].blank?}
      self.errors.add(:base, "Please fill out all required fields.")
    end
  end

  def send_email
    PostOffice.new_promotion_response(self).deliver
  end
end