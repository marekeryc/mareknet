class ScriptEmbed < ActiveRecord::Base
  attr_accessible :page_id, :position, :embed_code

  belongs_to :page

  validates :embed_code, :presence => true
  validate :embed_code_whitelist

  def embed_code_whitelist
    whitelist_services = ['paypal.com', 'wepay.com', 'livechatinc.com', 'dwolla', 'shoplocket', 'facebook', 'clickdesk', 'meetup', 'simpli.fi', 'cookieassistant', 'ccnow', 'gmodules', 'aweber', 'simpli.fi', 'flickr', 'widgetbox.com', 'trumba.com', 'goodreads', 'mailchimp', 'list-manage', 'twitter', 'trackalyzer.com', 'squareup.com', 'sq-api.com', 'angieslist.com', 'demandforce', 'google', 'zopim.com', 'houzz.com', 'homeadvisor.com', 'sketchup.com', 'amazon.com', 'ecwid.com', 'onepagerapp.com', 'sumome.com', 'etsy.com', 'visualwebsiteoptimizer.com', 'wufoo.com', 'adwerx.com', 'singleplatform', 'icontact', 'sendowl', 'olark', 'flnaffiliate.com', 'jotform', 'docmein', 'snapretail', 'gumroad', 'exactdrive.com', 'yelp.com', 'authorize.net', 'skypeassets.com', 'vagaro.com', 'optimizely.com', 'zozi.com', 'feedgrabbr.com', 'statcounter.com', 'tawk.to', 'pinterest.com', 'adsrvr.org', 'adnxs.com', 'wistia.com', 'snipcart.com']
    if !whitelist_services.any?{ |w| embed_code.include?(w) }
      self.errors["base"] << "This embed code is not allowed."
    end
  end
end
