class SocialLink < ActiveRecord::Base
  SERVICES = ["Facebook", "Twitter", "LinkedIn", "Blog", "tumblr", "YouTube", "Vimeo", "Pinterest", "Yelp", "Foursquare", "Google+", "Instagram", "Vine", "Medium", "Periscope"]
  attr_accessible :position, :service, :url
  
  belongs_to :social_button
  
  validates :url, :presence => true
  validates :position, :numericality => true
  
  def full_url
    if !url.include? "http://" and !url.include? "https://"
      return "http://" + url
    else
      return url
    end
  end
end