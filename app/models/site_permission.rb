class SitePermission < ActiveRecord::Base
  belongs_to :user
  belongs_to :parent_user, :class_name => "User", :foreign_key => :parent_user_id
  belongs_to :site
end