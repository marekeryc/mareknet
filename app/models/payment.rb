class Payment < ActiveRecord::Base
  EXP_MONTHS = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
  EXP_YEARS = ["2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025"]
  attr_accessor :credit_card_number, :expiration_month, :expiration_year, :security_code, :accept_language, :is_upgrading
  
  belongs_to :user, :inverse_of => :payments
  validates_presence_of :user
  
  validates :credit_card_number, :presence => true
  validate :validate_cc
  validate :validate_expiration
  validates :security_code, :format => /^(\d{3}|\d{4})$/
  validate :update_recurring, :on => :update, :if => Proc.new {|p| p.errors.empty? and !p.is_upgrading}
  validate :upgrade, :if => Proc.new {|p| p.errors.empty? and p.is_upgrading}
  
  before_create :set_last_four, :set_credit_card_type
  
  def recurring(plan_code, ip_address, accept_language = "", starts_at = "")
    response = {:uuid => "", :errors => ""}

    begin
      s = Recurly::Subscription.create(
        :plan_code => plan_code,
        :coupon_code => user.promo_code,
        :starts_at => starts_at,
        :account => {
          :account_code => user.recurly_account_uuid,
          :email => user.email,
          :first_name => user.first_name,
          :last_name => user.last_name,
          :accept_language => accept_language,
          :billing_info => billing_info(ip_address)
        }
      )
    rescue Recurly::Transaction::DeclinedError => e
      response[:errors] = e.message
    end

    if s
      response[:errors] = s.errors.merge(s.account.errors).merge(s.account.billing_info.errors)
      response[:uuid] = s.uuid
    end

    return response
  end

  def upgrade
    if user.recurly_subscription_uuid.blank?
      user.recurly_account_uuid = SecureRandom.uuid
      response = recurring(user.recurly_plan_code, user.current_login_ip, self.accept_language)
      
      if !response[:errors].blank?
        errors.add(:base, response[:errors])
      else
        if [111, 99.9, 88.8, 66.6, 25, 22.50, 20, 15].include? amount
          recurring("domain-registration", user.current_login_ip, self.accept_language, self.created_at + 1.year)
        end
        self.user.recurly_subscription_uuid = response[:uuid]
        self.user.upgrade_date = Time.now
        self.user.save
        self.user.send_email
      end
    else
      s = Recurly::Subscription.find(user.recurly_subscription_uuid)
      if s.canceled_at # if user has a previously cancelled recurly subscription, reactivate it first
        s.reactivate
      end
      s.update_attributes(:plan_code => user.recurly_plan_code, :timeframe => "now")
      if !s.errors.blank?
        errors.add(:base, s.errors)
      else
        self.user.upgrade_date = Time.now
        self.user.save
        self.user.send_email
      end
    end
  end

  def update_recurring
    if user.recurly_account_uuid.blank?
      user.recurly_account_uuid = SecureRandom.uuid
      response = recurring(user.recurly_plan_code, user.current_login_ip, self.accept_language, self.next_bill_date)

      if !response[:errors].blank?
        errors.add(:base, response[:errors])
      else
        user.recurly_subscription_uuid = response[:uuid]
        user.save
      end
      if response[:errors].blank? and [111, 99.9, 88.8, 66.6, 25, 22.50, 20, 15].include? amount
        recurring("domain-registration", user.current_login_ip, self.accept_language, self.created_at + 1.year)
      end
    else
      response = {:uuid => "", :errors => ""}
      account = Recurly::Account.find(user.recurly_account_uuid)
      begin
        account.billing_info.update_attributes(billing_info(user.current_login_ip))
      rescue
      end
      # for some reason Recurly::Transaction::DeclinedError errors appear below
      if !account.billing_info.valid?
        errors.add(:base, account.billing_info.errors.map {|field, errors| field.gsub("base", "") + " " + errors.join(", ")}.join(", "))
      else
        account.update_attributes(
          :email => user.email,
          :first_name => user.first_name,
          :last_name => user.last_name
        )
      end
    end
  end

  def billing_info(ip_address)
    {
      :first_name => user.first_name,
      :last_name => user.last_name,
      :address1 => user.address,
      :address2 => user.address2,
      :city => user.city,
      :state => user.state,
      :zip => user.zip,
      :country => user.country,
      :number => credit_card_number,
      :month => expiration_month.to_i,
      :year => expiration_year.to_i,
      :verification_value => security_code,
      :ip_address => ip_address
    }
  end

  def next_bill_date
    signed_up = nil
    if !self.user.upgrade_date.blank?
      signed_up = self.user.upgrade_date
    else
      signed_up = self.created_at
    end
    if self.user.payment_frequency == "Monthly"
      begin
        signed_up += 1.month
      end while signed_up <= Date.today
    elsif user.payment_frequency == "Yearly"
      begin
        signed_up += 1.year
      end while signed_up <= Date.today
    end
    return signed_up
  end
  
private
  def validate_cc
    odd = true
    remainder = credit_card_number.to_s.gsub(/\D/,'').reverse.split('').map(&:to_i).collect { |d|
      d *= 2 if odd = !odd
      d > 9 ? d - 9 : d
    }.sum % 10
    errors.add(:credit_card_number, 'is invalid.') if remainder != 0
  end
  
  def validate_expiration
    expiration = Date.parse(expiration_year.to_s + "-" + expiration_month.to_s + "-1")
    expiration = expiration >> 1
    if expiration < Date.today
      errors.add(:credit_card_number, 'is expired')
    end
  end
  
  def set_credit_card_type
    case credit_card_number.to_s[0]
      when "3"
        self.credit_card_type = "American Express"
      when "4"
        self.credit_card_type = "Visa"
      when "5"
        self.credit_card_type = "MasterCard"
      when "6"
        self.credit_card_type = "Discover"
    end
  end
  
  def set_last_four
    self.last_four = credit_card_number[-4..-1]
  end
end
