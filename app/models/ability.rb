class Ability
  include CanCan::Ability

  def initialize(user, temp_page_id = nil)
    user ||= User.new
    
    if UserSession.find # if user logged in
      can :manage, Site do |site|
        if user.payment_plan == "Child"
          SitePermission.where(:user_id => user.id, :site_id => site.id).count > 0
        else
          site.user_id == user.id
        end
      end
      can :manage, Page do |page|
        if user.payment_plan == "Child"
          SitePermission.where(:user_id => user.id, :site_id => page.site.id).count > 0
        else
          page.site.user_id == user.id
        end
      end
      can :manage, Button, :page => user.pages
      can :manage, ContactSection, :page => user.pages
      can :manage, Container, :page => user.pages
      can :manage, Embed, :page => user.pages
      can :manage, TextArea, :page => user.pages
      can :manage, Product, :page => user.pages
      can :manage, SocialFeed, :page => user.pages
      can :manage, Iframe, :page => user.pages
      can :manage, ScriptEmbed, :page => user.pages
      can :manage, PaypalButton, :page => user.pages
      can :manage, Promotion, :page => user.pages
    else
      can :manage, Site do |site|
        site.pages.collect { |p| p.id }.include? temp_page_id
      end
      can :manage, Page, :id => temp_page_id
      can :manage, Button, :page_id => temp_page_id
      can :manage, ContactSection, :page_id => temp_page_id
      can :manage, Container, :page_id => temp_page_id
      can :manage, Embed, :page_id => temp_page_id
      can :manage, TextArea, :page_id => temp_page_id
      can :manage, SocialFeed, :page_id => temp_page_id
      can :manage, Iframe, :page_id => temp_page_id
      can :manage, PaypalButton, :page_id => temp_page_id
      can :manage, Promotion, :page_id => temp_page_id
    end
    
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
