class Embed < ActiveRecord::Base
  attr_accessible :page_id, :title, :description, :url, :position
  
  belongs_to :page
  
  validates :url, :presence => true
  
  validate :url_to_embed
  
  def url_to_embed
    return if url.blank?
    if self.url_changed?
      require 'embedly'
      embedly_api = Embedly::API.new :key => '0293f83372c04a64a924ee543b8705c2'
      obj = embedly_api.oembed :url => self.url, :maxwidth => 430
      
      if obj[0].type == "error"
        errors[:base] << obj[0].error_message
      elsif obj[0].html.blank?
        errors[:base] << "This embed type is not supported, please try another link. (e.g. http://youtube.com/watch?v=oHg5SJYRHA0)"
      else
        self.body = obj[0].html
        self.provider = obj[0].provider_name
        self.category = obj[0].type
      end
    end
  end
end