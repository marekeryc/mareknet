class SocialFeed < ActiveRecord::Base
  FEED_TYPES = ["Twitter", "RSS Feed"]
  
  require 'open-uri'
  require 'nokogiri'

  attr_accessible :page_id, :position, :title, :description, :url, :num_items, :feed_type

  belongs_to :page
  
  validates :url, :presence => true
  validates :num_items, :numericality => true, :inclusion => 1..10
  validate :set_feed_type

  def set_feed_type
    if url.include? "."
      if !url.include? "http://" and !url.include? "https://"
        self.url = "http://" + self.url 
      end
      begin
        body = open(url) {|f| f.read }
        if body.downcase.include? '<rss'
          self.feed_type = "RSS"
        else
          errors[:base] << "There was an error finding your RSS feed URL. Please check your entry and try again."
        end
      rescue
        errors[:base] << "There was an error finding your Twitter handle or RSS feed URL. Please check your entry and try again."
      end
    else
      begin
        tc = twitter_client
        tc.user_timeline(url)
        self.feed_type = "Twitter"
      rescue
        errors[:base] << "There was an error finding your Twitter handle. Please check your entry and try again."
      end
    end
  end
  
  def feed
    if feed_type == "Twitter"
      # For people who add a Twitter feed, and then protects it.
      begin
        return twitter_client.user_timeline(url)[0..num_items-1]
      rescue
        return Array.new
      end
    else
      doc = Nokogiri::XML(open(url))
      return doc.xpath('//item')[0..num_items-1]
    end
  end

  def twitter_client
    Twitter::REST::Client.new do |config|
      config.consumer_key = "UJrxnYm4LVaLm9GFEoYGQ"
      config.consumer_secret = "uNv294h6903IApA9VEiIzCsuoQkT6pH0RwMve6olLs"
      config.access_token = "311004385-2bZWbH0oEC1OLlmuWsEw9MLGL4k0bUgq6IEf6yv8"
      config.access_token_secret = "nXL8pXTFTLqGWeAATmrDm2Vn67M6yeW78PwJO6SA"
    end
  end
end
