class Newsletter < ActiveRecord::Base
  attr_accessible :subject, :message, :from
  
  belongs_to :site
  
  validates :subject, :presence => true
  validates :message, :presence => true
  validates :from, :presence => true, :format => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
  
  after_create :send_newsletter
  
  def send_newsletter
    site.subscribers.each do |s|
      begin
        PostOffice.newsletter(self, s).deliver
      rescue
        s.active = false
        s.save
      end
    end
  end
end
