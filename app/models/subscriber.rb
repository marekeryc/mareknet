class Subscriber < ActiveRecord::Base
  include Obfuscate
  
  attr_accessible :email, :site_id_enc, :crosscheck
  attr_accessor :site_id_enc, :crosscheck
  
  belongs_to :site
  
  validates :email, :format => /^([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})$/i
  # validates :email, :uniqueness => { :scope => :site_id }
  validates :site_id_enc, :presence => true, :on => :create
  validates :crosscheck, :inclusion => { :in => ["negative37892"] }
  validate :validate_many_of_same_email

  before_validation :set_site_id, :on => :create
  after_create :email_confirmation
  
  def email_confirmation
    PostOffice.confirm_subscription(self).deliver
  end
  
  def to_param
    encrypt(id)
  end
  
  def set_site_id
    self.site_id = decrypt(self.site_id_enc) if !self.site_id_enc.blank?
  end

  def validate_many_of_same_email
    if Subscriber.where(:email => self.email).count > 3
      errors.add(:base, "Duplicate email address")
    end
  end
  
  # This is wonderful code, but the validates uniqueness with scope doesn't work with it :-(
  # def container_id
  #   encrypt(self[:container_id].to_s)
  # end
  # 
  # def container_id=(val)
  #   self[:container_id] = decrypt(val).to_i
  # end
end
