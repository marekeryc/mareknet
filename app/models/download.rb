require 'file_size_validator'
class Download < ActiveRecord::Base
  attr_accessible :position, :caption, :filename
  
  belongs_to :container
  
  mount_uploader :filename, DownloadUploader
  
  validates :filename, :presence => true, :file_size => { :maximum => 20.megabytes.to_i }
end
