class Iframe < ActiveRecord::Base
  attr_accessible :page_id, :position, :embed_code

  belongs_to :page

  validates :embed_code, :presence => true
  validate :embed_code_has_iframe
  validate :embed_code_has_src

  def embed_code_has_iframe
    if /<iframe/.match(embed_code) == nil
      self.errors["base"] << "Iframe is invalid. Does not contain an iframe tag."
    end
  end

  def embed_code_has_src
    if /src="http:\/\/([^"]+)"/.match(embed_code) == nil and /src="https:\/\/([^"]+)"/.match(embed_code) == nil
      self.errors["base"] << "Iframe is invalid. Does not contain a src attribute."
    end
  end

  def embed_src
    src = /src="(http[^"]*)"/.match(embed_code)
    src[1].gsub("javascript", "") if src
  end

  def embed_height
    /height="([^"]+)"/.match(embed_code)[1] if /height="([^"]+)"/.match(embed_code) != nil
  end
  
  def embed_scrolling
    if /scrolling="([^"]+)"/.match(embed_code) != nil
      /scrolling="([^"]+)"/.match(embed_code)[1]
    else
      "no"
    end
  end
end
