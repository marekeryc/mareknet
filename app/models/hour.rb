class Hour < ActiveRecord::Base
  attr_accessible :day, :start, :end
  DAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
  ABBR_DAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
  TIMES = ["12:00 AM", "12:30 AM", "1:00 AM", "1:30 AM", "2:00 AM", "2:30 AM", "3:00 AM", "3:30 AM", "4:00 AM", "4:30 AM", "5:00 AM", "5:30 AM", "6:00 AM", "6:30 AM", "7:00 AM", "7:30 AM", "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM", "6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM", "8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM", "10:00 PM", "10:30 PM", "11:00 PM", "11:30 PM"]
  
  belongs_to :container
  
  validates :day, :inclusion => { :in => DAYS }
  validates :start, :inclusion => { :in => TIMES }
  validates :end, :inclusion => { :in => TIMES }
  
  def self.concat_days(days)
      # PRESETS OF SEQUENCE POSSIBILITIES
      options = {
        ['Monday', 'Tuesday'] => 'Mon - Tue',
        ['Monday', 'Tuesday', 'Wednesday'] =>'Mon - Wed',
        ['Monday', 'Tuesday', 'Wednesday', 'Thursday'] => 'Mon - Thu',
        ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'] => 'Mon - Fri',
        ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'] => 'Mon - Sat',
        ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] => 'Mon - Sun',
        ['Tuesday', 'Wednesday'] => 'Tue - Wed',
        ['Tuesday', 'Wednesday', 'Thursday'] => 'Tue - Thu',
        ['Tuesday', 'Wednesday', 'Thursday', 'Friday'] => 'Tue - Fri',
        ['Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'] => 'Tue - Sat',
        ['Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] => 'Tue - Sun',
        ['Wednesday', 'Thursday'] => 'Wed - Thu',
        ['Wednesday', 'Thursday', 'Friday'] => 'Wed - Fri',
        ['Wednesday', 'Thursday', 'Friday', 'Saturday'] => 'Wed - Sat',
        ['Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] => 'Wed - Sun',
        ['Thursday', 'Friday'] => 'Thu - Fri',
        ['Thursday', 'Friday', 'Saturday'] => 'Thu - Sat',
        ['Thursday', 'Friday', 'Saturday', 'Sunday'] => 'Thu - Sun',
        ['Friday', 'Saturday'] => 'Fri - Sat',
        ['Friday', 'Saturday', 'Sunday'] => 'Fri - Sun',
        ['Saturday', 'Sunday'] => 'Sat - Sun'
      }
      
      # FIND THE SEQUENCES AND CONCAT THEM
      days.each_index do |i|
        sequence_days = days.clone
        sequence_days << '' # Hack so first pop doesn't remove last item
        
        begin
          sequence_days.pop
        end until options.has_key?(sequence_days) || sequence_days.empty?
        
        if !sequence_days.empty?
          days << options[sequence_days]
          sequence_days.each { |t| days.delete(t) }
        else
          days << days.shift
        end
      end
      
      # PRETTY UP THE TEXT
      days.sort! { |d1, d2| (Hour::ABBR_DAYS).index(d1[0, 3]) <=> (Hour::ABBR_DAYS).index(d2[0, 3]) }
      days.each_index { |i| days[i] = days[i][0, 3] if !options.has_value?(days[i]) }
    end
  
  def self.view_formatted_hours(container_id)
    hours = Hour.where("container_id = ?", container_id)
    
    # Start by making sure each set of hours are ordered in reverse sequence
    hours.sort! { | h1, h2 | (Hour::DAYS).index(h1.day) <=> (Hour::DAYS).index(h2.day) }
    
    # Create a new hash to store the aggregated hours
    hours_set = Hash.new()
    
    hours.each do |h|
      hours_set[h.start + " - " + h.end] = Array.new if hours_set[h.start + " - " + h.end] == nil
      hours_set[h.start + " - " + h.end] << h.day
    end
    
    hours_set.each { |hour, day| hours_set[hour] = concat_days(day) }
  end

  def self.translate_hours(instring)
    instring = instring.gsub("Sun", I18n.t("date.sun", :default => "Sun"))
    instring = instring.gsub("Mon", I18n.t("date.mon", :default => "Mon"))
    instring = instring.gsub("Tue", I18n.t("date.tue", :default => "Tue"))
    instring = instring.gsub("Wed", I18n.t("date.wed", :default => "Wed"))
    instring = instring.gsub("Thu", I18n.t("date.thu", :default => "Thu"))
    instring = instring.gsub("Fri", I18n.t("date.fri", :default => "Fri"))
    instring = instring.gsub("Sat", I18n.t("date.sat", :default => "Sat"))
  end
end