class Affiliate < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :address, :city, :state, :postal_code, :phone, :paypal_email, :country
  
  validates :first_name, :presence => true
  validates :last_name, :presence => true
  validates :address, :presence => true
  validates :city, :presence => true
  validates :state, :presence => true
  validates :postal_code, :presence => true
  validates :phone, :presence => true
  validates :paypal_email, :presence => true, :format => { :with => /^([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})$/i }, :uniqueness => true

  def transactions
    Transaction.joins(:user).where("affiliate_of = ?", self.user_id).where(:transaction_status => "Settled Successfully")
  end

  def payouts
    Transaction.where(:user_id => self.user_id).where(:transaction_status => "PayPal Affiliate Payment")
  end

  def total
    total = 0.0
    transactions.each { |t| total += t.amount }
    total = total * 0.2
    payouts.each { |t| total -= t.amount }
    return total
  end
end