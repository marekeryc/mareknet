class Transaction < ActiveRecord::Base
  belongs_to :user
  validates :transaction_id, :uniqueness => true
end