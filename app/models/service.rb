class Service < ActiveRecord::Base
  attr_accessible :position, :body
  
  belongs_to :container
  
  validates :body, :presence => true
  validates :position, :numericality => true
end