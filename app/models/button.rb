class Button < ActiveRecord::Base
  attr_accessible :page_id, :position, :title, :url
  
  belongs_to :page
  
  validates :title, :presence => true
  validates :url, :presence => true
  
  def full_url
    if !url.include? ":"
      return "http://" + url
    else
      return url
    end
  end
end