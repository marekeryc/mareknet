class ContactFormField < ActiveRecord::Base
  attr_accessible :position, :field_name, :field_type, :required
  FIELD_TYPES = [["Single line of text", "text"], ["Paragraph of text", "textarea"]]
  
  belongs_to :container
  has_many :contact_form_responses
  
  validates :field_name, :presence => true
  validates :field_type, :presence => true, :inclusion => { :in => FIELD_TYPES.map{|x| x[1]} }
  validates :position, :numericality => true
end