require 'mysql2'
old_db = Mysql2::Client.new(:host => "localhost", :username => "onepagr", :password => "0nir", :database => "onepagr_prod")
new_db = Mysql2::Client.new(:host => "localhost", :username => "onepagr", :password => "0nir", :database => "onepager_dev")

query = "TRUNCATE TABLE users; TRUNCATE TABLE sites; TRUNCATE TABLE pages; TRUNCATE TABLE text_areas; "
new_db.query("TRUNCATE TABLE users;")
new_db.query("TRUNCATE TABLE sites;")
new_db.query("TRUNCATE TABLE pages;")
new_db.query("TRUNCATE TABLE text_areas;")
new_db.query("TRUNCATE TABLE embeds;")
new_db.query("TRUNCATE TABLE containers;")
new_db.query("TRUNCATE TABLE services;")
new_db.query("TRUNCATE TABLE buttons;")
new_db.query("TRUNCATE TABLE contact_form_fields;")
new_db.query("TRUNCATE TABLE gallery_images;")
new_db.query("TRUNCATE TABLE downloads;")
new_db.query("TRUNCATE TABLE hours;")
new_db.query("TRUNCATE TABLE contact_sections;")
new_db.query("TRUNCATE TABLE social_buttons;")
new_db.query("TRUNCATE TABLE social_links;")
new_db.query("TRUNCATE TABLE analytics;")
new_db.query("TRUNCATE TABLE subscribers;")

old_db.query("update links set facebook = NULL where facebook = '';")
old_db.query("update links set twitter = NULL where twitter = '';")
old_db.query("update links set linkedin = NULL where linkedin = '';")
old_db.query("update links set flickr = NULL where flickr = '';")
old_db.query("update links set yelp = NULL where yelp = '';")
old_db.query("update links set foursquare = NULL where foursquare = '';")
old_db.query("update links set tweet_button = NULL where tweet_button = 0;")
old_db.query("update links set like_button = NULL where like_button = 0;")
old_db.query("update links set google_plus = NULL where google_plus = 0;")
old_db.query("update links set blog = NULL where blog = '';")

results = old_db.query("SELECT * FROM users WHERE created_at IS NOT NULL ORDER BY id ASC LIMIT 150")

results.each_with_index(:symbolize_keys => true) { |old_user, i|
  
  old_user.each_pair do |k, v|
    if old_user[k].class == String
      old_user[k] = "'" + old_db.escape(old_user[k]) + "'"
    elsif old_user[k].class == NilClass
      old_user[k] = "NULL"
    elsif old_user[k].class == Time
      old_user[k] = "'" + old_user[k].to_s.gsub(" -0400", "") + "'"
    end
  end
  
  old_user[:crypted_password] = "'4ce6c295b2b0f126433fe2c5ea5a91f8f9eaf87a83aefd3b4efa7d1450e5e28f124b05b08d605c6480a9b9477c7cd85c5a96c0c45c9915b4d7e68788dcc95654'"
  old_user[:password_salt] = "'8eJulWear3BuwORHBoP'"
  
  query = "INSERT INTO users (id, email, first_name, last_name, address, address2, city, state, zip, phone, country, promo_code, payment_frequency, crypted_password, password_salt, persistence_token, single_access_token, perishable_token, login_count, failed_login_count, last_request_at, current_login_at, last_login_at, current_login_ip, last_login_ip, upgrade_date, cancellation_date, temp_cancellation_date, affiliate_of, api_user, created_at, updated_at) VALUES (#{old_user[:id]}, #{old_user[:email]}, #{old_user[:first_name]}, #{old_user[:last_name]}, #{old_user[:address]}, #{old_user[:address2]}, #{old_user[:city]}, #{old_user[:state]}, #{old_user[:zip]}, #{old_user[:phone]}, NULL, #{old_user[:promo_code]}, #{old_user[:payment_frequency]}, #{old_user[:crypted_password]}, #{old_user[:password_salt]}, #{old_user[:persistence_token]}, #{old_user[:single_access_token]}, #{old_user[:perishable_token]}, #{old_user[:login_count]}, #{old_user[:failed_login_count]}, #{old_user[:last_request_at]}, #{old_user[:current_login_at]}, #{old_user[:last_login_at]}, #{old_user[:current_login_ip]}, #{old_user[:last_login_ip]}, #{old_user[:upgrade_date]}, #{old_user[:cancellation_date]}, #{old_user[:temp_cancellation_date]}, #{old_user[:affiliate_of]}, #{old_user[:api_user]}, #{old_user[:created_at]}, #{old_user[:updated_at]});"
  new_db.query(query)
  
  theme = old_db.query("SELECT * FROM themes WHERE user_id = #{old_user[:id]}", :symbolize_keys => true).first
  
  theme[:header_type] = theme[:header_type].split("-").each{|w| w.capitalize!}.join(" ")
  theme[:subheader_type] = theme[:subheader_type].split("-").each{|w| w.capitalize!}.join(" ")
  theme[:body_type] = theme[:body_type].split("-").each{|w| w.capitalize!}.join(" ")
  theme.each_pair do |k, v|
    if theme[k].class == String
      theme[k] = "'" + old_db.escape(theme[k]) + "'"
    elsif theme[k].class == NilClass
      theme[k] = "NULL"
    elsif theme[k].class == Time
      theme[k] = "'" + theme[k].to_s.gsub(" -0400", "") + "'"
    end
  end
  
  about = old_db.query("SELECT * FROM abouts WHERE theme_id = #{theme[:id]}", :symbolize_keys => true).first
  about.each_pair do |k, v|
    if about[k].class == String
      about[k] = "'" + old_db.escape(about[k]) + "'"
    elsif about[k].class == NilClass
      about[k] = "NULL"
    elsif about[k].class == Time
      about[k] = "'" + about[k].to_s.gsub(" -0400", "") + "'"
    end
  end
  
  if theme[:tagline_enabled] == 0
    about[:header] = "NULL"
  end
  
  setting = old_db.query("SELECT * FROM settings WHERE theme_id = #{theme[:id]}", :symbolize_keys => true)
  if setting.size > 0
    setting = setting.first
    setting.each_pair do |k, v|
      if setting[k].class == String
        setting[k] = "'" + old_db.escape(setting[k]) + "'"
      elsif setting[k].class == NilClass
        setting[k] = "NULL"
      end
    end
  else
    setting = {:favicon_file_name => "NULL", :custom_css => "NULL", :analytics_id => "NULL", :seo_title => "NULL", :seo_description => "NULL", :seo_keywords => "NULL"}
  end
  
  if old_user[:payment_frequency] == "'Trial'"
    old_user[:domain_type] = "'no_custom_domain'"
  end
  
  query = "INSERT INTO sites (user_id, domain_name, domain_type, onepager_address, company_name, tagline, theme_id, background_colour, content_background_colour, content_background_opacity, background_image, background_image_alignment, background_image_enabled, logo_image, favicon_image, header_type, header_colour, header_size, subheader_type, subheader_colour, subheader_size, body_type, body_colour, body_size, custom_css, analytics_property_id, created_at, updated_at) VALUES (#{old_user[:id]}, #{old_user[:domain_name]}, #{old_user[:domain_type]}, #{old_user[:onepager_address]}, #{theme[:company_name]}, #{about[:header]}, NULL, #{theme[:background_colour]}, #{theme[:content_background_colour]}, #{theme[:content_background_opacity]}, #{theme[:background_file_name]}, #{theme[:background_alignment]}, #{theme[:background_file_name] == "NULL" ? false : true},  #{theme[:logo_file_name]}, #{setting[:favicon_file_name]}, #{theme[:header_type]}, #{theme[:header_colour]}, #{theme[:header_size]}, #{theme[:subheader_type]}, #{theme[:subheader_colour]}, #{theme[:subheader_size]}, #{theme[:body_type]}, #{theme[:body_colour]}, #{theme[:body_size]}, #{setting[:custom_css]}, #{setting[:analytics_id]}, #{theme[:created_at]}, #{theme[:updated_at]});"
  new_db.query(query)
  
  query = "INSERT INTO pages (site_id, layout_alignment, layout_width, seo_title, seo_description, seo_keywords, created_at, updated_at) VALUES (#{i+1}, #{theme[:layout_align]}, #{theme[:layout_width]}, #{setting[:seo_title]}, #{setting[:seo_description]}, #{setting[:seo_keywords]}, #{theme[:created_at]}, #{theme[:updated_at]});"
  new_db.query(query)
  
  if theme[:about_enabled] == 1
    query = "INSERT INTO text_areas (page_id, position, title, body, created_at, updated_at) VALUES (#{i+1}, 0, NULL, #{about[:body]}, #{about[:created_at]}, #{about[:updated_at]});"
    new_db.query(query)
  end
  
  embeds = old_db.query("SELECT * FROM embeds WHERE theme_id = #{theme[:id]}", :symbolize_keys => true)
  if embeds.size > 0 && theme[:embeds_enabled] == 1
    
    embeds.each_with_index do |embed,eindex|
      embed.each_pair do |k, v|
        if embed[k].class == String
          embed[k] = "'" + old_db.escape(embed[k]) + "'"
        elsif embed[k].class == NilClass
          embed[k] = "NULL"
        elsif embed[k].class == Time
          embed[k] = "'" + embed[k].to_s.gsub(" -0400", "") + "'"
        end
      end
      
      query = "INSERT INTO embeds (page_id, position, title, url, body, category, created_at, updated_at) VALUES (#{i+1}, 2, #{embed[:title]}, #{embed[:url]}, #{embed[:body]}, #{embed[:category]}, #{embed[:created_at]}, #{embed[:updated_at]});"
      new_db.query(query)
    end
    
  end
  
  services = old_db.query("SELECT * FROM services WHERE theme_id = #{theme[:id]} AND BODY IS NOT NULL", :symbolize_keys => true)
  if services.size > 0 && theme[:services_enabled] == 1
    query = "INSERT INTO containers (page_id, position, title, child_object, created_at, updated_at) VALUES (#{i+1}, 3, #{theme[:services_title]}, 'services', #{theme[:created_at]}, #{theme[:updated_at]});"
    new_db.query(query)
    container_id = new_db.last_id
    
    services.each_with_index do |s, sindex|
      s.each_pair do |k, v|
        if s[k].class == String
          s[k] = "'" + old_db.escape(s[k]) + "'"
        elsif s[k].class == NilClass
          s[k] = "NULL"
        elsif s[k].class == Time
          s[k] = "'" + s[k].to_s.gsub(" -0400", "") + "'"
        end
      end
      
      query = "INSERT INTO services (container_id, position, body, created_at, updated_at) VALUES (#{container_id}, #{s[:position]}, #{s[:body]}, #{s[:created_at]}, #{s[:updated_at]});"
      new_db.query(query)
    end
  end
  
  buttons = old_db.query("SELECT * FROM call_to_actions WHERE theme_id = #{theme[:id]}", :symbolize_keys => true)
  if buttons.size > 0 && theme[:call_to_action_enabled] == 1
    buttons = buttons.first
    if !buttons[:title].empty? || !buttons[:url].empty?
      buttons.each_pair do |k, v|
        if buttons[k].class == String
          buttons[k] = "'" + old_db.escape(buttons[k]) + "'"
        elsif buttons[k].class == NilClass
          buttons[k] = "NULL"
        elsif buttons[k].class == Time
          buttons[k] = "'" + buttons[k].to_s.gsub(" -0400", "") + "'"
        end
      end
    
      query = "INSERT INTO buttons (page_id, position, title, url, created_at, updated_at) VALUES (#{i+1}, 5, #{buttons[:title]}, #{buttons[:url]}, #{buttons[:created_at]}, #{buttons[:updated_at]})"
      new_db.query(query)
    end
  end
  
  newsletter_container_id = 0
  if theme[:subscribers_enabled] == 1
    query = "INSERT INTO containers (page_id, position, title, description, child_object, created_at, updated_at) VALUES (#{i+1}, 6, #{theme[:subscribers_title]}, #{theme[:subscribers_description]}, 'newsletters', #{theme[:created_at]}, #{theme[:updated_at]});"
    new_db.query(query)
    newsletter_container_id = new_db.last_id
  end
  
  subs = old_db.query("SELECT * FROM subscribers WHERE theme_id = #{theme[:id]}", :symbolize_keys => true)
  subs.each_with_index do |s, subindex|
    query = "INSERT INTO subscribers (site_id, email, active, created_at, updated_at) VALUES (#{i+1}, '#{s[:email]}', #{s[:active]}, '#{s[:created_at].to_s.gsub(" -0400", "")}', '#{s[:updated_at].to_s.gsub(" -0400", "")}');"
    new_db.query(query)
  end
  
  cffs = old_db.query("SELECT * FROM contact_form_fields WHERE theme_id = #{theme[:id]}", :symbolize_keys => true)
  if cffs.size > 0 && theme[:contact_form_enabled] == 1
    query = "INSERT INTO containers (page_id, position, title, description, child_object, created_at, updated_at) VALUES (#{i+1}, 7, #{theme[:contact_form_title]}, #{theme[:contact_form_description]}, 'contact_form_fields', #{theme[:created_at]}, #{theme[:updated_at]});"
    new_db.query(query)
    container_id = new_db.last_id
    
    cffs.each_with_index do |cff, cindex|
      cff.each_pair do |k, v|
        if cff[k].class == String
          cff[k] = "'" + old_db.escape(cff[k]) + "'"
        elsif cff[k].class == NilClass
          cff[k] = "NULL"
        elsif cff[k].class == Time
          cff[k] = "'" + cff[k].to_s.gsub(" -0400", "") + "'"
        end
      end
      
      query = "INSERT INTO contact_form_fields (id, container_id, position, field_name, field_type, required, created_at, updated_at) VALUES (#{cff[:id]}, #{container_id}, #{cff[:position]}, #{cff[:field_name]}, #{cff[:field_type]}, #{cff[:required]}, #{cff[:created_at]}, #{cff[:updated_at]});"
      new_db.query(query)
    end
  end
  
  images = old_db.query("SELECT * FROM images WHERE theme_id = #{theme[:id]} AND file_file_name IS NOT NULL", :symbolize_keys => true)
  if images.size > 0 && theme[:images_enabled] == 1
    
    query = "INSERT INTO containers (page_id, position, title, child_object, created_at, updated_at) VALUES (#{i+1}, 1, NULL, 'gallery_images', #{theme[:created_at]}, #{theme[:updated_at]});"
    new_db.query(query)
    container_id = new_db.last_id
    
    images.each_with_index do |img, imgindex|
      if img[:position].nil?
        img[:position] = imgindex
      end
      
      new_file = "'#{img[:id]}-original#{img[:file_file_name].scan(/\.[a-z]+$/i)[0]}'"
      img.each_pair do |k,v|
        if img[k].class == String
          img[k] = "'" + old_db.escape(img[k]) + "'"
        elsif img[k].class == NilClass
          img[k] = "NULL"
        elsif img[k].class == Time
          img[k] = "'" + img[k].to_s.gsub(" -0400", "") + "'"
        end
      end
      
      query = "INSERT INTO gallery_images (container_id, position, caption, filename, created_at, updated_at) VALUES (#{container_id}, #{img[:position]}, NULL, #{new_file}, #{img[:created_at]}, #{img[:updated_at]});"
      new_db.query(query)
    end
  end
  
  downloads = old_db.query("SELECT * FROM downloads WHERE theme_id = #{theme[:id]} AND file_file_name IS NOT NULL", :symbolize_keys => true)
  if downloads.size > 0 && theme[:download_enabled] == 1
    
    query = "INSERT INTO containers (page_id, position, title, child_object, created_at, updated_at) VALUES (#{i+1}, 4, #{theme[:downloads_title]}, 'downloads', #{theme[:created_at]}, #{theme[:updated_at]});"
    new_db.query(query)
    container_id = new_db.last_id
    
    downloads.each_with_index do |d, dindex|
      d.each_pair do |k,v|
        if d[k].class == String
          d[k] = "'" + old_db.escape(d[k]) + "'"
        elsif d[k].class == NilClass
          d[k] = "NULL"
        elsif d[k].class == Time
          d[k] = "'" + d[k].to_s.gsub(" -0400", "") + "'"
        end
      end
      
      query = "INSERT INTO downloads (container_id, position, caption, filename, created_at, updated_at) VALUES (#{container_id}, #{dindex}, #{d[:title]}, #{d[:file_file_name]}, #{d[:created_at]}, #{d[:updated_at]});"
      new_db.query(query)
    end
  end
  
  hours = old_db.query("SELECT * FROM hours WHERE theme_id = #{theme[:id]}", :symbolize_keys => true)
  if hours.size > 0 && theme[:hours_enabled] == 1
    query = "INSERT INTO containers (page_id, position, title, child_object, created_at, updated_at) VALUES (#{i+1}, 100, #{theme[:hours_title]}, 'hours', #{theme[:created_at]}, #{theme[:updated_at]});"
    new_db.query(query)
    container_id = new_db.last_id
    
    hours.each_with_index do |h, cindex|
      h.each_pair do |k, v|
        if h[k].class == String
          h[k] = "'" + old_db.escape(h[k]) + "'"
        elsif h[k].class == NilClass
          h[k] = "NULL"
        elsif h[k].class == Time
          h[k] = "'" + h[k].to_s.gsub(" -0400", "") + "'"
        end
      end
      
      query = "INSERT INTO hours (container_id, day, start, end, created_at, updated_at) VALUES (#{container_id}, #{h[:day]}, #{h[:start]}, #{h[:end]}, #{h[:created_at]}, #{h[:updated_at]});"
      new_db.query(query)
    end
  end
  
  cs = old_db.query("SELECT * FROM contacts WHERE theme_id = #{theme[:id]}", :symbolize_keys => true)
  if cs.size > 0 && theme[:contact_enabled] == 1
    cs = cs.first
    cs.each_pair do |k, v|
      if cs[k].class == String
        cs[k] = "'" + old_db.escape(cs[k]) + "'"
      elsif cs[k].class == NilClass
        cs[k] = "NULL"
      elsif cs[k].class == Time
        cs[k] = "'" + cs[k].to_s.gsub(" -0400", "") + "'"
      end
    end
    
    query = "INSERT INTO contact_sections (page_id, position, title, email, phone, fax, address, address2, city, state, zip, map_enabled, created_at, updated_at) VALUES (#{i+1}, 101, #{theme[:contacts_title]}, #{cs[:email]}, #{cs[:phone]}, #{cs[:fax]}, #{cs[:address]}, #{cs[:address2]}, #{cs[:city]}, #{cs[:state]}, #{cs[:zip]}, #{cs[:map_enabled]}, #{cs[:created_at]}, #{cs[:updated_at]})"
    new_db.query(query)
  end
  
  soc = old_db.query("SELECT * FROM links WHERE theme_id = #{theme[:id]} AND (facebook IS NOT NULL OR twitter IS NOT NULL OR linkedin IS NOT NULL OR flickr IS NOT NULL OR yelp IS NOT NULL OR foursquare IS NOT NULL OR tweet_button IS NOT NULL OR like_button IS NOT NULL OR google_plus IS NOT NULL OR blog IS NOT NULL)", :symbolize_keys => true)
  if soc.size > 0 && theme[:links_enabled] == 1
    soc = soc.first
    
    
    soc[:like_button] = 0 if soc[:like_button].class == NilClass
    soc[:tweet_button] = 0 if soc[:tweet_button].class == NilClass
    soc[:google_plus] = 0 if soc[:google_plus].class == NilClass
    
    soc.each_pair do |k, v|
      if soc[k].class == String
        soc[k] = "'" + old_db.escape(soc[k]) + "'"
      elsif soc[k].class == NilClass
        soc[k] = "NULL"
      elsif soc[k].class == Time
        soc[k] = "'" + soc[k].to_s.gsub(" -0400", "") + "'"
      end
    end
    
    query = "INSERT INTO containers (page_id, position, child_object, created_at, updated_at) VALUES (#{i+1}, 102, 'social_buttons', #{soc[:created_at]}, #{soc[:updated_at]});"
    new_db.query(query)
    container_id = new_db.last_id
    query = "INSERT INTO social_buttons (container_id, like_button, tweet_button, google_plus_button, created_at, updated_at) VALUES (#{container_id}, #{soc[:like_button]}, #{soc[:tweet_button]}, #{soc[:google_plus]}, #{soc[:created_at]}, #{soc[:updated_at]});"
    new_db.query(query)
    social_buttons_id = new_db.last_id
    
    if soc[:facebook] != "''" && soc[:facebook] != "NULL"
      query = "INSERT INTO social_links (social_button_id, position, service, url, created_at, updated_at) VALUES (#{social_buttons_id}, 0, 'Facebook', #{soc[:facebook]}, #{soc[:created_at]}, #{soc[:updated_at]});"
      new_db.query(query)
    end
    if soc[:twitter] != "''" && soc[:twitter] != "NULL"
      query = "INSERT INTO social_links (social_button_id, position, service, url, created_at, updated_at) VALUES (#{social_buttons_id}, 1, 'Twitter', #{soc[:twitter]}, #{soc[:created_at]}, #{soc[:updated_at]});"
      new_db.query(query)
    end
    if soc[:linkedin] != "''" && soc[:linkedin] != "NULL"
      query = "INSERT INTO social_links (social_button_id, position, service, url, created_at, updated_at) VALUES (#{social_buttons_id}, 2, 'LinkedIn', #{soc[:linkedin]}, #{soc[:created_at]}, #{soc[:updated_at]});"
      new_db.query(query)
    end
    if soc[:yelp] != "''" && soc[:yelp] != "NULL"
      query = "INSERT INTO social_links (social_button_id, position, service, url, created_at, updated_at) VALUES (#{social_buttons_id}, 3, 'Yelp', #{soc[:yelp]}, #{soc[:created_at]}, #{soc[:updated_at]});"
      new_db.query(query)
    end
    if soc[:foursquare] != "''" && soc[:foursquare] != "NULL"
      query = "INSERT INTO social_links (social_button_id, position, service, url, created_at, updated_at) VALUES (#{social_buttons_id}, 4, 'Foursquare', #{soc[:foursquare]}, #{soc[:created_at]}, #{soc[:updated_at]});"
      new_db.query(query)
    end
    if soc[:blog] != "''" && soc[:blog] != "NULL"
      query = "INSERT INTO social_links (social_button_id, position, service, url, created_at, updated_at) VALUES (#{social_buttons_id}, 5, 'Blog', #{soc[:blog]}, #{soc[:created_at]}, #{soc[:updated_at]});"
      new_db.query(query)
    end
  end
  
  an = old_db.query("SELECT * FROM analytics WHERE theme_id = #{theme[:id]}", :symbolize_keys => true)
  an.each_with_index do |an, cindex|
    an.each_pair do |k, v|
      if an[k].class == String
        an[k] = "'" + old_db.escape(an[k]) + "'"
      elsif an[k].class == NilClass
        an[k] = "NULL"
      elsif an[k].class == Time
        an[k] = "'" + an[k].to_s.gsub(" -0400", "") + "'"
      end
    end
    query = "INSERT INTO analytics (site_id, total_views, avg_time_on_site, new_visitors, deep_dive, keywords, sources, time_travel, created_at, updated_at) VALUES (#{i+1}, #{an[:total_views]}, #{an[:avg_time_on_site]}, #{an[:new_visitors]}, #{an[:deep_dive]}, #{an[:keywords]}, #{an[:sources]}, #{an[:time_travel]}, #{an[:created_at]}, #{an[:updated_at]});"
    new_db.query(query)
  end
  
}

# puts query
old_db.close
new_db.close
# File.open('import.sql', 'w') {|f| f.write(query) }