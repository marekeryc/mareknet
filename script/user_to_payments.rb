require 'mysql2'
old_db = Mysql2::Client.new(:host => "localhost", :username => "onepagr", :password => "0nir", :database => "onepagr_prod")
new_db = Mysql2::Client.new(:host => "localhost", :username => "onr", :password => "0nir", :database => "onepager_dev")

results = old_db.query("SELECT * FROM users WHERE authorization IS NOT NULL;")

results.each_with_index(:symbolize_keys => true) do |old_user, i|
	old_user.each_pair do |k, v|
      if old_user[k].class == String
        old_user[k] = "'" + old_db.escape(old_user[k]) + "'"
      elsif old_user[k].class == NilClass
        old_user[k] = "NULL"
      elsif old_user[k].class == Time
        old_user[k] = "'" + old_user[k].to_s.gsub(" -0400", "") + "'"
      end
    end
	query = "INSERT INTO PAYMENTS (user_id, authorization_number, subscriber_id_monthly, subscriber_id_annual, created_at, updated_at) VALUES (#{old_user[:id]}, #{old_user[:authorization]}, #{old_user[:subscriber_id_monthly]}, #{old_user[:subscriber_id_annual]}, #{old_user[:created_at]}, #{old_user[:created_at]});"
	new_db.query(query)
end

old_db.close
new_db.close