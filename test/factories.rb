include ActionDispatch::TestProcess
FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    password { Faker::Lorem.sentence[0..10] }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    address { Faker::Address.street_address }
    address2 { Faker::Address.secondary_address }
    city { Faker::Address.city }
    state { Faker::Address.state }
    zip { Faker::Address.zip_code }
    country { "United States" }
    payment_frequency { "Trial" }
  end
  
  factory :site do
    # user
    company_name { Faker::Company.name }
    domain_type { "no_custom_domain" }
    domain_name {
      if domain_type != "no_custom_domain"
        Faker::Internet.domain_name
      end
    }
    onepager_address { Faker::Lorem.words(2).join("") }
  end
  
  factory :page do
    site
  end
  
  factory :free_user, class: User do
    email { Faker::Internet.email }
    password { Faker::Lorem.sentence[0..10] }
    payment_frequency { "Trial" }
    after(:build) do |user, evaluator|
      user.sites << FactoryGirl.build(:site, user: user, domain_type: "no_custom_domain")
      user.sites[0].pages << FactoryGirl.build(:page, site: user.sites[0])
    end
  end
  
  factory :paid_user_with_custom_domain, class: User do
    email { Faker::Internet.email }
    password { Faker::Lorem.sentence[0..10] }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    address { Faker::Address.street_address }
    address2 { Faker::Address.secondary_address }
    city { Faker::Address.city }
    state { Faker::Address.state }
    zip { Faker::Address.zip_code }
    country { "United States" }
    payment_frequency { "Monthly" }
    promo_code { }
    # ignore do
    #   domain_type { "existing_custom_domain" }
    # end
    # after(:create) do |user, evaluator|
    #   user.sites << FactoryGirl.create(:site, user: user, domain_name: Faker::Internet.domain_name, domain_type: evaluator.domain_type)
    #   user.sites[0].pages << FactoryGirl.create(:page, site: user.sites[0])
    # end
  end
  
  factory :button do
    page
    position { 0 }
    title { Faker::Lorem.sentence }
    url { Faker::Internet.domain_name }
    
    after(:build) do |button, evaluator|
      button.page = FactoryGirl.create(:page)
    end
  end
  
  factory :contact_section do
    page
    position { 0 }
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    email { Faker::Internet.email }
    phone { Faker::PhoneNumber.phone_number }
    fax { Faker::PhoneNumber.phone_number }
    address { Faker::Address.street_address }
    address2 { Faker::Address.secondary_address }
    city { Faker::Address.city }
    state { Faker::Address.state }
    zip { Faker::Address.zip_code }
    map_enabled { }
    
    after(:build) do |contact_section, evaluator|
      contact_section.page = FactoryGirl.create(:page)
    end
  end
  
  factory :embed do
    page
    position { 0 }
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    url { }
    body { }
    category { }
    
    after(:build) do |embed, evaluator|
      embed.page = FactoryGirl.create(:page)
    end
  end
  
  factory :text_area do
    page
    position { 0 }
    title { Faker::Lorem.sentence }
    body { Faker::Lorem.paragraph }
    
    after(:build) do |text_area, evaluator|
      text_area.page = FactoryGirl.create(:page)
    end
  end

  factory :product do
    page
    position { 0 }
    name { Faker::Lorem.sentence }
    url
    price
    description
    max_purchase_count
    country_available
    product_type
    gumroad_id
    gumroad_url
    preview_file
    product_file
  end

  factory :iframe do
    page
    position { 0 }
    embed_code %{<iframe height="710" allowTransparency="true" frameborder="0" scrolling="no" style="width:100%;border:none" src="http://jamesiha.wufoo.com/embed/z7x4m1/"><a href="http://jamesiha.wufoo.com/forms/z7x4m1/">Fill out my Wufoo form!</a></iframe>}
  end
  
  # Container Content Types
  
  factory :container do
    page
    position { 0 }
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    child_object { }
    
    factory :container_with_services do
      child_object { "services" }
      after(:build) do |container, evaluator|
        (2..5).each do
          container.services << FactoryGirl.build(:service, :container => container)
        end
        container.page = FactoryGirl.create(:page)
      end
    end
    
    factory :container_with_hours do
      child_object { "hours" }
      after(:build) do |container, evaluator|
        (2..5).each do
          container.hours << FactoryGirl.build(:hour, :container => container)
        end
        container.page = FactoryGirl.create(:page)
      end
    end
    
    factory :container_with_social_buttons do
      child_object { "social_buttons" }
      after(:build) do |container, evaluator|
        container.social_button = FactoryGirl.build(:social_button, :container => container)
        container.page = FactoryGirl.create(:page)
      end
    end
    
    factory :container_with_contact_form_fields do
      child_object { "contact_form_fields" }
      after(:build) do |container, evaluator|
        (2..5).each do
          container.contact_form_fields << FactoryGirl.build(:contact_form_field, :container => container)
        end
        container.page = FactoryGirl.create(:page)
      end
    end
    
    factory :container_with_downloads do
      child_object { "downloads" }
      after(:build) do |container, evaluator|
        (0..1).each do
          container.downloads << FactoryGirl.create(:download, :container => container)
        end
        container.page = FactoryGirl.create(:page)
      end
    end
    
    factory :container_with_gallery_images do
      child_object { "gallery_images" }
      after(:build) do |container, evaluator|
        (0..1).each do
          container.gallery_images << FactoryGirl.create(:gallery_image, :container => container)
        end
        container.page = FactoryGirl.create(:page)
      end
    end
  end
  
  factory :service do
    container
    position { 0 }
    body { Faker::Lorem.sentence }
  end
  
  factory :hour do
    container
    day { Hour::DAYS[rand(7)] }
    start { Hour::TIMES[rand(48)] }
    self.end { Hour::TIMES[rand(48)] }
  end
  
  factory :social_button do
    container
    like_button { true }
    tweet_button { true }
    google_plus_button { }
    
    after(:build) do |social_button, evaluator|
      (0..5).each do
        social_button.social_links << FactoryGirl.build(:social_link, :social_button => social_button)
      end
    end
  end
  
  factory :social_link do
    social_button
    position { 0 }
    service { SocialLink::SERVICES[rand(10)] }
    url { Faker::Internet.domain_name }
  end
  
  factory :contact_form_field do
    container
    position { 0 }
    field_name { Faker::Lorem.sentence }
    field_type { ContactFormField::FIELD_TYPES.map{|x| x[1]}[rand(2)] }
    required { [true, false][rand(2)] }
  end
  
  factory :contact_form_response do
    contact_form_field
    value { Faker::Lorem.sentence }
  end
  
  factory :subscriber do
    site
    email { Faker::Internet.email }
    active { }
  end
  
  factory :download do
    container
    position { 0 }
    caption { Faker::Lorem.sentence }
    filename {  fixture_file_upload(Rails.root.join('test', 'fixtures', 'fw2.pdf'), 'application/pdf') }
  end
  
  factory :gallery_image do
    container
    position { 0 }
    caption { Faker::Lorem.sentence }
    filename {  fixture_file_upload(Rails.root.join('test', 'fixtures', 'AppleEvolution.jpg'), 'image/jpeg') }
  end
end