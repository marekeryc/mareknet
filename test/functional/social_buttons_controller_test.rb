require 'test_helper'

class ContentTypes::SocialButtonsControllerTest < ActionController::TestCase
  setup :activate_authlogic

  def setup
    @cws = FactoryGirl.create(:container_with_social_buttons)
    u = FactoryGirl.build(:free_user)
    u.sites << @cws.page.site
    u.save
    UserSession.create(u)
  end

  test "should get new" do
    xhr :get, :new
    assert_response :success
    assert_not_nil assigns(:container)
  end
  
  test "should create social button and links" do
    social_links_hash = Hash.new
    @cws.social_button.social_links.each_with_index do |sl, i|
      social_links_hash.merge!({"#{i}" => { "service" => sl.service, "url" => sl.url } })
    end
    
    social_button_hash = {"like_button" => "#{@cws.social_button.like_button}", "tweet_button" => "#{@cws.social_button.tweet_button}", "google_plus_button" => "#{@cws.social_button.google_plus_button}", "social_links_attributes" => social_links_hash}
    
    assert_difference('SocialLink.count', @cws.social_button.social_links.length) do
      xhr :post, :create, :container => { :title => @cws.title, :description => @cws.description, :page_id => @cws.page_id, :social_button_attributes => social_button_hash }
    end
    assert_equal "social_buttons", assigns(:container).child_object
    assert_response :success
  end
  
  test "should update social button and links" do
    social_links_hash = Hash.new
    @cws.social_button.social_links.each_with_index do |sl, i|
      social_links_hash.merge!({"#{i}" => { "id" => "#{sl.id}", "service" => sl.service, "url" => sl.url } })
    end
    social_links_hash["0"]["url"] = "http://google.com"
    
    social_button_hash = {"id" => "#{@cws.social_button.id}", "like_button" => "0", "tweet_button" => "#{@cws.social_button.tweet_button}", "google_plus_button" => "#{@cws.social_button.google_plus_button}", "social_links_attributes" => social_links_hash}
    
    xhr :put, :update, :id => @cws.id, :container => { :title => @cws.title, :description => @cws.description, :social_button_attributes => social_button_hash }
    assert_equal false, assigns(:container).social_button.like_button
    assert_equal "http://google.com", assigns(:container).social_button.social_links[0].url
    assert_response :success
  end
  
  test "should update container and destroy a social link" do
    social_links_hash = Hash.new
    @cws.social_button.social_links.each_with_index do |sl, i|
      social_links_hash.merge!({"#{i}" => { "id" => "#{sl.id}", "service" => sl.service, "url" => sl.url } })
    end
    social_links_hash["0"].merge!({"_destroy" => "1"})
    
    social_button_hash = {"id" => "#{@cws.social_button.id}", "like_button" => "#{@cws.social_button.like_button}", "tweet_button" => "#{@cws.social_button.tweet_button}", "google_plus_button" => "#{@cws.social_button.google_plus_button}", "social_links_attributes" => social_links_hash}
    
    assert_difference('SocialLink.count', -1) do
      xhr :put, :update, :id => @cws.id, :container => { :title => @cws.title, :description => @cws.description, :social_button_attributes => social_button_hash }
    end
    
    assert_response :success
  end
  
  test "should destroy social button and links" do    
    assert_difference('Container.count', -1) do
      assert_difference('SocialButton.count', -1) do
        assert_difference('SocialLink.count', 0 - @cws.social_button.social_links.length) do
          xhr :delete, :destroy, :id => @cws.id
        end
      end
    end
    assert_response :success
  end
end