require 'test_helper'

class ContentTypes::IframesControllerTest < ActionController::TestCase
  setup :activate_authlogic

  test "should get new" do
    xhr :get, :new
    assert_response :success
    assert_not_nil assigns(:iframe)
  end
  
  test "should create iframe" do
    setup_iframe_and_user

    assert_difference('Iframe.count', 1) do
      xhr :post, :create, :iframe => { :embed_code => @iframe.embed_code, :page_id => @iframe.page_id }
    end
    assert_not_nil assigns(:iframe)
    assert_response :success
  end
  
  test "should not create iframe" do
    setup_iframe_and_user

    assert_no_difference('Iframe.count') do
      xhr :post, :create, :iframe => { :embed_code => "google.com", :page_id => @iframe.page_id }
    end
    assert_response :unprocessable_entity
  end
  
  test "should update iframe" do
    setup_iframe_and_user
    embed_code = %{ <iframe src="http://stage.uplanme.com/business/widget/685529?size=2&color1=1E5867&color2=E4EBEB" width="400" height="400"></iframe> }
    
    xhr :put, :update, :id => @iframe.id, :iframe => { :embed_code => embed_code }
    assert_equal "http://stage.uplanme.com/business/widget/685529?size=2&color1=1E5867&color2=E4EBEB", assigns(:iframe).embed_src
    assert_equal "400", assigns(:iframe).embed_height
    assert_response :success
  end
  
  test "should destroy iframe" do
    setup_iframe_and_user

    assert_difference('Iframe.count', -1) do
      xhr :delete, :destroy, :id => @iframe.id
    end
    assert_response :success
  end

  def setup_iframe_and_user
    @iframe = FactoryGirl.create(:iframe)
    u = FactoryGirl.build(:free_user)
    u.sites << @iframe.page.site
    u.save
    UserSession.create(u)
  end
end