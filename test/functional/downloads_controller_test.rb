require 'test_helper'

class ContentTypes::DownloadsControllerTest < ActionController::TestCase
  test "should create" do
    assert_difference('Download.count', 2) do
      xhr :post, :create, :download_files => [ fixture_file_upload('fw2.pdf', 'application/pdf'), fixture_file_upload('fw2.pdf', 'application/pdf') ]
    end
    
    assert_difference('Download.count', 1) do
      FactoryGirl.create(:download)
    end
  end
end