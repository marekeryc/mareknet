require 'test_helper'

class ContentTypes::TextAreasControllerTest < ActionController::TestCase
  setup :activate_authlogic

  def setup
    @t = FactoryGirl.create(:text_area)
    u = FactoryGirl.build(:free_user)
    u.sites << @t.page.site
    u.save
    UserSession.create(u)
  end

  test "should get new" do
    xhr :get, :new
    assert_response :success
    assert_not_nil assigns(:text_area)
  end
  
  test "should create text area" do
    assert_difference('TextArea.count', 1) do
      xhr :post, :create, :text_area => { :title => @t.title, :body => @t.body, :page_id => @t.page_id }
    end
    assert_not_nil assigns(:text_area)
    assert_response :success
  end
  
  test "should not create text area" do
    assert_no_difference('TextArea.count') do
      xhr :post, :create, :text_area => { :title => "Van Morrison", :page_id => @t.page.id }
    end
    assert_response :unprocessable_entity
  end
  
  test "should update text area" do
    xhr :put, :update, :id => @t.id, :text_area => { :title => "Do Something Now", :body => "Brown Eyed Girl" }
    assert_equal "Do Something Now", assigns(:text_area).title
    assert_equal "Brown Eyed Girl", assigns(:text_area).body
    assert_response :success
  end
  
  test "should destroy text area" do
    assert_difference('TextArea.count', -1) do
      xhr :delete, :destroy, :id => @t.id
    end
    assert_response :success
  end
end
