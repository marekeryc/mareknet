require 'test_helper'

class ContentTypes::ServicesControllerTest < ActionController::TestCase
  setup :activate_authlogic

  def setup
    @cws = FactoryGirl.create(:container_with_services)
    u = FactoryGirl.build(:free_user)
    u.sites << @cws.page.site
    u.save
    UserSession.create(u)
  end

  test "should get new" do
    xhr :get, :new
    assert_response :success
    assert_not_nil assigns(:container)
  end
  
  test "should create services" do
    services_hash = Hash.new
    @cws.services.each_with_index do |s, i|
      services_hash.merge!({"#{i}" => { "body" => s.body } })
    end
    
    assert_difference('Service.count', @cws.services.length) do
      xhr :post, :create, :container => { :title => @cws.title, :description => @cws.description, :page_id => @cws.page_id, :services_attributes => services_hash }
    end
    assert_equal "services", assigns(:container).child_object
    assert_response :success
  end
  
  test "should not create services" do
    services_hash = Hash.new
    @cws.services.each_with_index do |s, i|
      services_hash.merge!({"#{i}" => { "body" => "" } })
    end
    
    assert_no_difference('Service.count') do
      xhr :post, :create, :container => { :title => @cws.title, :description => @cws.description, :page_id => @cws.page_id, :services_attributes => services_hash }
    end
    assert_response :unprocessable_entity
  end
  
  test "should update service" do
    services_hash = Hash.new
    @cws.services.each_with_index do |s, i|
      services_hash.merge!({"#{i}" => { "id" => "#{s.id}", "body" => s.body } })
    end
    services_hash["0"]["body"] = "Chuck Schumer"
    
    xhr :put, :update, :id => @cws.id, :container => { :title => @cws.title, :description => @cws.description, :services_attributes => services_hash }
    assert_equal "Chuck Schumer", assigns(:container).services[0].body
    assert_response :success
  end
  
  test "should update container and destroy a service" do
    services_hash = Hash.new
    @cws.services.each_with_index do |s, i|
      services_hash.merge!({"#{i}" => { "id" => "#{s.id}", "body" => s.body } })
    end
    services_hash["0"].merge!({"_destroy" => "1"})
    
    assert_difference('Service.count', -1) do
      xhr :put, :update, :id => @cws.id, :container => { :title => @cws.title, :description => @cws.description, :services_attributes => services_hash }
    end
    
    assert_response :success
  end
  
  test "should destroy services" do
    assert_difference('Container.count', -1) do
      assert_difference('Service.count', 0 - @cws.services.length) do
        xhr :delete, :destroy, :id => @cws.id
      end
    end
    assert_response :success
  end
end