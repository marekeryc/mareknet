require 'test_helper'

class ContentTypes::DownloadSectionsControllerTest < ActionController::TestCase
  setup :activate_authlogic

  def setup
    @cwd = FactoryGirl.create(:container_with_downloads)
    u = FactoryGirl.build(:free_user)
    u.sites << @cwd.page.site
    u.save
    UserSession.create(u)
  end

  test "should get new" do
    xhr :get, :new
    assert_response :success
    assert_not_nil assigns(:container)
  end
  
  test "should create" do
    d = Download.create(
      :position => 0,
      :caption => "fjkdlsjfld",
      :filename => fixture_file_upload('fw2.pdf', 'application/pdf')
    )

    assert_difference('Container.count', 1) do
      xhr :post, :create, :container_id => "new", :container => { :title => "Lorem", :description => "Ipsum", :page_id => @cwd.page.id, :position => "1", :downloads_attributes => { "#{d.id}" => { :caption => "Lorem", :id => "#{d.id}" } } }
    end
    assert_equal "downloads", assigns(:container).child_object
    assert_not_nil assigns(:container)
    assert_response :success
  end
  
  test "should update download sections" do
    downloads_hash = Hash.new
    @cwd.downloads.each_with_index do |d, i|
      downloads_hash.merge!({"#{i}" => { "id" => "#{d.id}", "caption" => d.caption } })
    end
    downloads_hash["0"]["caption"] = "Chuck Schumer"
    
    xhr :put, :update, :id => @cwd.id, :container => { :title => @cwd.title, :description => @cwd.description, :downloads_attributes => downloads_hash }
    assert_equal "Chuck Schumer", assigns(:container).downloads[0].caption
    assert_response :success
  end
  
  test "should update container and destroy a download" do
    downloads_hash = Hash.new
    @cwd.downloads.each_with_index do |d, i|
      downloads_hash.merge!({"#{i}" => { "id" => "#{d.id}", "caption" => d.caption } })
    end
    downloads_hash["0"].merge!({"_destroy" => "1"})
    
    assert_difference('Download.count', -1) do
      xhr :put, :update, :id => @cwd.id, :container => { :title => @cwd.title, :description => @cwd.description, :downloads_attributes => downloads_hash }
    end
    
    assert_response :success
  end
  
  test "should destroy container" do
    assert_difference('Container.count', -1) do
      assert_difference('Download.count', 0 - @cwd.downloads.length) do
        xhr :delete, :destroy, :id => @cwd.id
      end
    end
    assert_response :success
  end
end