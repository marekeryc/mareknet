require 'test_helper'

class ContentTypes::ContactFormFieldsControllerTest < ActionController::TestCase
  setup :activate_authlogic

  def setup
    @cwc = FactoryGirl.create(:container_with_contact_form_fields)
    u = FactoryGirl.build(:free_user)
    u.sites << @cwc.page.site
    u.save
    UserSession.create(u)
  end

  test "should get new" do
    xhr :get, :new
    assert_response :success
    assert_not_nil assigns(:container)
  end
  
  test "should create contact form fields" do
    contact_form_fields_hash = Hash.new
    @cwc.contact_form_fields.each_with_index do |c, i|
      contact_form_fields_hash.merge!({"#{i}" => { "field_name" => c.field_name, "field_type" => c.field_type } })
    end
    
    assert_difference('ContactFormField.count', @cwc.contact_form_fields.length) do
      xhr :post, :create, :container => { :title => @cwc.title, :description => @cwc.description, :page_id => @cwc.page_id, :contact_form_fields_attributes => contact_form_fields_hash }
    end
    assert_equal "contact_form_fields", assigns(:container).child_object
    assert_response :success
  end
  
  test "should not create contact form fields" do
    contact_form_fields_hash = Hash.new
    @cwc.contact_form_fields.each_with_index do |c, i|
      contact_form_fields_hash.merge!({"#{i}" => { "field_name" => "", "field_type" => c.field_type } })
    end
    
    assert_no_difference('ContactFormField.count') do
      xhr :post, :create, :container => { :title => @cwc.title, :description => @cwc.description, :page_id => @cwc.page_id, :contact_form_fields_attributes => contact_form_fields_hash }
    end
    assert_response :unprocessable_entity
  end
  
  test "should update contact form field" do
    contact_form_fields_hash = Hash.new
    @cwc.contact_form_fields.each_with_index do |c, i|
      contact_form_fields_hash.merge!({"#{i}" => { "id" => "#{c.id}", "field_name" => c.field_name, "field_type" => c.field_type } })
    end
    contact_form_fields_hash["0"]["field_name"] = "Chuck Schumer"
    
    xhr :put, :update, :id => @cwc.id, :container => { :title => @cwc.title, :description => @cwc.description, :contact_form_fields_attributes => contact_form_fields_hash }
    assert_equal "Chuck Schumer", assigns(:container).contact_form_fields[0].field_name
    assert_response :success
  end
  
  test "should update container and destroy a contact form field" do
    contact_form_fields_hash = Hash.new
    @cwc.contact_form_fields.each_with_index do |c, i|
      contact_form_fields_hash.merge!({"#{i}" => { "id" => "#{c.id}", "field_name" => c.field_name, "field_type" => c.field_type } })
    end
    contact_form_fields_hash["0"].merge!({"_destroy" => "1"})
    
    assert_difference('ContactFormField.count', -1) do
      xhr :put, :update, :id => @cwc.id, :container => { :title => @cwc.title, :description => @cwc.description, :contact_form_fields_attributes => contact_form_fields_hash }
    end
    
    assert_response :success
  end
  
  test "should destroy contact form fields" do
    assert_difference('Container.count', -1) do
      assert_difference('ContactFormField.count', 0 - @cwc.contact_form_fields.length) do
        xhr :delete, :destroy, :id => @cwc.id
      end
    end
    assert_response :success
  end
end