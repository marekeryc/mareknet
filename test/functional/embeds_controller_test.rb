require 'test_helper'

class ContentTypes::EmbedsControllerTest < ActionController::TestCase
  setup :activate_authlogic

  def setup
    @embed = FactoryGirl.create(:embed, url: "http://youtube.com/watch?v=oHg5SJYRHA0")
    u = FactoryGirl.build(:free_user)
    u.sites << @embed.page.site
    u.save
    UserSession.create(u)
  end

  test "should get new" do
    xhr :get, :new
    assert_response :success
    assert_not_nil assigns(:embed)
  end
  
  test "should create embed" do
    assert_difference('Embed.count', 1) do
      xhr :post, :create, :embed => { :url => "http://youtube.com/watch?v=oHg5SJYRHA0", :page_id => @embed.page.id  }
    end
    assert_not_nil assigns(:embed)
    assert_response :success
  end
  
  test "should not create embed" do
    assert_no_difference('Embed.count') do
      xhr :post, :create, :embed => { :url => "hsdfasdfHA0", :page_id => @embed.page.id }
    end
    assert_response :unprocessable_entity
  end
  
  test "should update embed" do
    xhr :put, :update, :id => @embed.id, :embed => { :title => "Do Something Now", :description => "Lorim Ipsom" }
    assert_equal "Do Something Now", assigns(:embed).title
    assert_equal "Lorim Ipsom", assigns(:embed).description
    assert_response :success
  end
  
  test "should destroy embed" do
    assert_difference('Embed.count', -1) do
      xhr :delete, :destroy, :id => @embed.id
    end
    assert_response :success
  end
end