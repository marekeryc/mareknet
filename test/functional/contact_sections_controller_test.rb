require 'test_helper'

class ContentTypes::ContactSectionsControllerTest < ActionController::TestCase
  setup :activate_authlogic

  def setup
    @cs = FactoryGirl.create(:contact_section)
    u = FactoryGirl.build(:free_user)
    u.sites << @cs.page.site
    u.save
    UserSession.create(u)
  end

  test "should get new" do
    xhr :get, :new
    assert_response :success
    assert_not_nil assigns(:contact_section)
  end
  
  test "should create contact section" do
    assert_difference('ContactSection.count', 1) do
      xhr :post, :create, :contact_section => { :title => @cs.title, :description => @cs.description, :email => @cs.email, :page_id => @cs.page_id }
    end
    assert_not_nil assigns(:contact_section)
    assert_response :success
  end
  
  test "should not create contact section" do
    assert_no_difference('ContactSection.count') do
      xhr :post, :create, :contact_section => { :title => "Lorem Ipsum", :page_id => @cs.page_id }
    end
    assert_response :unprocessable_entity
  end
  
  test "should update contact section" do
    xhr :put, :update, :id => @cs.id, :contact_section => { :title => "Do Something Now", :address => "123 Main St." }
    assert_equal "Do Something Now", assigns(:contact_section).title
    assert_equal "123 Main St.", assigns(:contact_section).address
    assert_response :success
  end
  
  test "should destroy button" do
    assert_difference('ContactSection.count', -1) do
      xhr :delete, :destroy, :id => @cs.id
    end
    assert_response :success
  end
end
