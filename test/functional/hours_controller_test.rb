require 'test_helper'

class ContentTypes::HoursControllerTest < ActionController::TestCase
  setup :activate_authlogic

  def setup
    @cwh = FactoryGirl.create(:container_with_hours)
    u = FactoryGirl.build(:free_user)
    u.sites << @cwh.page.site
    u.save
    UserSession.create(u)
  end

  test "should get new" do
    xhr :get, :new
    assert_response :success
    assert_not_nil assigns(:container)
  end
  
  test "should create hours" do
    hours_hash = Hash.new
    @cwh.hours.each_with_index do |h, i|
      hours_hash.merge!({"#{i}" => { "day" => h.day, "start" => h.start, "end" => h.end } })
    end
    
    assert_difference('Hour.count', @cwh.hours.length) do
      xhr :post, :create, :container => { :title => @cwh.title, :description => @cwh.description, :page_id => @cwh.page_id, :hours_attributes => hours_hash }
    end
    assert_equal "hours", assigns(:container).child_object
    assert_response :success
  end
  
  test "should not create hours" do
    hours_hash = Hash.new
    @cwh.hours.each_with_index do |h, i|
      hours_hash.merge!({"#{i}" => { "day" => "" } })
    end
    
    assert_no_difference('Hour.count') do
      xhr :post, :create, :container => { :title => @cwh.title, :description => @cwh.description, :page_id => @cwh.page_id, :hours_attributes => hours_hash }
    end
    assert_response :unprocessable_entity
  end
  
  test "should update hour" do
    hours_hash = Hash.new
    @cwh.hours.each_with_index do |h, i|
      hours_hash.merge!({"#{i}" => { "id" => "#{h.id}", "day" => h.day, "start" => h.start, "end" => h.end } })
    end
    hours_hash["0"]["day"] = "Saturday"
    
    xhr :put, :update, :id => @cwh.id, :container => { :title => @cwh.title, :description => @cwh.description, :hours_attributes => hours_hash }
    assert_equal "Saturday", assigns(:container).hours[0].day
    assert_response :success
  end
  
  test "should update container and destroy a service" do
    hours_hash = Hash.new
    @cwh.hours.each_with_index do |h, i|
      hours_hash.merge!({"#{i}" => { "id" => "#{h.id}", "day" => h.day, "start" => h.start, "end" => h.end } })
    end
    hours_hash["0"].merge!({"_destroy" => "1"})
    
    assert_difference('Hour.count', -1) do
      xhr :put, :update, :id => @cwh.id, :container => { :title => @cwh.title, :description => @cwh.description, :hours_attributes => hours_hash }
    end
    
    assert_response :success
  end
  
  test "should destroy hours" do
    assert_difference('Container.count', -1) do
      assert_difference('Hour.count', 0 - @cwh.hours.length) do
        xhr :delete, :destroy, :id => @cwh.id
      end
    end
    assert_response :success
  end
end