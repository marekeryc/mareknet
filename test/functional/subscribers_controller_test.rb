require 'test_helper'

class SubscribersControllerTest < ActionController::TestCase
  def setup
    u = FactoryGirl.build(:paid_user_with_custom_domain)
    @container = FactoryGirl.create(:container, child_object: "newsletters")
    u.sites << @container.page.site
    u.save
    @subscriber = Subscriber.new
    @subscriber.email = Faker::Internet.email
    @subscriber.site_id_enc = Subscriber.encrypt(@container.page.site.id.to_s)
  end
  
  test "should create subscriber" do
    assert_difference('Subscriber.count', 1) do
      xhr :post, :create, :subscriber => { :email => @subscriber.email, :site_id_enc => @subscriber.site_id_enc }
    end
    assert_equal @container.page.site.id, assigns(:subscriber).site_id
    assert_response :success
  end
  
  test "activate and deactivate subscription" do
    @subscriber.save
    
    assert_equal false, @subscriber.active
    
    get :subscribe, :id => @subscriber.to_param
    
    assert_equal true, assigns(:subscriber).active
    
    get :unsubscribe, :id => @subscriber.to_param
    
    assert_equal false, assigns(:subscriber).active
  end
end