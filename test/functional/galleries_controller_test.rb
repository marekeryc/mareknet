require 'test_helper'

class ContentTypes::GalleriesControllerTest < ActionController::TestCase
  setup :activate_authlogic

  def setup
    @cwg = FactoryGirl.create(:container_with_gallery_images)
    u = FactoryGirl.build(:free_user)
    u.sites << @cwg.page.site
    u.save
    UserSession.create(u)
  end

  test "should get new" do
    xhr :get, :new
    assert_response :success
    assert_not_nil assigns(:container)
  end
  
  test "should create" do
    i = GalleryImage.create(
      :position => 0,
      :caption => "fjkdlsjfld",
      :filename => fixture_file_upload('AppleEvolution.jpg', 'image/jpeg')
    )
    assert_difference('Container.count', 1) do
      xhr :post, :create, :container_id => "new", :container => { :title => "Lorem", :description => "Ipsum", :page_id => @cwg.page.id, :position => "1", :gallery_images_attributes => { "#{i.id}" => { :caption => "Lorem", :id => "#{i.id}" } } }
    end
    assert_equal "gallery_images", assigns(:container).child_object
    assert_not_nil assigns(:container)
    assert_response :success
  end
  
  test "should update gallery" do
    gallery_images_hash = Hash.new
    @cwg.gallery_images.each_with_index do |g, i|
      gallery_images_hash.merge!({"#{i}" => { "id" => "#{g.id}", "caption" => g.caption } })
    end
    gallery_images_hash["0"]["caption"] = "Chuck Schumer"
    
    xhr :put, :update, :id => @cwg.id, :container => { :title => @cwg.title, :description => @cwg.description, :gallery_images_attributes => gallery_images_hash }
    assert_equal "Chuck Schumer", assigns(:container).gallery_images[0].caption
    assert_response :success
  end
  
  test "should update container and destroy a gallery image" do
    gallery_images_hash = Hash.new
    @cwg.gallery_images.each_with_index do |g, i|
      gallery_images_hash.merge!({"#{i}" => { "id" => "#{g.id}", "caption" => g.caption } })
    end
    gallery_images_hash["0"].merge!({"_destroy" => "1"})
    
    assert_difference('GalleryImage.count', -1) do
      xhr :put, :update, :id => @cwg.id, :container => { :title => @cwg.title, :description => @cwg.description, :gallery_images_attributes => gallery_images_hash }
    end
    
    assert_response :success
  end
  
  test "should destroy container" do
    assert_difference('Container.count', -1) do
      assert_difference('GalleryImage.count', 0 - @cwg.gallery_images.length) do
        xhr :delete, :destroy, :id => @cwg.id
      end
    end
    assert_response :success
  end
end