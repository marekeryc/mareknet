require 'test_helper'

class ContentTypes::GalleryImagesControllerTest < ActionController::TestCase
  test "should create" do
    assert_difference('GalleryImage.count', 2) do
      xhr :post, :create, :gallery_image_files => [ fixture_file_upload('AppleEvolution.jpg', 'image/jpeg'), fixture_file_upload('AppleEvolution.jpg', 'image/jpeg') ]
    end
    
    assert_difference('GalleryImage.count', 1) do
      FactoryGirl.create(:gallery_image)
    end
  end
  
  test "should not create" do
    xhr :post, :create, :gallery_image_files => [ fixture_file_upload('hugefilesize.gif', 'image/gif') ]
    assert_equal "Filename is too big (should be at most 2 MB)", assigns(:errors)
  end
end