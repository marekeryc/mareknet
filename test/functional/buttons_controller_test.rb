require 'test_helper'

class ContentTypes::ButtonsControllerTest < ActionController::TestCase
  setup :activate_authlogic

  test "should get new" do
    xhr :get, :new
    assert_response :success
    assert_not_nil assigns(:button)
  end
  
  test "should create button" do
    setup_button_and_user

    assert_difference('Button.count', 1) do
      xhr :post, :create, :button => { :title => @button.title, :url => @button.url, :page_id => @button.page_id }
    end
    assert_not_nil assigns(:button)
    assert_response :success
  end
  
  test "should not create button" do
    setup_button_and_user

    assert_no_difference('Button.count') do
      xhr :post, :create, :button => { :url => "google.com", :page_id => @button.page_id }
    end
    assert_response :unprocessable_entity
  end
  
  test "should update button" do
    setup_button_and_user
    
    xhr :put, :update, :id => @button.id, :button => { :title => "Do Something Now", :url => "google.com" }
    assert_equal "Do Something Now", assigns(:button).title
    assert_equal "google.com", assigns(:button).url
    assert_response :success
  end
  
  test "should destroy button" do
    setup_button_and_user

    assert_difference('Button.count', -1) do
      xhr :delete, :destroy, :id => @button.id
    end
    assert_response :success
  end

  def setup_button_and_user
    @button = FactoryGirl.create(:button)
    u = FactoryGirl.build(:free_user)
    u.sites << @button.page.site
    u.save
    UserSession.create(u)
  end
end