require 'test_helper'

class SiteTest < ActiveSupport::TestCase
  test "onepager address format" do
    s = FactoryGirl.build(:site)
    u = FactoryGirl.build(:free_user)
    u.sites << s
    
    s.onepager_address = "something and something"
    assert !s.valid?
    s.onepager_address = "something_and-Something"
    assert s.valid?
  end
  
  test "active false hides the site" do
    a = Site.find(:all).length
    
    u = FactoryGirl.build(:paid_user_with_custom_domain)
    s = FactoryGirl.create(:site)
    u.sites << s
    
    s.save
    u.save
    
    b = Site.find(:all).length
    
    s.active = false
    s.save
    
    c = Site.find(:all).length
    
    assert_equal(c, b-1) 
  end
end
