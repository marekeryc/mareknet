require 'test_helper'

class SubscriberTest < ActiveSupport::TestCase
  def setup
    @container = FactoryGirl.create(:container, child_object: "newsletters")
    
    u = FactoryGirl.build(:paid_user_with_custom_domain)
    u.sites << @container.page.site
    u.save
    assert_equal ActionMailer::Base.deliveries.length, 2
    ActionMailer::Base.deliveries.clear
    
    @subscriber = Subscriber.new
    @subscriber.email = Faker::Internet.email
    @subscriber.site_id_enc = Subscriber.encrypt(@container.page.site.id.to_s)
  end
  
  def teardown
    ActionMailer::Base.deliveries.clear
  end
  
  test "create new subscriber" do
    assert_difference('Subscriber.count', 1) do
      @subscriber.save
    end
    assert_equal @subscriber.site_id, @container.page.site.id
    assert_equal ActionMailer::Base.deliveries.length, 1
  end
  
  test "should not create new subscriber" do
    @subscriber.site_id_enc = ""
    
    assert_difference('Subscriber.count', 0) do
      @subscriber.save
    end
  end
  
  test "to param is encrypted id" do
    @subscriber.save
    assert_equal Subscriber.encrypt(@subscriber.id), @subscriber.to_param 
  end
end