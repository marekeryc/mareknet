require 'test_helper'

class ContactSectionTest < ActiveSupport::TestCase
  test "all fields not blank" do
    cs = ContactSection.new
    cs.save
    assert_equal cs.errors[:base][0], "You've left all fields blank! Please fill out something in the contact section."
    cs.email = "huareiaariug@uidahfudashfnlkx.com"
    assert cs.valid?
  end
  
  test "full address" do
    cs = FactoryGirl.create(:contact_section)
    full_address = cs.address + " " + cs.address2 + " " + cs.city + ", " + cs.state + " " + cs.zip
    assert_equal cs.full_address, full_address
    cs.address2 = nil
    full_address = cs.address + " " + cs.city + ", " + cs.state + " " + cs.zip
    assert_equal cs.full_address, full_address
    cs.city = nil
    full_address = cs.address + " " + cs.state + " " + cs.zip
    assert_equal cs.full_address, full_address
  end
end
