require 'test_helper'

class ContactFormResponseTest < ActiveSupport::TestCase
  def teardown
    ActionMailer::Base.deliveries.clear
  end
  
  test "create many contact form responses" do
    cwc = FactoryGirl.create(:container_with_contact_form_fields)
    
    u = FactoryGirl.build(:paid_user_with_custom_domain)
    u.sites << cwc.page.site
    u.save
    assert_equal ActionMailer::Base.deliveries.length, 2
    ActionMailer::Base.deliveries.clear
    
    cfrs = Array.new
    cwc.contact_form_fields.each do |cff|
      cfr = FactoryGirl.build(:contact_form_response, contact_form_field: cff)
      cfrs << { :contact_form_field_id => cfr.contact_form_field_id, :value => cfr.value}
      cfr = FactoryGirl.build(:contact_form_response, contact_form_field: cff)
      cfrs << { :contact_form_field_id => cfr.contact_form_field_id, :value => cfr.value}
    end
    
    assert_difference('ContactFormResponse.count', cwc.contact_form_fields.length * 2) do
      ContactFormResponse.create_many(cfrs)
    end
    
    assert_equal cwc.contact_form_fields.length * 2, ContactFormResponse.where(:contact_form_field_id => cwc.contact_form_fields.map{|cff| cff.id}).count
  end
  
  test "create no contact form responses" do
    container = FactoryGirl.create(:container)
    
    u = FactoryGirl.build(:paid_user_with_custom_domain)
    u.sites << container.page.site
    u.save
    assert_equal ActionMailer::Base.deliveries.length, 2
    ActionMailer::Base.deliveries.clear
    
    cff1 = FactoryGirl.create(:contact_form_field, required: true)
    cff2 = FactoryGirl.create(:contact_form_field, required: false)
    container.contact_form_fields << cff1
    container.contact_form_fields << cff2
    
    cfrs = [ {:contact_form_field_id => cff1.id, :value => ""}, {:contact_form_field_id => cff2.id, :value => "lksdljkfds"} ]
    response = nil
    
    assert_difference('ContactFormResponse.count', 0) do
      response = ContactFormResponse.create_many(cfrs)
    end
    
    assert_equal cff1.field_name, response[0]
  end
end
