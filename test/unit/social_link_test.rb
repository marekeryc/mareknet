require 'test_helper'

class SocialLinkTest < ActiveSupport::TestCase
  test "full_url" do
    sl = SocialLink.new
    sl.url = "google.com"
    assert_equal "http://google.com", sl.full_url
    sl.url = "http://google.com"
    assert_equal "http://google.com", sl.full_url
  end
end