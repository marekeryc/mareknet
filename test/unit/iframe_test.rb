require 'test_helper'

class IframeTest < ActiveSupport::TestCase
  test "embed code has src" do
    iframe = FactoryGirl.build(:iframe)
    iframe.embed_code = "jkfdsl"
    iframe.save
    assert_equal "Embed code is invalid. Does not contain a src attribute.", iframe.errors.full_messages[0]
  end

  test "embed src" do
  	iframe = FactoryGirl.build(:iframe)
  	iframe.embed_code = %{<iframe height="710" allowTransparency="true" frameborder="0" scrolling="no" style="width:100%;border:none" src="http://jamesiha.wufoo.com/embed/z7x4m1/"><a href="http://jamesiha.wufoo.com/forms/z7x4m1/">Fill out my Wufoo form!</a></iframe>}
  	assert_equal "http://jamesiha.wufoo.com/embed/z7x4m1/", iframe.embed_src
  end

  test "embed height" do
  	iframe = FactoryGirl.build(:iframe)
  	iframe.embed_code = %{<iframe height="710" allowTransparency="true" frameborder="0" scrolling="no" style="width:100%;border:none" src="http://jamesiha.wufoo.com/embed/z7x4m1/"><a href="http://jamesiha.wufoo.com/forms/z7x4m1/">Fill out my Wufoo form!</a></iframe>}
  	assert_equal "710", iframe.embed_height
  end
end