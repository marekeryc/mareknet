require 'test_helper'

class SocialButtonTest < ActiveSupport::TestCase
  test "all fields not blank" do
    sb = SocialButton.new
    sb.like_button = false
    sb.tweet_button = false
    sb.google_plus_button = false
    sb.save
    assert_equal ["You've left everything blank! Please add a social link or button."], sb.errors.full_messages
  end
end
