require 'test_helper'

class ContainerTest < ActiveSupport::TestCase
  test "must_have_at_least_one_child_object" do
    c = Container.new(:child_object => "services")
    assert_equal false, c.valid?
    c.services << FactoryGirl.create(:service)
    assert_equal true, c.valid?
    
    c = Container.new(:child_object => "hours")
    assert_equal false, c.valid?
    c.hours << FactoryGirl.create(:hour)
    assert_equal true, c.valid?
    
    c = Container.new(:child_object => "gallery_images")
    assert_equal false, c.valid?
    c.gallery_images << FactoryGirl.create(:gallery_image)
    assert_equal true, c.valid?
    
    c = Container.new(:child_object => "downloads")
    assert_equal false, c.valid?
    c.downloads << FactoryGirl.create(:download)
    assert_equal true, c.valid?
    
    c = Container.new(:child_object => "contact_form_fields")
    assert_equal false, c.valid?
    c.contact_form_fields << FactoryGirl.create(:contact_form_field)
    assert_equal true, c.valid?
    
    c = Container.new(:child_object => "newsletters")
    assert_equal true, c.valid?
    
    c = Container.new(:child_object => "social_buttons")
    assert_equal true, c.valid?
  end
end
