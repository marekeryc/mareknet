require 'test_helper'

class HourTest < ActiveSupport::TestCase
  test "view formatted hours scenario 1" do
    c = FactoryGirl.build(:container, child_object: "hours")
    c.hours.build(:day => "Monday", :start => "9:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Tuesday", :start => "9:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Wednesday", :start => "9:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Thursday", :start => "9:00 AM", :end => "5:00 PM")
    c.save
    assert_equal Hour.view_formatted_hours(c.id), {"9:00 AM - 5:00 PM" => ["Mon - Thu"]}
  end
  
  test "view formatted hours scenario 2" do
    c = FactoryGirl.build(:container, child_object: "hours")
    c.hours.build(:day => "Monday", :start => "9:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Wednesday", :start => "9:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Thursday", :start => "9:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Friday", :start => "9:00 AM", :end => "3:00 PM")
    c.hours.build(:day => "Sunday", :start => "9:00 AM", :end => "5:00 PM")
    c.save
    assert_equal Hour.view_formatted_hours(c.id), {"9:00 AM - 5:00 PM"=>["Mon", "Wed - Thu", "Sun"], "9:00 AM - 3:00 PM"=>["Fri"]}
  end
  
  test "view formatted hours scenario 3" do
    c = FactoryGirl.build(:container, child_object: "hours")
    c.hours.build(:day => "Monday", :start => "9:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Tuesday", :start => "9:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Thursday", :start => "9:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Sunday", :start => "9:00 AM", :end => "5:00 PM")
    c.save
    assert_equal Hour.view_formatted_hours(c.id), {"9:00 AM - 5:00 PM"=>["Mon - Tue", "Thu", "Sun"]}
  end
  
  test "view formatted hours scenario 4" do
    c = FactoryGirl.build(:container, child_object: "hours")
    c.hours.build(:day => "Monday", :start => "9:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Tuesday", :start => "8:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Wednesday", :start => "9:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Thursday", :start => "8:00 AM", :end => "5:00 PM")
    c.hours.build(:day => "Friday", :start => "10:00 AM", :end => "5:00 PM")
    c.save
    assert_equal Hour.view_formatted_hours(c.id), {"9:00 AM - 5:00 PM"=>["Mon", "Wed"], "8:00 AM - 5:00 PM"=>["Tue", "Thu"], "10:00 AM - 5:00 PM"=>["Fri"]}
  end
end