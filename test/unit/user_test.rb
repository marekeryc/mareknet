require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def teardown
    ActionMailer::Base.deliveries.clear
  end
  
  test "trial user" do
    u = FactoryGirl.create(:free_user)
    assert !u.paying_user?
    assert u.valid?
    assert_equal 0, u.yearly_total
    assert_equal 0, u.domain_registration_total
    assert_equal 0, u.plan_only_yearly_total
    assert_equal 0, u.monthly_total
    assert_equal ActionMailer::Base.deliveries.length, 2
  end
  
  test "paying user" do
    s = FactoryGirl.create(:site, domain_type: "existing_custom_domain")
    u = FactoryGirl.build(:paid_user_with_custom_domain)
    u.sites << s
    u.save
    assert s.user.paying_user?
    assert_equal 10, s.user.monthly_total
    assert_equal 0, s.user.yearly_total
    assert_equal ActionMailer::Base.deliveries.length, 2
  end
  
  test "paying user with coupon code" do
    s = FactoryGirl.create(:site)
    u = FactoryGirl.build(:paid_user_with_custom_domain, payment_frequency: "Monthly", promo_code: "greatscott")
    u.sites << s
    u.save
    assert u.paying_user?
    assert_equal 9, u.monthly_total
    assert_equal 0, u.yearly_total
    assert_equal ActionMailer::Base.deliveries.length, 2
  end
  
  test "monthly user with domain registration" do
    s = FactoryGirl.create(:site, domain_type: "new_custom_domain")
    u = FactoryGirl.build(:paid_user_with_custom_domain, payment_frequency: "Monthly")
    u.sites << s
    u.save
    assert u.paying_user?
    assert_equal 10, u.monthly_total
    assert_equal 15, u.yearly_total
    assert_equal 15, u.domain_registration_total
    assert_equal 0, u.plan_only_yearly_total
    assert_equal ActionMailer::Base.deliveries.length, 2
  end
  
  test "yearly user with domain registration and coupon code" do
    s = FactoryGirl.create(:site, domain_type: "new_custom_domain")
    u = FactoryGirl.build(:paid_user_with_custom_domain, payment_frequency: "Yearly", promo_code: "greatscott")
    u.sites << s
    u.save
    assert u.paying_user?
    assert_equal 0, u.monthly_total
    assert_equal 99.9, u.yearly_total
    assert_equal 13.5, u.domain_registration_total
    assert_equal 86.4, u.plan_only_yearly_total
    assert_equal ActionMailer::Base.deliveries.length, 2
  end
end