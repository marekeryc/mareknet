require 'test_helper'

class EmbedTest < ActiveSupport::TestCase
  test "invalid url" do
    e = Embed.new
    e.url = "w89ufaeijrefeioefjio.dsjafifj329j9dsa"
    assert !e.valid?
  end
  
  test "unsupported embed type" do
    e = Embed.new
    e.url = "http://google.com"
    assert !e.valid?
  end
  
  test "valid url" do
    e = Embed.new
    e.url = "http://www.youtube.com/watch?v=oHg5SJYRHA0"
    assert e.valid?
  end
end
