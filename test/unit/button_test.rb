require 'test_helper'

class ButtonTest < ActiveSupport::TestCase
  test "full url" do
    button = FactoryGirl.create(:button)
    button.url = "google.com"
    assert_equal "http://google.com", button.full_url
    button.url = "https://yahoo.com"
    assert_equal "https://yahoo.com", button.full_url
    button.url = "http://bing.com"
    assert_equal "http://bing.com", button.full_url
  end
end
