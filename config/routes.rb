Onepager::Application.routes.draw do
  root :to => 'static#index'
  
  resources :sites, :only => [:create, :show] do
    resources :pages, :only => [:show, :edit, :update] do
      member do
        put 'update_positions'
      end
    end
  end
  
  resource :signin, :controller => "user_sessions", :only => [:new, :create, :destroy], :path_names => { :new => "" }
  resource 'reset-password', :as => :reset_password, :controller => :reset_password, :only => [:new, :create, :edit, :update], :path_names => { :new => "" }
  resource :signup, :controller => "users", :only => [:new, :create], :path_names => { :new => "" } do
    collection do
      get 'get_city_state'
      get 'check_domain_availability'
      get 'validate_promo_code'
      get 'thankyou'
      post 'thankyou_survey'
    end
  end
  
  resources :themes, :only => [:show]
  
  # Things that happen on customers' Onepagers
  resources :subscribers, :only => [:create] do
    member do
      get 'subscribe'
      get 'unsubscribe'
    end
  end
  post 'contact_form_responses' => 'contact_form_responses#create'
  post 'promotion_responses' => 'promotion_responses#create'
  
  # Custom theme styles
  get 'custom/:id.css' => 'pages#css'
  get 'custom/:id.ie.css' => 'pages#css_ie'
  
  namespace :dashboard do
    root :to => 'analytics#show'
    resource :newsletter, :only => [:new, :create], :path_names => { :new => "" } do
      collection do
        post 'confirm'
      end
    end
    get 'forms' => 'forms#index'
    get 'forms/:id' => 'forms#show'
    resources :subscribers, :only => [:index]
    resource :setting, :only => [:edit, :update, :destroy], :path_names => { :edit => "" }
    resource :domain, :controller => "sites", :only => [:edit, :update], :path_names => { :edit => "" } do
      put 'update_free_domain_user' => 'sites#update_free_domain_user'
    end
    get 'sites/:site_id/export' => 'sites#export'
  end
  
  namespace :account do
    resources :sites, :only => [:index, :create, :destroy]
    resource :affiliate, :only => [:show, :new, :create]
    
    get 'upgrade' => 'users#upgrade'
    put 'upgrade' => 'users#upgrade_update'
    post 'feedback' => 'users#feedback_send'
    
    resources :users, :except => [:show]
    resource :billing, :only => [:edit, :update]
    resources :billing, :controller => :billings, :only => [:index, :show]
    resource :settings, :only => [:edit, :update], :path_names => { :edit => "" }
    resource :whitelabel, :only => [:edit, :update], :path_names => { :edit => "" }
  end
  
  namespace :content_types do
    resources :buttons, :only => [:new, :create, :update, :destroy]
    resources :contact_form_fields, :only => [:new, :create, :update, :destroy]
    resources :contact_sections, :only => [:new, :create, :update, :destroy]
    resources :download_sections, :only => [:new, :create, :update, :destroy]
    resources :downloads, :only => [:create]
    resources :embeds, :only => [:new, :create, :update, :destroy]
    resources :galleries, :only => [:new, :create, :update, :destroy]
    resources :gallery_images, :only => [:create]
    resources :hours, :only => [:new, :create, :update, :destroy]
    resources :newsletters, :only => [:new, :create, :update, :destroy]
    resources :services, :only => [:new, :create, :update, :destroy]
    resources :social_buttons, :only => [:new, :create, :update, :destroy]
    resources :social_feeds, :only => [:new, :create, :update, :destroy]
    resources :text_areas, :only => [:new, :create, :update, :destroy]
    resources :products, :only => [:new, :create, :update, :destroy]
    resources :iframes, :only => [:new, :create, :update, :destroy]
    resources :script_embeds, :only => [:new, :create, :update, :destroy]
    resources :paypal_buttons, :only => [:new, :create, :update, :destroy]
    resources :promotions, :only => [:new, :create, :update, :destroy]
  end
  
  # STATIC PAGES
  match 'en/:pagename' => 'static#show'
  match 'en/:section/:pagename' => 'static#show'
  
  # ADMIN PAGES
  namespace :opgadmin do
    root :to => redirect("/opgadmin/users")
    resources :themes, :except => [:show] do
      put 'update_positions', :on => :collection
    end
    resources :users, :only => [:index, :show, :destroy, :edit, :update] do
      post 'cancel', :on => :member
      post 'activate', :on => :member
      post 'signin_as', :on => :member
    end
    resources :sites, :only => [:destroy] do
      post 'activate', :on => :member
      post 'clone', :on => :member
      post 'dont_renew_domain', :on => :member
    end
  end

  # API
  post '/api/users' => 'api#create_user'
  match 'api/paypal_callback' => 'api#paypal_callback'
  
  get '/sitemap' => 'static#sitemap'
  get '/sitemaps/:id' => 'static#sitemaps'
  get '/robots' => 'static#robots'
  get ':onepager_address' => 'pages#show'
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
