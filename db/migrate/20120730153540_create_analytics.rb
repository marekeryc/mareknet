class CreateAnalytics < ActiveRecord::Migration
  def change
    create_table :analytics do |t|
      t.references :site
      t.integer :total_views
      t.integer :avg_time_on_site
      t.integer :new_visitors
      t.text :deep_dive
      t.text :keywords
      t.text :sources
      t.string :time_travel
      
      t.timestamps
    end
  end
end