class AddPasswordProtectedToSites < ActiveRecord::Migration
  def change
  	add_column :sites, :password_protected, :boolean, :default => false
  end
end