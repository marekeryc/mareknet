class AddUrlToGalleryImages < ActiveRecord::Migration
  def change
    add_column :gallery_images, :url, :string
  end
end
