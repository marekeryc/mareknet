class CreateContactFormFields < ActiveRecord::Migration
  def change
    create_table :contact_form_fields do |t|
      t.references :container
      t.integer :position, :default => 0
      t.string :field_name
      t.string :field_type
      t.boolean :required
      
      t.timestamps
    end
  end
end
