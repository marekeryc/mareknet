class AddGumroadIdToSites < ActiveRecord::Migration
  def change
  	add_column :sites, :gumroad_id, :string

  	User.where("gumroad_id IS NOT NULL").where(:active => true).each do |u|
  		u.sites.first.update_column(:gumroad_id, u.gumroad_id)
  	end
  end
end