class CreateDownloads < ActiveRecord::Migration
  def change
    create_table :downloads do |t|
      t.references :container
      t.integer :position, :default => 0
      t.string :caption
      t.string :filename
      
      t.timestamps
    end
  end
end
