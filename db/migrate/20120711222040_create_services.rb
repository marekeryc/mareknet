class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.references :container
      t.integer :position, :default => 0
      t.string :body
      t.timestamps
    end
  end
end