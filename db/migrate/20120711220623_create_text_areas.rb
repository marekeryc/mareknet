class CreateTextAreas < ActiveRecord::Migration
  def change
    create_table :text_areas do |t|
      t.references :page
      t.integer :position, :default => 0
      t.string :title
      t.text :body
      
      t.timestamps
    end
  end
end