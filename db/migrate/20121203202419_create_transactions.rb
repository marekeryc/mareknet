class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :user
      t.integer :transaction_id
      t.string :transaction_status
      t.decimal :amount, :precision => 8, :scale => 2
      t.string :last_four
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :city
      t.string :state
      t.string :postal_code
      t.string :email
      t.string :customer_id
      t.string :invoice_number

      t.timestamps
    end
  end
end
