class AddGumroadTokenToSite < ActiveRecord::Migration
  def change
    add_column :sites, :gumroad_token, :string

    json_file = File.read("#{Rails.root}/sessions.json")
    parsed_json = JSON(json_file)
    Site.where("gumroad_id IS NOT NULL").each do |s|
      parsed_json.each do |pj|
        if pj["user_id"].to_s == s.gumroad_id
          s.gumroad_token = pj["token"]
          s.save
        end
      end
    end
  end
end