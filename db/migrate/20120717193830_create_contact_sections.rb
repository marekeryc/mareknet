class CreateContactSections < ActiveRecord::Migration
  def change
    create_table :contact_sections do |t|
      t.references :page
      t.integer :position, :default => 0
      t.string :title
      t.text :description
      t.string :email
      t.string :phone
      t.string :fax
      t.string :address
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip
      t.boolean :map_enabled
      
      t.timestamps
    end
  end
end
