class CreateContactFormResponses < ActiveRecord::Migration
  def change
    create_table :contact_form_responses do |t|
      t.references :contact_form_field
      t.text :value
      
      t.timestamps
    end
  end
end
