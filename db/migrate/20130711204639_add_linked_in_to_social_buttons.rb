class AddLinkedInToSocialButtons < ActiveRecord::Migration
  def change
  	add_column :social_buttons, :linked_in_button, :boolean
  end
end