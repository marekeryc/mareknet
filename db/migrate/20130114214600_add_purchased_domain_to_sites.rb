class AddPurchasedDomainToSites < ActiveRecord::Migration
  def change
    add_column :sites, :purchased_domain, :string
    Site.where(:domain_type => "new_custom_domain").each do |s|
      s.purchased_domain = s.domain_name
      s.save
    end
  end
end