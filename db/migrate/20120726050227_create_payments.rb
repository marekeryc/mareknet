class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :user
      t.string :credit_card_type
      t.string :last_four
      t.string :authorization_number
      t.decimal :amount, :precision => 8, :scale => 2
      t.string :subscriber_id_monthly
      t.string :subscriber_id_annual
      
      t.timestamps
    end
  end
end