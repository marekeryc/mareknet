class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.references :site
      
      t.string :layout_alignment
      t.string :layout_width
      t.string :seo_title
      t.string :seo_description
      t.string :seo_keywords
      t.string :url
      
      t.timestamps
    end
  end
end