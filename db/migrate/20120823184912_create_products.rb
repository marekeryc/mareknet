class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.references :page
      t.integer :position, :default => 0
      t.string :name
      t.string :url
      t.decimal :price, precision: 12, scale: 2
      t.text :description
      t.integer :max_purchase_count
      t.string :country_available
      t.string :product_type
      t.string :gumroad_id
      t.string :gumroad_url
      t.string :preview_file
      t.string :product_file
      
      t.timestamps
    end
  end
end