class CreatePromotionResponses < ActiveRecord::Migration
  def change
    create_table :promotion_responses do |t|
      t.references :page
      t.references :promotion
      t.string :name
      t.string :email
      t.string :phone
      
      t.timestamps
    end
  end
end
