class CreatePaypalButtons < ActiveRecord::Migration
  def change
    create_table :paypal_buttons do |t|
      t.references :page
      t.integer :position, :default => 0
      t.string :name
      t.string :paypal_email
      t.decimal :price, precision: 12, scale: 2
      t.text :description
      t.string :preview_file
      t.string :currency
      t.boolean :customize_qty, :default => false
      t.decimal :shipping, precision: 12, scale: 2, :default => 0.00
      t.decimal :tax_rate, precision: 12, scale: 2, :default => 0.00
      
      t.timestamps
    end
  end
end