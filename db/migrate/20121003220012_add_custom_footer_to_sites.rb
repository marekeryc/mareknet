class AddCustomFooterToSites < ActiveRecord::Migration
  def change
    add_column :sites, :custom_footer, :string
  end
end
