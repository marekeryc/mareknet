class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.references :page
      t.integer :position, :default => 0
      t.string :title
      t.text :prize
      t.boolean :require_name
      t.boolean :require_email
      t.boolean :require_phone
      t.boolean :require_facebook_like
      t.string :facebook_url
      t.text :other

      t.timestamps
    end
  end
end
