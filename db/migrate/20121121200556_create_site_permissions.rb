class CreateSitePermissions < ActiveRecord::Migration
  def change
    create_table :site_permissions do |t|
      t.references :user
      t.integer :parent_user_id
      t.references :site

      t.timestamps
    end
  end
end
