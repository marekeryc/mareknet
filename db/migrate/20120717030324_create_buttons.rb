class CreateButtons < ActiveRecord::Migration
  def change
    create_table :buttons do |t|
      t.references :page
      t.integer :position, :default => 0
      t.string :title
      t.string :url
      
      t.timestamps
    end
  end
end
