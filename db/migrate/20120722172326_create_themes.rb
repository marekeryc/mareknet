class CreateThemes < ActiveRecord::Migration
  def change
    create_table :themes do |t|
      t.string :theme_name
      t.string :background_colour
      t.string :content_background_colour
      t.integer :content_background_opacity
      t.string :background_image
      t.string :background_image_alignment
      t.boolean :background_image_enabled
      t.boolean :background_image_fixed
      t.string :header_type
      t.string :header_colour
      t.integer :header_size
      t.string :subheader_type
      t.string :subheader_colour
      t.integer :subheader_size
      t.string :body_type
      t.string :body_colour
      t.integer :body_size
      t.string :screenshot_image
      t.integer :position, :default => 0
      
      t.timestamps
    end
  end
end
