class CreateIframes < ActiveRecord::Migration
  def change
    create_table :iframes do |t|
      t.references :page
      t.integer :position, :default => 0
      t.text :embed_code
      
      t.timestamps
    end
  end
end
