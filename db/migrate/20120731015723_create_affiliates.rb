class CreateAffiliates < ActiveRecord::Migration
  def change
    create_table :affiliates do |t|
      t.references :user
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :city
      t.string :state
      t.string :postal_code
      t.string :country
      t.string :phone
      t.string :paypal_email
      
      t.timestamps
    end
  end
end
