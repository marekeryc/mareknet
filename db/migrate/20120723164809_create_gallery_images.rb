class CreateGalleryImages < ActiveRecord::Migration
  def change
    create_table :gallery_images do |t|
      t.references :container
      t.integer :position, :default => 0
      t.string :caption
      t.string :filename
      
      t.timestamps
    end
  end
end
