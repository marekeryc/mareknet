class CreateNewsletters < ActiveRecord::Migration
  def change
    create_table :newsletters do |t|
      t.references :site
      t.string :subject
      t.text :message
      
      t.timestamps
    end
  end
end
