class CreateSocialButtons < ActiveRecord::Migration
  def change
    create_table :social_buttons do |t|
      t.references :container
      t.boolean :like_button
      t.boolean :tweet_button
      t.boolean :google_plus_button
      
      t.timestamps
    end
  end
end
