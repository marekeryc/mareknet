class CreateContainers < ActiveRecord::Migration
  def change
    create_table :containers do |t|
      t.references :page
      t.integer :position, :default => 0
      t.string :title
      t.text :description
      t.string :child_object
      
      t.timestamps
    end
  end
end