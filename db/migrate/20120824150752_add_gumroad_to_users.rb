class AddGumroadToUsers < ActiveRecord::Migration
  def change
    add_column :users, :gumroad_id, :integer
  end
end