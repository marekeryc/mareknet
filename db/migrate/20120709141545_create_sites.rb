class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.references :user
      
      t.string :domain_name
      t.string :domain_type # new_custom_domain, existing_custom_domain, no_custom_domain
      t.string :onepager_address
      
      t.string :company_name
      t.string :tagline
      t.references :theme
      t.string :background_colour
      t.string :content_background_colour
      t.integer :content_background_opacity
      
      t.string :background_image
      t.string :background_image_alignment
      t.boolean :background_image_enabled
      t.boolean :background_image_fixed, :default => 1
      
      t.string :logo_image
      t.string :favicon_image
      
      t.string :header_type
      t.string :header_colour
      t.integer :header_size
      t.string :subheader_type
      t.string :subheader_colour
      t.integer :subheader_size
      t.string :body_type
      t.string :body_colour
      t.integer :body_size
      
      t.text :custom_css
      t.string :analytics_property_id
      
      t.timestamps
    end
  end
end
