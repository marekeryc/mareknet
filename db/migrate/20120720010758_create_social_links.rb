class CreateSocialLinks < ActiveRecord::Migration
  def change
    create_table :social_links do |t|
      t.references :social_button
      t.integer :position, :default => 0
      t.string :service
      t.string :url
      
      t.timestamps
    end
  end
end
