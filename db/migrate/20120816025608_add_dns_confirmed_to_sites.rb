class AddDnsConfirmedToSites < ActiveRecord::Migration
  def change
    add_column :sites, :dns_confirmed, :boolean, :default => false
  end
end