class AddCustomizePriceToPaypalButtons < ActiveRecord::Migration
  def change
    add_column :paypal_buttons, :customize_price, :boolean, :default => false
  end
end
