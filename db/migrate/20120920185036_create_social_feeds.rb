class CreateSocialFeeds < ActiveRecord::Migration
  def change
    create_table :social_feeds do |t|
      t.references :page
      t.integer :position, :default => 0
      t.string :title
      t.text :description
      t.string :url
      t.integer :num_items
      t.string :feed_type

      t.timestamps
    end
  end
end
