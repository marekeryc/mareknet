class AddPaymentPlanToUsers < ActiveRecord::Migration
  def change
    add_column :users, :payment_plan, :string
    add_column :users, :recurly_account_uuid, :string
    add_column :users, :recurly_subscription_uuid, :string

    execute <<-SQL
      UPDATE users
      SET payment_plan = "Starter"
      WHERE payment_frequency != "Trial"
    SQL

    execute <<-SQL
      UPDATE users
      SET payment_plan = "Trial"
      WHERE payment_frequency = "Trial"
    SQL
  end
end
