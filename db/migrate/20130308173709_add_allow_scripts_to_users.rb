class AddAllowScriptsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :allow_script_embeds, :boolean, :default => false
  end
end
