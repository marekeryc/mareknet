class AddDeactivateResponsiveStylesToSites < ActiveRecord::Migration
  def change
    add_column :sites, :deactivate_responsive_styles, :boolean, :default => false
  end
end
