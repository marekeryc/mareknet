class CreateSubscribers < ActiveRecord::Migration
  def change
    create_table :subscribers do |t|
      t.references :site
      t.string :email
      t.boolean :active, :default => false
      
      t.timestamps
    end
  end
end