class CreateEmbeds < ActiveRecord::Migration
  def change
    create_table :embeds do |t|
      t.references :page
      t.integer :position, :default => 0
      t.string :title
      t.text :description
      t.string :url
      t.text :body
      t.string :provider
      t.string :category
      
      t.timestamps
    end
  end
end
