class AddWhiteLabelToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :white_label_active, :boolean
  	add_column :users, :white_label_title, :string
  	add_column :users, :white_label_colour, :string
  end
end