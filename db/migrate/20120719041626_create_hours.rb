class CreateHours < ActiveRecord::Migration
  def change
    create_table :hours do |t|
      t.references :container
      t.string :day
      t.string :start
      t.string :end
      
      t.timestamps
    end
  end
end
