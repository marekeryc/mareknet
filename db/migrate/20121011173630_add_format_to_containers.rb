class AddFormatToContainers < ActiveRecord::Migration
  def change
    add_column :containers, :format, :string
    
    Container.where(["child_object = ?", 'gallery_images']).each do |c|
      if c.format.blank?
        c.format = "thumbnails"
        c.save
      end
    end
  end
end
