# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Theme.create(
  :theme_name => 'Basic Light',
  :background_colour => "b7c2e2",
  :background_image_alignment => "auto",
  :background_image_fixed => true,
  :background_image_enabled => true,
  :content_background_colour => "ffffff",
  :content_background_opacity => "100",
  :header_type => "Arial",
  :header_colour => "212325",
  :header_size => "58",
  :subheader_type => "Arial",
  :subheader_colour => "333366",
  :subheader_size => "31",
  :body_type => "Arial",
  :body_colour => "526766",
  :body_size => "18",
  :screenshot_image => File.open("#{Rails.root}/app/assets/images/pages/toolbox/sample_theme_screenshot.jpg")
)

u = User.new(
  :email => "erictarn@gmail.com",
  :password => "admin123",
  :first_name => Faker::Name.first_name,
  :last_name => Faker::Name.last_name,
  :address => Faker::Address.street_address,
  :address2 => Faker::Address.secondary_address,
  :city => Faker::Address.city,
  :state => Faker::Address.state,
  :zip => Faker::Address.zip_code,
  :country => "United States",
  :payment_frequency => "Monthly"
)

u.payments << Payment.new(
  :credit_card_number => "4111111111111111",
  :expiration_month => "01",
  :expiration_year => "2016",
  :security_code => "111"
)

s = Site.default
s.domain_type = "no_custom_domain"
s.onepager_address = "et"
s.analytics << Analytic.default(:week) << Analytic.default(:month)
u.sites << s
u.save

# Site.create(
#   :user_id => 1,
#   :domain_type => "no_custom_domain",
#   :onepager_address => "bobross",
#   :custom_domain => false,
#   :company_name => "Bob Ross Painting Archive Inc.",
#   :theme_name => "Custom",
#   :background_colour => "222023",
#   :content_background_colour => "000000",
#   :content_background_opacity => "80",
#   :header_type => "damion",
#   :header_colour => "ffffff",
#   :header_size => 78,
#   :subheader_type => "maiden-orange",
#   :subheader_colour => "fff89a",
#   :subheader_size => 43,
#   :body_type => "cabin",
#   :body_colour => "ffffff",
#   :body_size => 21
# )
# 
# Page.create(
#   :site_id => 1,
#   :layout_alignment => "none",
#   :layout_width => "450",
#   :seo_title => "Bob Ross Painting",
#   :seo_description => "Lorem ipsum dolorean bobo rosso",
#   :seo_keywords => "painting, bob, ross, art"
# )