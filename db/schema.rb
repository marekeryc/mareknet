# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150210232924) do

  create_table "affiliates", :force => true do |t|
    t.integer  "user_id"
    t.string   "first_name",   :default => "", :null => false
    t.string   "last_name",    :default => "", :null => false
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "postal_code"
    t.string   "country"
    t.string   "phone"
    t.string   "paypal_email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "analytics", :force => true do |t|
    t.integer  "site_id"
    t.integer  "total_views"
    t.integer  "avg_time_on_site"
    t.integer  "new_visitors"
    t.text     "deep_dive"
    t.text     "keywords"
    t.text     "sources"
    t.string   "time_travel"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "buttons", :force => true do |t|
    t.integer  "page_id"
    t.integer  "position",   :default => 0
    t.string   "title"
    t.string   "url"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "contact_form_fields", :force => true do |t|
    t.integer  "container_id"
    t.integer  "position"
    t.string   "field_name"
    t.string   "field_type"
    t.boolean  "required"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "contact_form_responses", :force => true do |t|
    t.integer  "contact_form_field_id"
    t.text     "value"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "contact_sections", :force => true do |t|
    t.integer  "page_id"
    t.integer  "position",    :default => 0
    t.string   "title"
    t.text     "description"
    t.string   "email"
    t.string   "phone"
    t.string   "fax"
    t.string   "address"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.boolean  "map_enabled"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "containers", :force => true do |t|
    t.integer  "page_id"
    t.integer  "position",     :default => 0
    t.string   "title"
    t.text     "description"
    t.string   "child_object"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.string   "format"
  end

  create_table "downloads", :force => true do |t|
    t.integer  "container_id"
    t.integer  "position"
    t.string   "caption"
    t.string   "filename"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "embeds", :force => true do |t|
    t.integer  "page_id"
    t.integer  "position",    :default => 0
    t.string   "title"
    t.text     "description"
    t.string   "url"
    t.text     "body"
    t.string   "provider"
    t.string   "category"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "gallery_images", :force => true do |t|
    t.integer  "container_id"
    t.integer  "position"
    t.string   "caption"
    t.string   "filename"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "url"
  end

  create_table "hours", :force => true do |t|
    t.integer  "container_id"
    t.string   "day"
    t.string   "start"
    t.string   "end"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "iframes", :force => true do |t|
    t.integer  "page_id"
    t.integer  "position",   :default => 0
    t.text     "embed_code"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "newsletters", :force => true do |t|
    t.integer  "site_id"
    t.string   "subject"
    t.text     "message"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "from"
  end

  create_table "pages", :force => true do |t|
    t.integer  "site_id"
    t.string   "layout_alignment"
    t.string   "layout_width"
    t.string   "seo_title"
    t.string   "seo_description"
    t.string   "seo_keywords"
    t.string   "url"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "payments", :force => true do |t|
    t.integer  "user_id"
    t.string   "credit_card_type"
    t.string   "last_four"
    t.string   "authorization_number"
    t.decimal  "amount",                :precision => 8, :scale => 2
    t.string   "subscriber_id_monthly"
    t.string   "subscriber_id_annual"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
  end

  create_table "paypal_buttons", :force => true do |t|
    t.integer  "page_id"
    t.integer  "position",                                       :default => 0
    t.string   "name"
    t.string   "paypal_email"
    t.decimal  "price",           :precision => 12, :scale => 2
    t.text     "description"
    t.string   "preview_file"
    t.string   "currency"
    t.boolean  "customize_qty",                                  :default => false
    t.decimal  "shipping",        :precision => 12, :scale => 2, :default => 0.0
    t.decimal  "tax_rate",        :precision => 12, :scale => 2, :default => 0.0
    t.datetime "created_at",                                                        :null => false
    t.datetime "updated_at",                                                        :null => false
    t.boolean  "customize_price",                                :default => false
  end

  create_table "products", :force => true do |t|
    t.integer  "page_id"
    t.integer  "position",                                          :default => 0
    t.string   "name"
    t.string   "url"
    t.decimal  "price",              :precision => 12, :scale => 2
    t.text     "description"
    t.integer  "max_purchase_count"
    t.string   "country_available"
    t.string   "product_type"
    t.string   "gumroad_id"
    t.string   "gumroad_url"
    t.string   "preview_file"
    t.string   "product_file"
    t.datetime "created_at",                                                       :null => false
    t.datetime "updated_at",                                                       :null => false
  end

  create_table "promotion_responses", :force => true do |t|
    t.integer  "page_id"
    t.integer  "promotion_id"
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "promotions", :force => true do |t|
    t.integer  "page_id"
    t.integer  "position",              :default => 0
    t.string   "title"
    t.text     "prize"
    t.boolean  "require_name"
    t.boolean  "require_email"
    t.boolean  "require_phone"
    t.boolean  "require_facebook_like"
    t.string   "facebook_url"
    t.text     "other"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.string   "email"
  end

  create_table "script_embeds", :force => true do |t|
    t.integer  "page_id"
    t.integer  "position",   :default => 0
    t.text     "embed_code"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "services", :force => true do |t|
    t.integer  "container_id"
    t.integer  "position",     :default => 0
    t.string   "body",         :default => "", :null => false
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "site_permissions", :force => true do |t|
    t.integer  "user_id"
    t.integer  "parent_user_id"
    t.integer  "site_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "sites", :force => true do |t|
    t.integer  "user_id"
    t.string   "domain_name"
    t.string   "domain_type"
    t.string   "onepager_address"
    t.string   "company_name"
    t.string   "tagline"
    t.integer  "theme_id"
    t.string   "background_colour"
    t.string   "content_background_colour"
    t.integer  "content_background_opacity"
    t.string   "background_image"
    t.string   "background_image_alignment"
    t.boolean  "background_image_enabled"
    t.boolean  "background_image_fixed",       :default => true
    t.string   "logo_image"
    t.string   "favicon_image"
    t.string   "header_type"
    t.string   "header_colour"
    t.integer  "header_size"
    t.string   "subheader_type"
    t.string   "subheader_colour"
    t.integer  "subheader_size"
    t.string   "body_type"
    t.string   "body_colour"
    t.integer  "body_size"
    t.text     "custom_css"
    t.string   "analytics_property_id"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.boolean  "dns_confirmed",                :default => false
    t.boolean  "password_protected",           :default => false
    t.string   "custom_footer"
    t.boolean  "active",                       :default => true
    t.string   "gumroad_id"
    t.string   "purchased_domain"
    t.string   "gumroad_token"
    t.boolean  "deactivate_responsive_styles", :default => false
  end

  create_table "social_buttons", :force => true do |t|
    t.integer  "container_id"
    t.boolean  "like_button"
    t.boolean  "tweet_button"
    t.boolean  "google_plus_button"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.boolean  "linked_in_button"
  end

  create_table "social_feeds", :force => true do |t|
    t.integer  "page_id"
    t.integer  "position",    :default => 0
    t.string   "title"
    t.text     "description"
    t.string   "url"
    t.integer  "num_items"
    t.string   "feed_type"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "social_links", :force => true do |t|
    t.integer  "social_button_id"
    t.integer  "position"
    t.string   "service"
    t.string   "url"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "subscribers", :force => true do |t|
    t.integer  "site_id"
    t.string   "email"
    t.boolean  "active",     :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "text_areas", :force => true do |t|
    t.integer  "page_id"
    t.integer  "position",   :default => 0
    t.string   "title"
    t.text     "body"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "themes", :force => true do |t|
    t.string   "theme_name"
    t.integer  "position",                   :default => 0
    t.string   "background_colour"
    t.string   "content_background_colour"
    t.integer  "content_background_opacity"
    t.string   "background_image"
    t.string   "background_image_alignment"
    t.boolean  "background_image_enabled"
    t.boolean  "background_image_fixed"
    t.string   "header_type"
    t.string   "header_colour"
    t.integer  "header_size"
    t.string   "subheader_type"
    t.string   "subheader_colour"
    t.integer  "subheader_size"
    t.string   "body_type"
    t.string   "body_colour"
    t.integer  "body_size"
    t.string   "screenshot_image"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  create_table "transactions", :force => true do |t|
    t.integer  "user_id"
    t.string   "transaction_id"
    t.string   "transaction_status"
    t.decimal  "amount",             :precision => 8, :scale => 2
    t.string   "last_four"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "postal_code"
    t.string   "email"
    t.string   "customer_id"
    t.string   "invoice_number"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                        :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "address"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "phone"
    t.string   "country"
    t.string   "promo_code"
    t.string   "payment_frequency"
    t.string   "crypted_password",                             :null => false
    t.string   "password_salt",                                :null => false
    t.string   "persistence_token",                            :null => false
    t.string   "single_access_token",                          :null => false
    t.string   "perishable_token",                             :null => false
    t.integer  "login_count",               :default => 0,     :null => false
    t.integer  "failed_login_count",        :default => 0,     :null => false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.datetime "upgrade_date"
    t.datetime "cancellation_date"
    t.datetime "temp_cancellation_date"
    t.integer  "affiliate_of"
    t.boolean  "api_user"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.integer  "gumroad_id"
    t.string   "payment_plan"
    t.string   "recurly_account_uuid"
    t.string   "recurly_subscription_uuid"
    t.boolean  "active",                    :default => true,  :null => false
    t.boolean  "white_label_active"
    t.string   "white_label_title"
    t.string   "white_label_colour"
    t.boolean  "admin",                     :default => false
    t.boolean  "allow_script_embeds",       :default => false
  end

  create_table "zips", :force => true do |t|
    t.string "city"
    t.string "state"
  end

end
